#include "game.hpp"

#include "SDL2/SDL.h"
#include "debug/logger.hpp"
#include "ini.h"
#include "input.hpp"
#include "utils/concommand.hpp"
#include <cmath>
#include <cstring>
#include "utils/convar.hpp"
#include "menus.hpp"
#include "renderer/renderer.hpp"
#include "utils/file.hpp"
#include "utils/file_system.hpp"
#include "world/world.hpp"
#include "dirent.h"
#include "renderer/nuklear_backend.hpp"
#include "renderer/view.hpp"
#include "world/map.hpp"
#include "utils/convar.hpp"

Game *g_game = nullptr;

void _quit(int argc, std::string *argv)
{
	g_game->shutdown();
}
void _toggle_console(int argc, std::string *argv)
{
	g_game->toggle_console();
}
void _set(int argc, std::string *argv)
{
	if(argc < 2) return;

	ConVar *cv = utils::get_convar(argv[0]);
	if(cv)
	{
		if(cv->isNumber())
		{
			cv->setNumber(atoi(argv[1].c_str()));
		}
		else if(cv->isString())
		{
			cv->setString(argv[1]);
		}
		else
		{
			LOG_ERROR("Couldn't set convar");
		}
	}
}

ConsoleCommand cm_toggle_console("toggle_console", _toggle_console, "toggle console");
ConsoleCommand cm_quit("quit", _quit, "Quit game");
ConsoleCommand cm_set("set", _set, "Set convar");
ConVar cv_current_game("current_game", "base", "Current game");
ConVar cv_res_width("res_width", 800, "width");
ConVar cv_res_height("res_height", 600, "height");
ConVar cv_res_scale("res_scale", 1);

Game::Game() :
	m_window(NULL),
	m_gl_context(NULL),
	m_world(NULL),
	m_renderer(NULL),
	m_input(NULL),
	m_initialized(false),
	m_show_console(false),
	m_current_ui_func(NULL)
{

}

Game::~Game()
{
	nk_delete_context(m_nuklear);
	delete m_world;
	delete m_renderer;
	delete m_input;

	SDL_GL_DeleteContext(m_gl_context);
	SDL_DestroyWindow(m_window);
	SDL_Quit();
}

int handler(void *usr, const char *section, const char *name, const char *val)
{
	if(strcmp(section, "convars") == 0)
	{
		//convar = val
		ConVar *cv = utils::get_convar(name);
		if(cv)
		{
			if(cv->isNumber())
			{
				cv->setNumber(std::round(std::stof(val, NULL)));
			}
			else if(cv->isString())
			{
				cv->setString(val);
			}
		}
	}
	else if(strcmp(section, "bindings") == 0)
	{
		//command = key
		g_game->getInput()->bind(val, name);
	}
	else
	{
		LOG_WARNING("INI Parse: Invalid section type");
		return 0;
	}

	return 1;
}

void load_config()
{
	File::Ptr file = filesystem::open_file("config.cfg", "r"); 
	if(!file)
	{
		LOG_FATAL_ERROR("Failed to find config file");
		return;
	}

	int size = filesystem::get_file_size(file.get());
	char *buffer = new char[size+1];
	int read = fread(buffer, 1, size, file.get());
	if(read == 0)
	{
		LOG_FATAL_ERROR("Failed to read config");
	}
	buffer[read] = '\0';

	int err = ini_parse_string(buffer, handler, NULL);
	if(err) 
	{
		LOG_ERROR("INI Parse: error on line %d", err);
	}

	delete[] buffer;
}

void save_config()
{
}

void Game::init()
{
	//init SDL
	if(SDL_Init(SDL_INIT_VIDEO))
	{
		LOG_ERROR("Unable to initialize SDL: %s", SDL_GetError());
		return;
	}

	m_input = new Input;

	//load config
	load_config();

    //init window
	int width = utils::get_convar("res_width")->getNumber();
	int height = utils::get_convar("res_height")->getNumber();
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	m_window = SDL_CreateWindow("CompSci_NEA", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_OPENGL);
	if(!m_window)
	{
		LOG_ERROR("Could not create window: %s", SDL_GetError());
		return;
	}

	//int gl context
	m_gl_context = SDL_GL_CreateContext(m_window);
	if(!m_gl_context)
	{
		LOG_ERROR("Couldn't create context: %s", SDL_GetError());
		return;
	}

    //make window gl context current and load glad
	SDL_GL_MakeCurrent(m_window, m_gl_context);
    if(!gladLoadGL())
    {
        LOG_ERROR("GLAD failed to load GL");
		return;
    }
    
    //init  renderer
    m_renderer = new Renderer;

    m_world = new World;

	//load nuklear
	m_nuklear = nk_create_context();

	//load fonts
	nk_font_atlas atlas;
	nk_font_stash_begin(m_nuklear, &atlas);
	const nk_font *font = nk_font_stash_add_from_file(&atlas, 14, "font.ttf");
	nk_font_stash_end(m_nuklear, &atlas);
	nk_style_set_font(m_nuklear, &font->handle);

	Viewport vp(0, 0, width, height);
	Camera nk_view(0, 1, 0, width, 0, height);
	nk_set_camera(m_nuklear, nk_view);
	
    //finally show window
	SDL_ShowWindow(m_window); 
    
	//start main menu
	set_state(MAIN_MENU);

	m_initialized = true;
}

void Game::loop()
{
	bool quit = false;
	float last_time = SDL_GetTicks()/1000.0f;

    while(!quit)
    {
		float current_time = SDL_GetTicks()/1000.0f;
		m_frametime = current_time - last_time;
		last_time = current_time;
        
        m_input->reset_rel_mouse();
	
		nk_input_begin(m_nuklear);
		SDL_Event event;
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_QUIT:
				{
					quit = true;
					break;
				}
				case SDL_KEYDOWN:
				case SDL_KEYUP:
				{
					m_input->handleKey(event);	
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				case SDL_MOUSEBUTTONUP:
				{
					m_input->handleMouseButton(event);	
					break;
				}
				case SDL_MOUSEMOTION:
				{
					m_input->handleMouseMotion(event);
					break;
				}
				case SDL_TEXTINPUT:
				{
					//pass staright to ui
					nk_glyph glyph;
					memcpy(glyph, event.text.text, NK_UTF_SIZE);
					nk_input_glyph(m_nuklear, glyph);
					break;
				}
				default:
				{
					break;
				}
			}
		}
		nk_input_end(m_nuklear);
		
		m_input->execute_held_keys();

		if(m_state == LOADING_LEVEL)
		{
			Menus::draw_loading_screen(m_nuklear, m_loading_status);
		}
		else
		{
			if(m_state == PLAYING) 
				m_world->tick();
			else if(m_state == MAIN_MENU) 
				Menus::draw_main_menu(m_nuklear);
			else if(m_state == PAUSED)
				Menus::draw_pause_menu(m_nuklear);
			if(m_show_console) 
				Menus::draw_console(m_nuklear);
		}
		
		//render
		m_renderer->beginRender();
		{
			if(m_state == PLAYING) m_world->render();
			nk_render(m_nuklear, m_renderer);
		}
        m_renderer->endRender();

		SDL_GL_SwapWindow(m_window);
    }
}

void Game::shutdown()
{
	//push quit event to sdl
	SDL_Event quit_event;
	quit_event.type = SDL_QUIT;
	SDL_PushEvent(&quit_event);
}

void Game::set_relative_mouse_mode(bool b)
{
	if(m_current_ui_func || m_show_console)
	{
		SDL_SetRelativeMouseMode((SDL_bool)b);
	}
}

void Game::loading_level_func(std::string map_path)
{
	Map *map = NULL;

	m_loading_status = "Loading Map File";
	{
		map = new Map;
		if(!map->load_from_file(map_path))
		{
			LOG_ERROR("Failed to load map %s", map_path.c_str());
			return;
		}
	}

	m_loading_status = "Spawning Entities";
	{
		m_world->initFromMap(map);
	}

	set_state(PLAYING);
}

//loading bar:
//loading map file
//spawning entities
void Game::change_map(std::string map_path)
{
	//spawn thread
	m_loading_level_thread = std::thread(&Game::loading_level_func, this, map_path);

	//set state
	set_state(LOADING_LEVEL);
}

void Game::toggle_pause()
{
	if(get_state() == PAUSED)
	{
		set_state(PLAYING);
	}
	else
	{
		pause();
	}
}

void Game::pause()
{
	if(get_state() == PLAYING)
	{
		set_state(PAUSED);
	}
}

void Game::toggle_console()
{
	pause(); //pause the game if not paused
	m_show_console = !m_show_console;
}

int Game::list_maps(const char ***maps)
{
    DIR *dir;
    dirent *ent;
    std::string maps_path = filesystem::get_data_path() + "maps";
    std::vector<std::string> files;
    
    if((dir = opendir(maps_path.c_str())) != NULL)
    {
        while((ent = readdir(dir)) != NULL)
        {
            files.push_back(std::string(ent->d_name));
        }
    }
    
    *maps = new const char *[files.size()];
    for(int i = 0; i < files.size();i++)
    {
        std::string file = files[i];
        
        (*maps)[i] = strdup(file.c_str());
    }
        
    return files.size();
}

void Game::set_state(GameState state)
{
	m_state = state;
	switch(state)
	{
		case PLAYING:
		case LOADING_LEVEL:
			SDL_SetRelativeMouseMode(SDL_TRUE);
			break;
		case PAUSED:
		case MAIN_MENU:
			SDL_SetRelativeMouseMode(SDL_FALSE);
			break;
		default:
			assert(false);
	}
}

GameState Game::get_state()
{
	return m_state;
}
