#include "input.hpp"

#include "SDL2/SDL.h"
#include "utils/concommand.hpp"
#include "debug/logger.hpp"
#include <SDL_mouse.h>
#include <memory>
#include "game.hpp"
#include <algorithm>
#include "renderer/nuklear_backend.hpp"

#if 0
//bind_key <key> "command1" "command2"..
void _bind_key(int argc, std::string argv[])
{
    if(argc <= 1) return;

    KeyboardCommands commands;

    for(int i = 1; i < argc; i++)
    {
        TokenisedCommand *cmd = ConsoleCommand_TokenizeCommand(argv[i]);  
        if(cmd)
        {
            commands.emplace_back(cmd);
        } 
    }

    g_game->getKeyboard()->bind(argv[0], commands);
    LOG_INFO("Bound key %s to %s", argv[0].c_str(), argv[1].c_str());
}
static ConsoleCommand cmd_bind_key("bind_key", _bind_key, "Bind key to action");
#endif

void handleCommand(const SDL_Event &event, bool down, std::map<SDL_Keycode, std::string> &command_binding, std::vector<SDL_Keycode> &down_keys)
{
	auto command_name_itr = command_binding.find(event.key.keysym.sym);
	if(command_name_itr == command_binding.end()) return;
	std::string command_name = command_name_itr->second;

	ConsoleCommand *cc = utils::get_concommand(command_name);
	if(cc)
	{
		//if the command also requires a key up event then supply that is need be
		command up_command = cc->getUpCommand();
		if(up_command)
		{
			if(down)
			{
				down_keys.push_back(event.key.keysym.sym);
			}
			else
			{
				//call up command
				up_command(0, NULL);
				//now you can remove key from down list
                down_keys.erase(std::remove(down_keys.begin(), down_keys.end(), event.key.keysym.sym), down_keys.end());
			}
		}
		else
		{
			if(down)
			{
				//don't add to down keys list, just call command
				cc->getCommand()(0, NULL);
			}
			//ignore key up
		}
	}
}

void Input::reset_rel_mouse()
{
    mouse_rpos_x = 0;
    mouse_rpos_y = 0;
}

void Input::execute_held_keys()
{
    for(int i = 0; i < down_keys.size(); i++)
    {
        SDL_Keycode key = down_keys[i];
        
        auto command_name_itr = command_binding.find(key);
		if(command_name_itr == command_binding.end()) break;
		std::string command_name = command_name_itr->second;
	
		ConsoleCommand *cc = utils::get_concommand(command_name);
		if(cc)
		{
			cc->getCommand()(0, NULL);
		}
    }
}

void Input::bind(const std::string &name, const std::string &command)
{
	if(utils::get_concommand(command) == NULL)
	{
		LOG_ERROR("%s is not a valid command to bind to", command.c_str());
		return;
	}

	SDL_Keycode key = SDL_GetKeyFromName(name.c_str());
    if(key == SDLK_UNKNOWN)
	{
		LOG_ERROR("%s is not a valid key", name.c_str());
		return;
	}
	command_binding[key] = command;
}

void Input::handleKey(const SDL_Event &event)
{
    if(event.key.repeat) return;
    
	bool down = event.key.state == SDL_PRESSED;
	if(event.key.keysym.sym == SDLK_ESCAPE)
	{
		if(down)
		{
			g_game->toggle_pause();
		}
	}
	else 
	{
		nk_keys key = nk_input_key_sdl_to_nk(event.key.keysym.sym, event.key.keysym.mod);
		if(key == NK_KEY_MAX) //key isnt handled by nk
		{
			handleCommand(event, down, command_binding, down_keys);
		}
		else
		{
			nk_input_key(g_game->getNuklear(), key, event.key.state == SDL_PRESSED);
		}
	}
}

void Input::handleMouseButton(const SDL_Event &event)
{
	//if the cursor is showing send to nuklear
	if(!SDL_GetRelativeMouseMode())
	{
		nk_buttons button = nk_input_button_sdl_to_nk(event.button.button);
		if(button == NK_BUTTON_MAX) return;
		nk_input_button(g_game->getNuklear(), button, event.button.x, event.button.y, event.button.state == SDL_PRESSED);
	}
	else
	{
		handleCommand(event, event.button.state == SDL_PRESSED, command_binding, down_keys);
	}
}

void Input::handleMouseMotion(const SDL_Event &event)
{
	if(!SDL_GetRelativeMouseMode())
	{
		nk_input_motion(g_game->getNuklear(), event.motion.x, event.motion.y);
	}
	else
	{
		mouse_rpos_x = event.motion.xrel;
		mouse_rpos_y = event.motion.yrel;
	}
}
