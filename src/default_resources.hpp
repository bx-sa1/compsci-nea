#ifndef DEFAULT_RESOURCES_H_
#define DEFAULT_RESOURCES_H_

namespace internal
{
    extern const char *default_vertex_shader;
    extern unsigned int default_vertex_shader_len;

    extern const char *default_fragment_shader;
    extern unsigned int default_fragment_shader_len;

    extern const char *default_material;
    extern unsigned int default_material_len;

    extern unsigned char default_texture_2d[];
    extern unsigned int default_texture_2d_len;

    extern unsigned char default_model[];
    extern unsigned int default_model_len;
};

#endif