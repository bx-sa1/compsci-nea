#ifndef DEBUG_DRAW_IMPLEMENTATION_H_
#define DEBUG_DRAW_IMPLEMENTATION_H_

#include "debug_draw.hpp"
#include "glad/glad.h"
#include "Eigen/Eigen"

class Material;
class Model;
class DebugDrawRenderInterface : public dd::RenderInterface
{
public:
	DebugDrawRenderInterface();
	~DebugDrawRenderInterface();
	void drawPointList(const dd::DrawVertex *points, int count, bool depthEnabled) override;
	void drawLineList(const dd::DrawVertex *lines, int count, bool depthEnabled) override;
	void endDraw() override;
	
	Eigen::Matrix4f m_mvp;
private:
	Model *m_debug_model;
};

#endif
