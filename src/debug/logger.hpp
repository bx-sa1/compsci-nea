#ifndef LOGGER_H_
#define LOGGER_H_

#include <cstdio>
#include <cstdlib>

/** 
 * Just simple defines that 
 * write to stdout some text with information
 * before it aswell.
**/

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_WHITE   "\x1b[37m"
#define ANSI_COLOR_RESET   "\x1b[0m"

void sync_fprintf(FILE *stream, const char *text, ...);

#define LOG_INFO(info, args...) { sync_fprintf(stdout, ANSI_COLOR_CYAN "INFO" ANSI_COLOR_RESET "   %s:%d   " info "\n", __FILE__, __LINE__, ##args); }
#define LOG_WARNING(warning, args...) { sync_fprintf(stdout, ANSI_COLOR_YELLOW "WARNING" ANSI_COLOR_RESET "    %s:%d   " warning "\n", __FILE__, __LINE__, ##args); }
#define LOG_ERROR(error, args...) { sync_fprintf(stderr, ANSI_COLOR_RED "ERROR" ANSI_COLOR_RESET "  %s:%d   " error "\n", __FILE__, __LINE__, ##args); }
#define LOG_FATAL_ERROR(f_error, args...) { sync_fprintf(stderr, ANSI_COLOR_RED "FATAL_ERROR   %s:%d   " f_error ANSI_COLOR_RESET "\n", __FILE__, __LINE__, ##args); g_game->shutdown(); }

inline static void assert_func(const char *x) { LOG_WARNING("Failed Assertion: %s", x); abort(); }

#ifndef NDEBUG

    #undef assert

    #define assert(x) (void)((x) || (assert_func(#x), 0))

#endif

#endif
