#include "logger.hpp"

#include <cstdarg>
#include <mutex>

static std::mutex mutex;
void sync_fprintf(FILE *stream, const char *format, ...)
{

    va_list args;
    va_start(args, format);

    {
        std::unique_lock<std::mutex> lock(mutex);
        vfprintf(stream, format, args);
    }

    va_end(args);
}