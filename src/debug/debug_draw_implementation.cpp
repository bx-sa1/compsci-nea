#define DEBUG_DRAW_IMPLEMENTATION
#include "debug_draw_implementation.hpp"

#include "renderer/renderer.hpp"
#include "game.hpp"
#include "renderer/render_vertex.hpp"
#include <string.h>
#include "renderer/vertex_buffer.hpp"
#include "renderer/material.hpp"
#include "renderer/model.hpp"

DebugDrawRenderInterface::DebugDrawRenderInterface()
{
	m_debug_model = new Model;
	m_debug_model->meshes().emplace_back();
	//m_debug_model->meshes()[0].mat = (Material*)g_game->getResourceLocator()->load(RES_MATERIAL, "materials/debug.mat", NULL); 
}

DebugDrawRenderInterface::~DebugDrawRenderInterface()
{
}

void DebugDrawRenderInterface::drawPointList(const dd::DrawVertex *points, int count, bool depthEnabled)
{
#if 0
	RenderVertex *vert = new RenderVertex[count];
	for(int i = 0; i < count; i++)
	{
		vert[i].pos.x() = points[i].point.x;
		vert[i].pos.y() = points[i].point.y;
		vert[i].pos.z() = points[i].point.z;
		vert[i].colour0[0] = points[i].point.r;
		vert[i].colour0[1] = points[i].point.g;
		vert[i].colour0[2] = points[i].point.b;
		vert[i].colour0[3] = points[i].point.size;
	}
	m_debug_model->meshes()[0].verts = vert;
	m_debug_model->meshes()[0].num_verts = count;

	GLuint *idx = new GLuint[count];
	for(int i = 0; i < count; i++)
	{
		idx[i] = i;
	}
	m_debug_model->meshes()[0].idxs = idx;
	m_debug_model->meshes()[0].num_idxs = count;

	delete[] vert;
	delete[] idx;
#endif
}

void DebugDrawRenderInterface::drawLineList(const dd::DrawVertex *lines, int count, bool depthEnabled)
{
#if 0
	RenderVertex *vert = new RenderVertex[count];
	for(int i = 0; i < count; i++)
	{
		vert[i].pos.x() = lines[i].line.x;
		vert[i].pos.y() = lines[i].line.y;
		vert[i].pos.z() = lines[i].line.z;
		vert[i].colour0[0] = (unsigned char)(lines[i].line.r*255);
		vert[i].colour0[1] = (unsigned char)(lines[i].line.g*255);
		vert[i].colour0[2] = (unsigned char)(lines[i].line.b*255);
		vert[i].colour0[3] = 0;
	}
	vbHandle vbo_handle = m_vbo->allocData(vert, count * sizeof(dd::DrawVertex));

	GLuint *idx = new GLuint[count];
	for(int i = 0; i < count; i++)
	{
		idx[i] = i;
	}
	vbHandle ibo_handle = m_ibo->allocData(idx, count * sizeof(GLuint));

	ViewMesh mesh = {vbo_handle, ibo_handle, NULL, m_mvp};

	m_shader->use();
	glUniformMatrix4fv(m_mvp_loc,1, GL_FALSE, m_mvp.data());
	g_game->getRenderer()->drawElements(mesh, GL_LINES);
	glUseProgram(0);

	delete[] vert;
	delete[] idx;
#endif
}

void DebugDrawRenderInterface::endDraw()
{
}
