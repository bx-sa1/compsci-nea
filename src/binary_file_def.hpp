#ifndef BINARY_FILE_DEF_H_
#define BINARY_FILE_DEF_H_

#include <stdint.h>

/**
 * .MAP FILE
**/
#define CHUNKS 6
#define MAP_MAGIC 0x4d464631

struct mapChunk
{
    uint32_t length; //length of data in bytes
    uint32_t data_ptr; //offset to data somewhere else in file
};

struct mapHeader
{
    uint32_t magic; //special identifier for file
    uint32_t version; //format version
    mapChunk chunks[CHUNKS]; //list of chunks
};

struct mapModel //0
{
    uint32_t mesh_start, mesh_num;
};

struct mapMesh //1
{
    uint32_t material;

    uint32_t idx_start, idx_num;
    uint32_t vert_start, vert_num;

	float max[3], min[3];
};

struct mapIndex{}; //2 uint16_t

struct mapMaterial{}; //3 32 char string

struct mapVertex//4
{
    float x, y, z; //float
    float u, v; //float
    float nx, ny, nz; //float
};

struct mapEntityDef{}; //5

#endif
