#ifndef OCTREE_H_
#define OCTREE_H_

#include "maths/aabb.hpp"
#include "maths/vector.hpp"
#include <list>

template <class T>
struct OctreeItem
{
    T m_item;
    AABB m_bounds;

    OctreeItem(T item, AABB bounds) : m_item(item), m_bounds(bounds) {}
};

template <class T>
class OctreeNode
{
    std::list<OctreeItem<T>> m_items;
    OctreeNode<T> *m_children;

    AABB m_bounds;
public:
    OctreeNode() : m_children(NULL) {}
    OctreeNode(AABB bounds) : m_bounds(bounds), m_children(NULL) {}
    ~OctreeNode() { if(m_children) delete[] m_children; }

    void split()
    {
	m_children = new OctreeNode<T>[8];

	//TLF
	m_children[0] = OctreeNode<T>(AABB(m_bounds.center+Vec3(-m_bounds.half_extent.x, m_bounds.half_extent.y, -m_bounds.half_extent.z),
						  m_bounds.half_extent/2));
	//TRF
	m_children[1] = OctreeNode<T>(AABB(m_bounds.center+Vec3(m_bounds.half_extent.x, m_bounds.half_extent.y, -m_bounds.half_extent.z),
						  m_bounds.half_extent/2));
	//TLB
	m_children[2] = OctreeNode<T>(AABB(m_bounds.center+Vec3(-m_bounds.half_extent.x, m_bounds.half_extent.y, m_bounds.half_extent.z),
						  m_bounds.half_extent/2));
	//TRB
	m_children[3] = OctreeNode<T>(AABB(m_bounds.center+Vec3(m_bounds.half_extent.x, m_bounds.half_extent.y, m_bounds.half_extent.z),
						  m_bounds.half_extent/2));
	//BLF
	m_children[4] = OctreeNode<T>(AABB(m_bounds.center+Vec3(-m_bounds.half_extent.x, -m_bounds.half_extent.y, -m_bounds.half_extent.z),
						  m_bounds.half_extent/2));
	//BRF
	m_children[5] = OctreeNode<T>(AABB(m_bounds.center+Vec3(m_bounds.half_extent.x, -m_bounds.half_extent.y, -m_bounds.half_extent.z),
						  m_bounds.half_extent/2));
	//BLB
	m_children[6] = OctreeNode<T>(AABB(m_bounds.center+Vec3(-m_bounds.half_extent.x, -m_bounds.half_extent.y, m_bounds.half_extent.z),
						  m_bounds.half_extent/2));
	//BRB
	m_children[7] = OctreeNode<T>(AABB(m_bounds.center+Vec3(m_bounds.half_extent.x, -m_bounds.half_extent.y, m_bounds.half_extent.z),
						  m_bounds.half_extent/2));
    }
    
    void insert(T &object, AABB(bounds), int capacity, int depth_level /* 0 = leaf */)
    {
	if(!m_bounds.intersects_or_touches(bounds)) return;

	if(m_children)
	{
	    //CHECK CHILDREN
	    for(int i = 0; i < 8; i++)
	    {
		m_children[i].insert(object, bounds, capacity, depth_level - 1);
	    }
	}
	else
	{
	    m_items.push_back(OctreeItem<T>(object, bounds));
	    if(m_items.size() > capacity && depth_level > 0) //split condition
	    {
		split();

		//push contents down
		for(auto &item : m_items)
		{
		    for(int i = 0; i < 8; i++)
		    {
			m_children[i].insert(item.m_item, bounds, capacity, depth_level-1);
		    }
		}
	    }
	}
    }
	    
			     
};
    
template <class T>
class Octree
{
    OctreeNode<T> m_root;
    int m_max_depth, m_capacity;
public:
    Octree(Vec3 size, int capacity, int maxDepth) :
	m_root(AABB(Vec3(), size/2)),
	m_capacity(capacity),
	m_max_depth(maxDepth) {}

    void insert(T &object, AABB bounds)
    {
	m_root.insert(object, bounds, m_capacity, m_max_depth);
    }
	
    OctreeNode<T> getRoot()
    {
	return m_root;
    }
};

#endif
