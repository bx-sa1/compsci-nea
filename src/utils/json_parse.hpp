#ifndef JSON_PARSE_H_
#define JSON_PARSE_H_

#define FIND_KEY(d, name, type) \
	const auto &key_##name = d.FindMember(#name);\
	if(key_##name != d.MemberEnd() && key_##name->value.Is##type())

#endif
