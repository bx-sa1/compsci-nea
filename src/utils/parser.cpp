#include "utils/parser.hpp"

#include <cstdlib>
#include <cstring>

Parser::Parser(const char *ptext)
{
    text = strdup(ptext);
    start = text;
    end = start;
    text_end = text + strlen(text);
}

Parser::~Parser()
{
    free((void*)text);
}

bool Parser::skipWhitespace(int *read)
{
    while(*end <= ' ' || *end == 127)
    {
	if(*end == '\0') return false;
	if(  read) (*read)++;

	end++;
    }
    return true;
}

bool Parser::parseToken(std::string &str, int *read, int *pos)
{
    if(!skipWhitespace(read)) return false;

    if(*end >= '0' && *end <= '9')
    {
		start = end;
		while(*end >= '0' && *end <= '9')
		{
		    if(*end == '\0') return false;
		    if(read) (*read)++;

		    end++;
		}

		str.replace(0, std::string::npos, start, end-start);
    }
    else if((*end == '\"' || *end == '\''))
    {
		start = ++end;
		while((*end != '\"' && *end != '\''))
		{
		    if(*end == '\0') return false;
		    if(*end == '\n') return false;
		    if(read) (*read)++;

		    end++;
		}

		str.replace(0, std::string::npos, start, end-start);
		end++; //skip over trailing quote
    }
    else if((*end >= 'A' && *end <= 'Z') || (*end >= 'a' && *end <= 'z') || *end == '_')
    {
		start = end;
		while((*end >= '0' && *end <= '9') ||
		      (*end >= 'A' && *end <= 'Z') ||
		      (*end >= 'a' && *end <= 'z') ||
		      *end == '_')
		{
		    if(*end == '\0') return false;
		    if(read) (*read)++;
		    
		    end++;
		}

		str.replace(0, std::string::npos, start, end-start);
    }
    else
    {
		start = end;
        while((*end >= '!' && *end <= '/') ||
		      (*end >= ':' && *end <= '@') ||
		      (*end >= '[' && *end <= '`') ||
		      (*end >= '{' && *end <= '~'))
		{
		    if(*end == '\0') return false;
		    if(read) (*read)++;

		    end++;
		}

		str.replace(0, std::string::npos, start, end-start);
    }

    if(pos) *pos = start - text;

    return true;
}

void Parser::parseUntil(std::string &token, std::string until)
{
	std::string str;
	while(parseToken(str) && str != until)
	{
		token += str + " ";
	}
}