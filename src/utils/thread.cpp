#include "thread.hpp"

#include <thread>

void thread_proc(Runnable *r) { r->run(); }

Thread::Thread(std::string name, Runnable *r) : m_runnable(r), t(NULL) {}

Thread::~Thread()
{
    delete t;
    t = NULL;
}

void Thread::start()
{
    if(t != NULL)
    {
       stop();
    }
    
    t = new std::thread(&thread_proc, m_runnable);
}

void Thread::stop()
{
    t->join();
    delete t;
    t = NULL;
}
    
