#include "asset_management_utils.hpp"

#include "file.hpp"
#include "file_system.hpp"
#include "debug/logger.hpp"

namespace assets 
{
	namespace internal
	{
		bool load_file(std::string path, std::string mode, std::vector<unsigned char> &buffer, size_t &read)
		{
			File::Ptr file = filesystem::open_file(path.c_str(), mode.c_str());
			if(!file)
			{
				LOG_ERROR("Failed to open file %s", path.c_str());
				return false;
			}

			size_t size = filesystem::get_file_size(file.get());
			buffer.resize(size);
			read = fread(buffer.data(), sizeof(unsigned char), size, file.get());
			if(!read)
			{
				LOG_ERROR("Failed to read file");
				return false;
			}

			return true;
		}

	};
};
