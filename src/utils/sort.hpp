#ifndef SORT_H_
#define SORT_H_

#include <memory>

/**
 * Generic sort
 **/
template<class T, class Compare>
void merge(T *arr, int first, int middle, int last, Compare comp)
{
    int n1 = middle - first + 1;
    int n2 = last - middle;

    T *L = new T[n1];
    T *R = new T[n2];

	std::copy(arr+first, arr+first+n1, L);
	std::copy(arr+(middle+1), arr+(middle+1)+n2, R);

    int i = 0, j = 0, k = first;
    while(i < n1 && j < n2)
    {
        if(comp(L[i], R[j]))
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while(i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }

    while(j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }

    delete[] L;
    delete[] R;
}

//0  1  2  3  4
//4, 7, 3, 2, 9
template<class T, class Compare>
void merge_sort(T *arr, int first, int last, Compare comp)
{
    if(last <= first)
        return;

    int middle = (first+last)/2;
    merge_sort(arr, first, middle, comp);
    merge_sort(arr, middle+1, last, comp);
    merge(arr, first, middle, last, comp);
}

#endif
