#ifndef UTILS_DEFINES_H_
#define UTILS_DEFINES_H_

#define degtorad(deg) ((double)(deg * PI) / 180.0)
#define radtodeg(rad) ((double)(rad * 180.0) / PI)
#define PI 3.14159265
#define flerp(a, b, f)  (a * (1.0 - f)) + (b * f)

#endif
