#ifndef FILE_H_
#define FILE_H_

#include <cstdio>
#include <memory>

namespace File
{
	typedef std::unique_ptr<std::FILE, void(*)(std::FILE*)> Ptr;

	std::uint8_t read_u8(std::FILE *file);
	std::uint16_t read_u16(std::FILE *file);
	std::uint32_t read_u32(std::FILE *file);
	std::uint64_t read_u64(std::FILE *file);
};

#endif
