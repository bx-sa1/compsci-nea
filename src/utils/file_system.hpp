#ifndef FILESYSTEM_H_
#define FILESYSTEM_H_

#include "file.hpp"

namespace filesystem
{
	std::string get_data_path();
	File::Ptr open_file(const char *path, const char *mode);
	std::size_t get_file_size(std::FILE *file);
	namespace internal { void close_file_ptr(std::FILE *file); };
};

#endif
