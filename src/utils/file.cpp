#include "file.hpp"
#include <stdio.h>
#include "swap.hpp"

namespace File
{
	std::uint8_t read_u8(std::FILE *file)
	{
		std::uint8_t num;
		fread(&num, 1, 1, file);
		return num;
	}

	std::uint16_t read_u16(std::FILE *file)
	{
		std::uint16_t num;
		fread(&num, 1, 2, file);
		return Swap16(num);
	}

	std::uint32_t read_u32(std::FILE *file)
	{
		std::uint32_t num;
		fread(&num, 1, 4, file);
		return Swap32(num);
	}

	std::uint64_t read_u64(std::FILE *file)
	{
		std::uint64_t num;
		fread(&num, 1, 8, file);
		return Swap64(num);
	}
};
