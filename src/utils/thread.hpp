#ifndef THREAD_H_
#define THREAD_H_

#include <string>
#include <thread>

class Runnable
{
public:
    virtual void run() = 0;
};

class Thread
{
public:
    Thread(std::string name, Runnable *r);
    ~Thread();

    void start();
    void stop();
	Runnable *getRunnable() { return m_runnable; }

private:
    Runnable *m_runnable;
    std::thread *t;
};
    
#endif
