#ifndef RESOURCE_LOADER_H_
#define RESOURCE_LOADER_H_

#include <queue>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <atomic>
#include <memory>
#include "resource/resource.hpp"
#include <future>

template<class T>
class Queue
{
public:
    void push(const T &e)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_queue.push_back(e);
    }
    bool pop(T &e)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        if(m_queue.empty()) 
            return false;
        e = m_queue.front();
        m_queue.pop_front();
        return true;
    }
    bool empty()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        return m_queue.empty();
    }
    std::deque<T> &queue() { return m_queue; }
private:
    std::mutex m_mutex;
    std::deque<T> m_queue;
};

class ThreadPool
{
public:
    ThreadPool(int num_threads);

    //add resource to queue
    template<typename F, typename... Args>
    auto push(F &&func, Args &&...args) -> std::future<decltype(func(args...))>
    {
        auto task = std::make_shared<std::packaged_task<decltype(func(args))>>(
            std::bind(std::forward<F>(func), std::forward<Args...>(args))
        );
        auto func = std::function<void()>([task]() {
            (*task)();
        });
        m_queue.push(std::move(func));
        m_cv.notify_one();
        return task->get_future();
    }

private:
    void thread_func()
    {
        while(m_running)
        {
            if(!m_queue.empty())
            {
                std::function<void()> func;
                {
                    std::unique_lock<std::mutex> lock(m_mutex);
                    m_queue.pop(func);
                }

                func();
            }
            else
            {
                std::unique_lock<std::mutex> lock(m_mutex);
                m_waiting++;
                m_cv.wait(lock);
                m_waiting--;
            }
        }
    }

    std::vector<std::unique_ptr<std::thread>> m_threads;
    Queue<std::function<void()>> m_queue;
    std::atomic<int> m_waiting;

    std::mutex m_mutex;
    std::condition_variable m_cv;
    std::atomic<bool> m_running;
};

#endif