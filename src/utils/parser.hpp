#ifndef PARSER_H_
#define PARSER_H_

#include <string>

class Parser
{
public:
    Parser(const char *text);
    ~Parser();

    bool parseToken(std::string &token, int *read = NULL, int *pos = NULL);
    bool skipWhitespace(int *read);
    void parseUntil(std::string &token, std::string until);

private:
    char *text;
    char *start;
    char *end;
    char *text_end;
};
    
#endif
