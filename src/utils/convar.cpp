#include "convar.hpp"
#include <algorithm>
#include <vector>
#include <cstring>

static std::vector<ConVar*> &getConVars()
{
    static std::vector<ConVar*> convars;
    return convars;
}

static void addConVar(ConVar *cv) { getConVars().push_back(cv); }
static void removeConVar(ConVar *cv)
{
    std::vector<ConVar*> &cvs = getConVars();
    cvs.erase(std::remove(cvs.begin(), cvs.end(), cv), cvs.end());
}

ConVar::ConVar(std::string name, float f, std::string help) :
    m_name(name),
    m_value(),
    m_type(VALUE_TYPE_NUMBER),
    m_help(help)
{
    setNumber(f);
    addConVar(this);
}

ConVar::ConVar(std::string name, std::string s, std::string help) :
    m_name(name),
    m_value(),
    m_type(VALUE_TYPE_STRING),
    m_help(help)
{
    setString(s);
    addConVar(this);
}

ConVar::~ConVar()
{
    if(m_type == VALUE_TYPE_STRING)
    {
        free(m_value.s);
    }
    removeConVar(this);
}

float ConVar::getNumber()
{
    if(m_type == VALUE_TYPE_NUMBER)
    {
	   return m_value.f;
    }
    else if(m_type == VALUE_TYPE_STRING)
    {
    	char *end;
    	float f = (float)strtod(m_value.s, &end);
    	if(m_value.s == end) return 0.0f;
    	while(isspace(*end)) end++;
    	if(*end == '\0')
    	{
    	    return f;
    	}
    }

    return 0.0f;
}

std::string ConVar::getString() { return m_value.s; }

bool ConVar::getBool()
{
    if(isNumber())
    {
        return m_value.f;
    }

    return false;
}

namespace utils
{
	ConVar * get_convar(std::string name)
	{
		for(ConVar *cv : getConVars())
		{
			if(cv->getName() == name)
			{
				return cv;
			}
		}
		
		return NULL;
	}
};
	        


