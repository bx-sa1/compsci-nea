#ifndef CONVAR_H_
#define CONVAR_H_

#include <vector>
#include <string>
#include <cstring>

// Console Variable
// choose not to use templates since it increases compile time;
class ConVar
{
public:
    ConVar(std::string name, float f, std::string help = "");
    ConVar(std::string name, std::string s, std::string help = "");
    ~ConVar();

    std::string getName(){ return m_name; }
    float getNumber();
    bool getBool();
    std::string getString();
    std::string getHelp(){ return m_help; }

    bool isString(){ return m_type == VALUE_TYPE_STRING; }
    bool isNumber(){ return m_type == VALUE_TYPE_NUMBER; }

    void setNumber(float f){ m_value.f = f; }
    void setString(std::string s){ free(m_value.s); m_value.s = strdup(s.c_str()); }

private:
    std::string m_name;
    union Value
    {
    	float f;
    	char *s;
    } m_value;
    enum { VALUE_TYPE_NUMBER, VALUE_TYPE_STRING } m_type;
    std::string m_help;
};

namespace utils
{
    ConVar *get_convar(std::string name);
};
    

#endif
