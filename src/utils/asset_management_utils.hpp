#ifndef ASSET_MANAGEMENT_UTILS_H_
#define ASSET_MANAGEMENT_UTILS_H_

#include <unordered_map>
#include <vector>
#include <string>

namespace assets
{
	namespace internal
	{
		template<class T>
		std::unordered_map<std::string, T*> &get_asset_cache()
		{
			static std::unordered_map<std::string, T*> asset_cache;
			return asset_cache;
		}

		bool load_file(std::string path, std::string mode, std::vector<unsigned char> &data, size_t &read);

		template<class T>
		T *find_in_cache(std::unordered_map<std::string, T *> &ac, std::string path)
		{
			auto &cache = get_asset_cache<T>();
			auto asset = cache.find(path);
			if(asset != cache.end())
			{
				return asset->second;
			}

			return NULL;
		}

	};
};

#endif
