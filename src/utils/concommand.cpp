#include "concommand.hpp"
#include "debug/logger.hpp"

#include <vector>
#include <cstring>
#include <algorithm>

std::vector<ConsoleCommand*> &getConCommands()
{
    static std::vector<ConsoleCommand*> concommands;
    return concommands;
}

void addConosoleCommand(ConsoleCommand *cc)
{
    getConCommands().push_back(cc);
}

void removeConsoleCommand(ConsoleCommand *cc)
{
    std::vector<ConsoleCommand*> &ccs = getConCommands();
    ccs.erase(std::remove(ccs.begin(), ccs.end(), cc), ccs.end());
}

ConsoleCommand::ConsoleCommand(std::string name, command cmd, std::string help, command up_cmd) :
    m_name(name),
    m_command(cmd),
    m_help(help),
	m_up_command(up_cmd)
{
    addConosoleCommand(this);
}

ConsoleCommand::~ConsoleCommand()
{
    removeConsoleCommand(this);
}

namespace utils
{
	ConsoleCommand *get_concommand(std::string name)
	{
		std::vector<ConsoleCommand*> cmds = getConCommands();

		for(int i = 0; i < cmds.size(); i++)
		{
			ConsoleCommand *cc = cmds[i];
			if(cc->getName() == name)
			{
				return cc;
			}
		}
		
		return NULL;
	}

	enum TokenState
	{
		COMMAND,
		ARGS
	};

	// COMMAND [ARGS]
	//delete the returned value after use
	TokenisedCommand *tokenize_command(std::string full_command)
	{
		const char *start = full_command.c_str();
		const char *end = start;
		TokenState state = COMMAND;
		std::vector<std::string> argv_buffer;
		TokenisedCommand *cmd = new TokenisedCommand;
		cmd->argv = NULL;
		cmd->argc = 0;

		while(*end != '\0')
		{
			//skip_whitespace
			while(*(end) <= ' ' || *end == 127)
			{
				if(*end == '\0') goto exit_loop;
				end++;
			}

			switch(state)
			{
			case COMMAND:
				{
					//continue until next whitespace
					start = end;
					int count = 0;
					while(!(*end <= ' ' || *end == 127))
					{
						count++;
						end++;
					}

					cmd->command = std::string(start, count);
					state = ARGS;
					break;
				}
			case ARGS:
				{
					//cotinue until white space, or next quotation mark
					if(*end == '\"' || *end == '\'')
					{
						//continue until next quotation mark
						start = ++end;
						int count = 0;

						while(!(*end == '\"' || *end == '\''))
						{
							if(*end == '\0') 
							{
								LOG_WARNING("ConsoleCommand Tokenise warning: invalid end of string");
								goto exit_loop;
							}
							count++;
							end++;
						}

						cmd->argc++;
						argv_buffer.push_back(std::string(start, count));
						state = ARGS;

						count++;
						end++; //skip over last quote mark
						break;
					}
					else
					{
						//continue until next whitespace
						start = end;
						int count = 0;

						while(!(*end <= ' ' || *end == 127))
						{
							count++;
							end++;
						}

						cmd->argc++;
						argv_buffer.push_back(std::string(start, count));
						state = ARGS;
						break;
					}
				}
			}
		}
		exit_loop:

		if(state == COMMAND) //still in command state, therefore there was an error while parsing
		{
			delete cmd;
			return NULL;
		}

		//add argv_buffer to argv
		cmd->argv = new std::string[cmd->argc];
		for(int i = 0; i < cmd->argc; i++)
		{
			cmd->argv[i] = argv_buffer[i]; 
		}

		return cmd;
	}
};


