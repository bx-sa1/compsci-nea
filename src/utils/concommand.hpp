#ifndef CONCOMMAND_H_
#define CONCOMMAND_H_

#include <vector>
#include <string>

typedef void (*command)(int argc, std::string argv[]);
class ConsoleCommand
{
    friend class ConsoleCommandManager;
public:
    ConsoleCommand(std::string name, command cmd, std::string help = "", command up_cmd = NULL);
    ~ConsoleCommand();

    std::string getName() { return m_name; }
    command getCommand() { return m_command; }
	command getUpCommand() { return m_up_command; }
    std::string getHelp() { return m_help; }
private:
    std::string m_name;
    command m_command;
	command m_up_command;
    std::string m_help;
};

struct TokenisedCommand
{
    ~TokenisedCommand(){delete[] argv;}
    std::string command;
    int argc;
    std::string *argv;
};

namespace utils
{
    ConsoleCommand *get_concommand(std::string name);
	TokenisedCommand *tokenize_command(std::string full_command);
};

#endif
