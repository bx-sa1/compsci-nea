#include "frustum.hpp"

bool Frustum::aabb_test(const Eigen::AlignedBox3f &aabb)
{
    for(int i = 0; i < 6; i++)
    {
        Eigen::Hyperplane<float, 3> &p = planes[i];

        float d = aabb.center().dot(p.normal());
        float r = (aabb.max()-aabb.center()).dot(p.normal().cwiseAbs());
    
        if(d + r < -p.offset()) return false;//outside frustum
    }

    return true;
}