#include "file_system.hpp"

#include "convar.hpp"
#include "SDL2/SDL.h"
#include "sys/stat.h"

ConVar cv_game("game", "base", "Game name");

std::string filesystem::get_data_path()
{
    std::string base = SDL_GetBasePath();

	return base + "data/" + cv_game.getString() + "/";
}

File::Ptr filesystem::open_file(const char *path, const char *mode)
{
	std::string full_path = get_data_path() + path;
	return File::Ptr(std::fopen(full_path.c_str(), mode), &internal::close_file_ptr);
}

size_t filesystem::get_file_size(std::FILE *file)
{
	fseek(file, 0, SEEK_END);
	size_t size = ftell(file);
	fseek(file, 0, SEEK_SET);
	return size;
}

void filesystem::internal::close_file_ptr(std::FILE *file)
{
	std::fclose(file);
}

