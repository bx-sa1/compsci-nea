#ifndef FRUSTUM_H_
#define FRUSTUM_H_

#include "eigen/Eigen"

class Frustum
{
public:
    bool aabb_test(const Eigen::AlignedBox3f &aabb);

    Eigen::Hyperplane<float, 3> planes[6];
};

#endif