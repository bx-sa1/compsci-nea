#include "menus.hpp"
#include "utils/concommand.hpp"
#include "utils/convar.hpp"
#include "game.hpp"
#include "renderer/material.hpp"

namespace Menus
{
	void draw_main_menu(nk_context *ctx)
	{
		Material *menu_banner_mat = assets::load_material("materials/ui/menu_banner.mat");

		int width = utils::get_convar("res_width")->getNumber();
		int height = utils::get_convar("res_height")->getNumber();
		if(nk_begin(ctx, "main_menu", nk_rect(0, 0, width, height), NK_WINDOW_NO_SCROLLBAR))
		{
			//banner
			nk_layout_space_begin(ctx, NK_DYNAMIC, height, INT_MAX);
			nk_layout_space_push(ctx, nk_rect(0.2, 0.1, 0.6, 0.3)); //width /5 height /10
			nk_image(ctx, nk_image_ptr(menu_banner_mat));

			//buttons
			nk_layout_space_push(ctx, nk_rect(0.4, 0.5, 0.2, 0.1*3)); //width /5 height /10
			if(nk_group_begin(ctx, "main_menu_buttons", 0))
			{
				nk_layout_row_dynamic(ctx, 0, 1);
				if(nk_button_label(ctx, "Start Game"))
				{
				}
				if(nk_button_label(ctx, "Options"))
				{
				}
				if(nk_button_label(ctx, "Exit"))
				{
				}
				nk_group_end(ctx);
			}
			nk_layout_space_end(ctx);
		}
		nk_end(ctx);
	}

	void draw_console(nk_context *ctx)
	{
		static char buffer[512];
		static int buffer_len;	
		static char input[64];
		static int input_len;

		if(nk_begin(ctx, "Console", nk_rect(0, 0, 200, 200), NK_WINDOW_BORDER | NK_WINDOW_CLOSABLE | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE))
		{
			nk_layout_row_dynamic(ctx, 120, 1);
			nk_edit_string(ctx, NK_EDIT_READ_ONLY | NK_EDIT_BOX, buffer, &buffer_len, 512, nk_filter_default);

			nk_layout_row_dynamic(ctx, 0, 2);
			bool active = nk_edit_string(ctx, NK_EDIT_FIELD | NK_EDIT_SIG_ENTER, input, &input_len, 64, nk_filter_ascii);
			if(nk_button_label(ctx, "Submit") || 
					(active & NK_EDIT_COMMITED))
			{
				//execute
				TokenisedCommand *tokens = utils::tokenize_command(input);
				ConsoleCommand *cmd = utils::get_concommand(tokens->command);
				cmd->getCommand()(tokens->argc, tokens->argv);
				//add to buffer
				input[input_len] = '\n';
				input_len++;
				memcpy(&buffer[buffer_len], input, input_len);
				buffer_len += input_len;
				input_len = 0;
			}
		}
		nk_end(ctx);
	}

	void draw_pause_menu(nk_context *ctx)
	{
		int width = utils::get_convar("res_width")->getNumber();
		int height = utils::get_convar("res_height")->getNumber();
		if(nk_begin(ctx, "pause", nk_rect(0, 0, width, height), NK_WINDOW_NO_SCROLLBAR))
		{
			//banner
			nk_layout_space_begin(ctx, NK_DYNAMIC, height, INT_MAX);
			nk_layout_space_push(ctx, nk_rect(0.2, 0.1, 0.6, 0.3)); //width /5 height /10
			//nk_image(ctx, nk_image_ptr(menu_banner_mat));

			//buttons
			nk_layout_space_push(ctx, nk_rect(0.4, 0.5, 0.2, 0.1*3)); //width /5 height /10
			if(nk_group_begin(ctx, "main_menu_buttons", 0))
			{
				nk_layout_row_dynamic(ctx, 0, 1);
				if(nk_button_label(ctx, "Resume"))
				{
				}
				if(nk_button_label(ctx, "Options"))
				{
				}
				if(nk_button_label(ctx, "Return to menu"))
				{
				}
				nk_group_end(ctx);
			}
			nk_layout_space_end(ctx);
		}

		nk_end(ctx);

	}

	void draw_loading_screen(nk_context *ctx, std::string loading_status)
	{

	}
};
