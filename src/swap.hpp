#ifndef SWAP_H_
#define SWAP_H_

#include <cstdint>
#include <cstring>

// very important:
// https://commandcenter.blogspot.com/2012/04/byte-order-fallacy.html

inline uint16_t Swap16(uint16_t v)
{
    uint8_t data[2] = {};
    memcpy(data, &v, sizeof(data));

    return ((uint16_t) data[0] << 0)
	| ((uint16_t) data[1] << 8);
}
			       
inline uint32_t Swap32(uint32_t v)
{
    uint8_t data[4] = {};
    memcpy(data, &v, sizeof(data));

    return ((uint32_t) data[0] << 0)
	| ((uint32_t) data[1] << 8)
	| ((uint32_t) data[2] << 16)
	| ((uint32_t) data[3] << 24);
}

inline uint64_t Swap64(uint64_t v)
{
    uint8_t data[8] = {};
    memcpy(data, &v, sizeof(data));

    return ((uint64_t) data[0] << 0)
	| ((uint64_t) data[1] << 8)
	| ((uint64_t) data[2] << 16)
	| ((uint64_t) data[3] << 24)
	| ((uint64_t) data[4] << 32)
	| ((uint64_t) data[5] << 40)
	| ((uint64_t) data[6] << 48)
	| ((uint64_t) data[7] << 56);
}

#endif
