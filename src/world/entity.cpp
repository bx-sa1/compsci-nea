#include "world/entity.hpp"

#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "physics/physics.hpp"
#include "game.hpp"
#include "renderer/model.hpp"
#include "world/world.hpp"
#include <unordered_map>
#include "utils/json_parse.hpp"
#include "world/map.hpp"
#include "physics/motion_state.hpp"

Entity::Entity() : 
    m_model(NULL),
    m_collision_object(NULL), 
    m_collision_model(NULL),
    ent_hidden(false),
    ent_no_collision(false),
    m_transform(Eigen::Affine3f::Identity())
{

}

Entity::~Entity()
{
	g_game->getWorld()->scene().remove_model(m_model);
	m_model = NULL;
	m_collision_model = NULL;

	g_game->getWorld()->physics()->removeFromWorld(m_collision_object);
    delete m_collision_object;
    m_collision_object = NULL;
}

void Entity::show()
{
	if(!m_model) return;
	g_game->getWorld()->scene().add_model(m_model);
	ent_hidden = false;
}

void Entity::hide()
{
	if(!m_model) return;
	g_game->getWorld()->scene().remove_model(m_model);
	ent_hidden = true;
}

void Entity::spawn(const SpawnArgs &args)
{
    //models
    std::string model_name, collision_model_name;
    FIND_KEY(args, model, String)
    {
        model_name = key_model->value.GetString();
        m_model = assets::load_model(model_name);
    }   
    else
    {
        if(key_model != args.MemberEnd())
        {
            Model *m = g_game->getWorld()->current_map()->static_models()[key_model->value.GetInt()];
            m_model = m;
        }     
    }

    FIND_KEY(args, no_collision, Bool)
    {
        ent_no_collision = key_no_collision->value.GetBool();
    }

    if(!ent_no_collision)
    {
        FIND_KEY(args, collision_model, String)
        {
            collision_model_name = key_collision_model->value.GetString();
            m_collision_model = assets::load_model(collision_model_name);
        }
        else if(m_model)
        {
            m_collision_model = m_model;
        }
    }

    //position
    FIND_KEY(args, position, Array)
    {
        Eigen::Vector3f p;
        p << key_position->value[0].GetFloat(),
             key_position->value[1].GetFloat(),
             key_position->value[2].GetFloat();
        m_transform.translate(p);
    }

    //rotation
    FIND_KEY(args, rotation, Array)
    {
        m_transform.rotate(Eigen::AngleAxisf(key_rotation->value[2].GetFloat(), Eigen::Vector3f::UnitZ()) *
            Eigen::AngleAxisf(key_rotation->value[1].GetFloat(), Eigen::Vector3f::UnitY()) *
            Eigen::AngleAxisf(key_rotation->value[0].GetFloat(), Eigen::Vector3f::UnitX()));
    }

	show();
}

void Entity::tick()
{
    entity_tick();

	render();


	if(m_model) m_model->model_matrix = m_transform.matrix();
}

void Entity::initCollisionObject(CollisionObjectType type, CollisionObjectInitArgs *args)
{
	switch(type)
	{
		case CO_GHOST:
		{
			m_collision_object = Physics_CreateGhostObject(Physics_ModelToStaticCollisionShape(m_collision_model), Physics_EToBT_TRANSFORM(getTransform()));
			g_game->getWorld()->physics()->addToWorld(m_collision_object);
			break;
		}
		case CO_RIGIDBODY_STATIC:
		{
			m_collision_object = Physics_CreateStaticObject(Physics_ModelToStaticCollisionShape(m_collision_model), new EntityMotionState(this));	
			g_game->getWorld()->physics()->addToWorld(m_collision_object);
			break;
		}
		case CO_RIGIDBODY_KINEMATIC:
		{
			m_collision_object = Physics_CreateKinematicObject(Physics_ModelToDynamicCollisionShape(m_collision_model), new EntityMotionState(this));	
			g_game->getWorld()->physics()->addToWorld(m_collision_object);
			break;
		}
		case CO_RIGIDBODY_DYNAMIC:
		{
			m_collision_object = Physics_CreateDynamicObject(args ? args->mass : 1.0f, Physics_ModelToDynamicCollisionShape(m_collision_model), new EntityMotionState(this));	
			g_game->getWorld()->physics()->addToWorld(m_collision_object);
			break;
		}
	}
	m_collision_object->setUserPointer(this);
}

void Entity::send_event(std::string event)
{
	auto itr = m_observers.begin();
	while(itr != m_observers.end())
	{
		Entity *ent = g_game->getWorld()->get_entity(*itr);
		if(!ent)
		{
			itr = m_observers.erase(itr);
		}
		else
		{
			ent->onEvent(name, event);
			itr++;
		}
	}
}

void Entity::add_observer(std::string observer)
{
	m_observers.push_back(observer);
}

typedef std::unordered_map<std::string, EntityTypeInfo*> EntityMap;
	    
static EntityMap &getEntityMap()
{
    static EntityMap cm;

    return cm;
}

Entity *Entity::instantiate(const std::string &name)
{
    EntityMap &cm = getEntityMap();

    auto entity = cm.find(name);
    if(entity == cm.end()) return NULL;

    return entity->second->instantiate();
}

EntityTypeInfo::EntityTypeInfo(const std::string &name, const std::string &classname, Entity *(*inst)())
{
    EntityMap &cm = getEntityMap();

    this->name = name;
    this->classname = classname;
    this->instantiate = inst;

    cm[name] = this;
}

DEFINE_ENTITY(ent_null, ENull)
