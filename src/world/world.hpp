#ifndef WORLD_H_
#define WORLD_H_

#include <map>
#include <string>
#include "entity.hpp"
#include "renderer/renderer.hpp"
#include "renderer/view.hpp"
#include "renderer/scene.hpp"

class EPlayer;
class Model;
class Map;
class Physics;

/**
 * World class.
 * This holds all the entities and iterates over them each frame.
 * It essentially the central hub for all the entities.
 **/
class World
{
public:
    World();
    ~World();

    void clear();

    /**
     * Initialize world from map file.
     * If map is NULL, just clears the maps
    **/
    bool initFromMap(Map *map);
    
    /**
     * Advance the world simulation by
     * one frame 
    **/
    void tick();

    /**
     * Creates ViewDefs from the list of 
     * views in the world and uploads models and
     * lights to the viewdefs
    **/
    void render();

    /**
     * Spawns an entity with a given name
     * from JSON entity definition
     **/
    Entity *spawn_entity(const std::string &name, rapidjson::Value &ent);
    EPlayer *spawn_player();
    void kill_entity(const std::string &name);

    void add_view(const View &view) { m_views.push_back(view); }
    void add_player_start_point(const Eigen::Affine3f &transform) { m_player_start_points.push_back(transform); }

    const Physics *physics() const { return m_physics; }
	Scene &scene() { return m_scene; }
    EPlayer *client_player() const
	{	
		auto player = m_entities.find("player_0");
		if(player != m_entities.end())
			return (EPlayer*)player->second;
		return NULL;
	}
	const Map *current_map() const { return m_current_map; }
	std::vector<LightInfo> &lights() { return m_lights; }
	Entity *get_entity(std::string name);

private:
	Scene m_scene;
    Physics *m_physics; //physics
	Map *m_current_map;

    bool ParseWorld(char *data, int size); //parse world json file

    std::map<std::string, Entity *> m_entities; //name to entity mapping

    std::vector<LightInfo> m_lights; //lights to use for rendering
    std::vector<View> m_views; //views to render from
    std::vector<Eigen::Affine3f> m_player_start_points; //player start points
    
    //Eigen::Vector3f m_world_size; //world size to the nearest power of two
    int player_num; //number of players
};

#endif
