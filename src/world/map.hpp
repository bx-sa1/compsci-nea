#ifndef MAP_H_
#define MAP_H_

#include "SDL2/SDL_rwops.h"
#include <vector>
#include <string>
#include "rapidjson/document.h"

class Entity;
class Model;

/*
 * Class representation of the map file
 * Loads the map file into a vector of static models and
 * a string of the entity def
 */
class Map
{
public:
	Map();
	~Map();

	bool load_from_file(std::string file);

	const std::vector<Model*> static_models() const { return m_static_models; }
	rapidjson::Document &entities() { return m_entities; }
private:
	std::vector<Model*> m_static_models;
	rapidjson::Document m_entities;
};

#endif
