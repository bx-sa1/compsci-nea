#ifndef ENTITY_H_
#define ENTITY_H_

#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "debug/logger.hpp"
#include "rapidjson/document.h"
#include <memory>
#include <vector>
#include "physics/physics.hpp"
#include <string>
#include "renderer/model.hpp"

//call inside class declaration inside private space
#define DECLARE_ENTITY(type)                                \
    static Entity *instantiate();       \
    static EntityTypeInfo typeinfo;     \
    EntityTypeInfo *getTypeInfo();                   

//call in correspoding source file, anywhere
#define DEFINE_ENTITY(name, type)                                                 \
  EntityTypeInfo type::typeinfo(#name, #type, type::instantiate);                  \
  Entity *type::instantiate() { return new type; }                          \
  EntityTypeInfo *type::getTypeInfo() { return &typeinfo; }


struct EntityTypeInfo;

typedef rapidjson::Value SpawnArgs;

enum CollisionObjectType
{
	CO_GHOST, //collision detection, no response, no movement, allows you to find what collided with it
    CO_RIGIDBODY_STATIC, //collision detection, no response, no movement
    CO_RIGIDBODY_KINEMATIC, //no collision detection, no response, movement, can push dynamics
    CO_RIGIDBODY_DYNAMIC //collision detection, response, movement
};

struct CollisionObjectInitArgs
{
	float mass;
};

/**
    Base class for entities
    Entities are what the world is made up of
**/
class Entity
{
public:
    Entity();
    ~Entity();

    void tick(); //tick entity

    virtual void spawn(const SpawnArgs &args);//spawn entity
    virtual EntityTypeInfo *getTypeInfo() { return NULL; } //get entity class type info

    static Entity *instantiate(const std::string &name); //instantiate entity from string

    Eigen::Affine3f &getTransform() { return m_transform; }
    Model *getModel() { return m_model; }
    void setModel(Model *model) { m_model = model; }
    Model *getCollisionModel() { return m_collision_model; }
    void setCollisionModel(Model *model) { m_collision_model = model; }
    btCollisionObject *getCollisionObject() { return m_collision_object; }
	CollisionObjectType getCollisionObjectType() { return m_co_type; }
	void show();
	void hide();
	bool hidden() { return ent_hidden; }

	virtual void onPicked() { LOG_INFO("PICK TEST, ENTITY: %s", name.c_str()); } //override to control what happens when picked
	virtual void onEvent(std::string entity, std::string event) {}

	void add_observer(std::string observer);

    std::string name; //entity name

protected:
    virtual void entity_tick() { } //overriden tick method for entity
    virtual void render(){ } //performs rendering stuff doesnt actually render anything
    void initCollisionObject(CollisionObjectType type, CollisionObjectInitArgs *args = NULL);
	void send_event(std::string event);

private:
    Eigen::Affine3f m_transform; //transform for entity
    Model *m_model; //visual representation
    Model *m_collision_model; //entities collision model
    btCollisionObject *m_collision_object; //physics representation
	CollisionObjectType m_co_type;

    bool ent_hidden; //is the entity hidden
    bool ent_no_collision; //does the entity collide with others

	std::vector<std::string> m_observers;
};

//structure that holds the relevant information needed for dynamic 
//instancing of entities
struct EntityTypeInfo
{
    std::string name; //the name should be used to instantiate the class
    std::string classname; //the name of the class that should be instantiated
    Entity *(*instantiate)(); //a function that instatiates the class and returns it

    EntityTypeInfo(const std::string &name, const std::string &classname, Entity *(*instantiate)());
};

//null entity, does absolutely nothing but has a position
//useful as a target for something else
class ENull : public Entity
{
    DECLARE_ENTITY(ENull)
public:
    void spawn(const SpawnArgs &args){ Entity::spawn(args); }
    void entity_tick(){}
    void render(){}
};


#endif
