#include "player_spawn.hpp"
#include "game.hpp"
#include "world/entity.hpp"
#include "world/world.hpp"

DEFINE_ENTITY(ent_player_spawn, EPlayerSpawn)

EPlayerSpawn::EPlayerSpawn()
{

}

EPlayerSpawn::~EPlayerSpawn()
{

}

void EPlayerSpawn::spawn(const SpawnArgs &args)
{
	Entity::spawn(args);

	//add spawn point to world list
	g_game->getWorld()->add_player_start_point(getTransform());
}