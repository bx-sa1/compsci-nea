#include "mover.hpp"

#include "game.hpp"
#include "utils/json_parse.hpp"
#include "world/entity.hpp"
#include "world/world.hpp"
#include "utils/defines.hpp"

DEFINE_ENTITY(ent_mover_activated, EMoverActivated);

EMoverBase::EMoverBase() :
	m_target(NULL),
	m_move_speed(0),
	m_move_state(NOT_MOVING)
{
}

EMoverBase::~EMoverBase()
{
}

void EMoverBase::spawn(const SpawnArgs &args)
{
	Entity::spawn(args);

	initCollisionObject(CO_RIGIDBODY_KINEMATIC);

	FIND_KEY(args, move_target, String)
	{
		std::string target = key_move_target->value.GetString();
		Entity *ent_target = g_game->getWorld()->get_entity(target);
		if(!ent_target)
		{
			LOG_WARNING("%s not an entity for activation", target.c_str());
		}
		else
		{
			m_target = ent_target;
		}
	}

	FIND_KEY(args, move_speed, Float)
	{
		m_move_speed = key_move_speed->value.GetFloat();
	}
}

void EMoverBase::entity_tick()
{
	return;
	static float time = 0;
	switch(m_move_state)
	{
		case NOT_MOVING:
		{
			time = 0;
			break;
		}
		case MOVING:
		{
			Eigen::Vector3f start = getTransform().translation();
			Eigen::Vector3f end = m_target->getTransform().translation();
			float total_time = (end-start).norm() / m_move_speed;
			getTransform().translation().x() = flerp(start.x(), end.x(), (total_time-time)/total_time);
			getTransform().translation().y() = flerp(start.y(), end.y(), (total_time-time)/total_time);
			getTransform().translation().z() = flerp(start.z(), end.z(), (total_time-time)/total_time);
			if(time >= total_time)
			{
				m_move_state = NOT_MOVING;
			}
			time += g_game->m_frametime;
			break;
		}
		default:
		{
			assert(false);
		}
	}
}

void EMoverBase::begin_move()
{
	m_move_state = MOVING;
}
	EMoverActivated::EMoverActivated()
{
}

EMoverActivated::~EMoverActivated()
{
}

void EMoverActivated::spawn(const SpawnArgs &args)
{
	EMoverBase::spawn(args);

	FIND_KEY(args, activator, String)
	{
		std::string activator = key_activator->value.GetString();
		Entity *ent_activator = g_game->getWorld()->get_entity(activator);
		if(!ent_activator)
		{
			LOG_WARNING("%s not an entity for activation", activator.c_str());
		}
		else
		{
			ent_activator->add_observer(name);
			m_activator = activator;
		}
	}
}

void EMoverActivated::onEvent(std::string entity, std::string event)
{
	if(event == "trigger_activated")
	{
		assert(m_activator == entity);

		begin_move();
	}
	else if(event == "trigger_exit")
	{
		assert(m_activator == entity);
		m_move_state = NOT_MOVING;
	}
}


