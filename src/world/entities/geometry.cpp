#include "world/entities/geometry.hpp"

#include "world/entity.hpp"

DEFINE_ENTITY(ent_geometry, EGeometry)

EGeometry::EGeometry() {}

EGeometry::~EGeometry() 
{
	
}

void EGeometry::spawn(const SpawnArgs &args)
{
	Entity::spawn(args);

	initCollisionObject(CO_RIGIDBODY_STATIC);
}
