#ifndef MOVER_H_
#define MOVER_H_

#include "world/entity.hpp"

enum MoveState
{
	NOT_MOVING,
	MOVING
};

class EMoverBase : public Entity
{
public:
	EMoverBase();
	virtual ~EMoverBase();

	virtual void spawn(const SpawnArgs &args);
	void entity_tick();
	virtual void onEvent(std::string entity, std::string event) {}
protected:
	void begin_move();

	Entity *m_target;
	float m_move_speed;
	MoveState m_move_state;
};

class EMoverActivated : public EMoverBase
{
	DECLARE_ENTITY(EMoverActivated)
public:
	EMoverActivated();
	~EMoverActivated();

	void spawn(const SpawnArgs &args);
	void onEvent(std::string entity, std::string event);
private:
	std::string m_activator;
};

#endif
