#ifndef PLAYER_H_
#define PLAYER_H_

#include "Eigen/Eigen"
#include "utils/convar.hpp"
#include "physics/character_controller.hpp"
#include "renderer/view.hpp"
#include "utils/concommand.hpp"
#include "world/entity.hpp"

class EPlayer : public Entity
{
    DECLARE_ENTITY(EPlayer)
public:
    EPlayer();
    ~EPlayer();

    void spawn(const SpawnArgs &args);

    void entity_tick();
    void render();
	void draw_hud();

	void ground_move(); //move the based on their move type
	void air_move();
	Entity *picking() const; //pick an object in the world by view dir and return it
	
    void setForwardMove(int mag);
    void setRightMove(int mag);
    
private:
	void sync_transforms();

    int mdelta_x, mdelta_y;
    Eigen::AngleAxisf yaw, pitch, roll;
    Eigen::Vector3f local_move_dir;
    Eigen::Vector3f eye;
    View player_view;
	CharacterController *m_controller;
	Eigen::Vector3f m_velocity;
	btVector3 m_controller_velocity;

    std::string xh_full_path;
};

#endif
