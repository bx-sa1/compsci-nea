#ifndef PHYSOBJ_H_
#define PHYSOBJ_H_

#include "world/entity.hpp"

class EPhysObj : public Entity
{
	DECLARE_ENTITY(EPhysObj)
public:
	EPhysObj();
	~EPhysObj();

	void spawn(const SpawnArgs &args);
	void entity_tick();
};

#endif