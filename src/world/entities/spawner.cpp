#include "world/entities/spawner.hpp"

#include "debug/logger.hpp"
#include "game.hpp"
#include "world/entity.hpp"
#include "world/world.hpp"
#include "utils/json_parse.hpp"

DEFINE_ENTITY(ent_spawner, ESpawner)

ESpawner::ESpawner() : count(1), freq(0) {}

ESpawner::~ESpawner() {}

void ESpawner::spawn(const SpawnArgs &args)
{
    FIND_KEY(args, spawn_entity, String)
    {
        spawn_entity = key_spawn_entity->value.GetString();
    }

    FIND_KEY(args, count, Number)
    {
        count = key_count->value.GetFloat();
    }

    FIND_KEY(args, spawn_entity_args, Object)
    {
        //spawn_entity_args = key_spawn_entity_args->value.GetObject();
    }

    //spawn the entity now if count is 1
    if(count == 1) 
    {
        
    }
}

void ESpawner::entity_tick()
{
    if(freq > 0 || count)
    {
        count--;
    }
}
