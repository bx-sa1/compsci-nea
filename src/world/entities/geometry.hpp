#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#include "world/entity.hpp"

/*
  a static entity that cannot be moved but can be collided with
*/

class EGeometry : public Entity
{
    DECLARE_ENTITY(EGeometry);
public:
    EGeometry();
    ~EGeometry();
    
    void spawn(const SpawnArgs &args);
};
    
#endif
