#ifndef PLAYER_SPAWN_H_
#define PLAYER_SPAWN_H_

#include "world/entity.hpp"

class EPlayerSpawn : public Entity
{
	DECLARE_ENTITY(EPlayerSpawn);
public:
	EPlayerSpawn();
	~EPlayerSpawn();

	void spawn(const SpawnArgs &args);
};

#endif