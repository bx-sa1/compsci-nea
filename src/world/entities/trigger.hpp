#ifndef TRIGGER_H_
#define TRIGGER_H_

#include "world/entity.hpp"

struct TriggerCollisionInfo
{
	Entity *other;
};

class ETriggerBase : public Entity
{
public:
	ETriggerBase();
	virtual	~ETriggerBase();

	virtual void spawn(const SpawnArgs &args);
	void entity_tick(); //used for collision detection
	virtual void onTrigger(TriggerCollisionInfo &info) = 0;

	bool active; //is the trigger active or inactive
	bool fired;
};

class ETrigger : public ETriggerBase
{
	DECLARE_ENTITY(ETrigger)
public:
	ETrigger();
	~ETrigger();

	void spawn(const SpawnArgs &args);
	void onTrigger(TriggerCollisionInfo &info);

	int count; //how many times the trigger has been fired
	int maxcount; //how many times it can be fired
	float wait_time; //time between refire
	bool once;

};

#endif
