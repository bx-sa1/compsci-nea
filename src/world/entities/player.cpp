#include "world/entities/player.hpp"

#include "Eigen/Eigen"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "debug/logger.hpp"
#include "physics/character_controller.hpp"
#include "physics/physics.hpp"
#include "renderer/model.hpp"
#include "game.hpp"
#include "renderer/renderer.hpp"
#include "game.hpp"
#include "input.hpp"
#include "utils/convar.hpp"
#include "world/entity.hpp"
#include "world/world.hpp"
#include <cmath>
#include <float.h>
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"
#include "debug_draw.hpp"
#include "utils/defines.hpp"
#include "renderer/camera.hpp"

DEFINE_ENTITY(ent_player, EPlayer)

ConVar cv_player_accel("player_accel", 5.0f);
ConVar cv_player_max_speed("player_max_speed", 20.0f);
ConVar cv_player_slope_max_angle("player_slope_max_angle", 78.0f);
ConVar cv_fly("fly", 0.0f, "fly cheat");
ConVar cv_player_cam_eye_height("player_cam_eye_height", 0.0f);
ConVar cv_crosshair("crosshair", "dot");
ConVar cv_crosshair_size("crosshair_size", 14.0f);
ConVar cv_show_player_stats("show_player_stats", false);

void cmd_forward(int argc, std::string argv[])
{
    EPlayer *player = g_game->getWorld()->client_player();

    if(player)
    {
        player->setForwardMove(1.0f);
    }
}

void cmd_backward(int argc, std::string argv[])
{
    EPlayer *player = g_game->getWorld()->client_player();

    if(player)
    {
        player->setForwardMove(-1.0f);
    }
}

void cmd_strafe_right(int argc, std::string argv[])
{
    EPlayer *player = g_game->getWorld()->client_player();

    if(player)
    {
        player->setRightMove(1.0f);
    }
}

void cmd_strafe_left(int argc, std::string argv[])
{
    EPlayer *player = g_game->getWorld()->client_player();

    if(player)
    {
        player->setRightMove(-1.0f);
    }
}

void cmd_use(int argc, std::string argv[])
{
	EPlayer *player = g_game->getWorld()->client_player();

	if(player)
	{
		Entity *picked = player->picking();
		if(picked) picked->onPicked();
	}
}

static void player_x_key_release(int argc, std::string argv[])
{
	EPlayer *player = g_game->getWorld()->client_player();

	if(player)
	{
		player->setRightMove(0.0f);
	}

}

static void player_y_key_release(int argc, std::string argv[])
{
	EPlayer *player = g_game->getWorld()->client_player();

	if(player)
	{
		player->setForwardMove(0.0f);
	}

}

ConsoleCommand cc_forward("player_forward", cmd_forward, "Move player forward", player_y_key_release);
ConsoleCommand cc_backward("player_backward", cmd_backward, "Move player backward", player_y_key_release);
ConsoleCommand cc_strafe_left("player_strafe_left", cmd_strafe_left, "Move player left", player_x_key_release);
ConsoleCommand cc_strafe_right("player_strafe_right", cmd_strafe_right, "Move player right", player_x_key_release);
ConsoleCommand cc_use("use", cmd_use, "Use");


// does not work correctly if there are multiple players
EPlayer::EPlayer() :
    yaw(0.0f, Eigen::Vector3f::UnitZ()), 
    pitch(0.0f, Eigen::Vector3f::UnitX()),
    roll(0.0f, Eigen::Vector3f::UnitY()),
    local_move_dir(Eigen::Vector3f::Zero()),
	m_controller(NULL),
	m_velocity(Eigen::Vector3f::Zero())
{
	int width = utils::get_convar("res_width")->getNumber();
	int height = utils::get_convar("res_height")->getNumber();
	player_view.camera = Camera(0.01f, 1000.0f, 60, (float)width/height);

    mdelta_x = g_game->getInput()->mouse_rpos_x;
	mdelta_y = g_game->getInput()->mouse_rpos_y;

	//preload crosshair
	std::string xh_name = cv_crosshair.getString();
	xh_full_path = "materials/crosshairs/" + xh_name + ".mat";
}

EPlayer::~EPlayer() 
{
	delete m_controller;
}

void EPlayer::spawn(const SpawnArgs &args)
{
    Entity::spawn(args);
	m_controller = new CharacterController(Physics_ModelToDynamicCollisionShape(getCollisionModel()));
	g_game->getWorld()->physics()->addToWorld(m_controller->get_body(), btBroadphaseProxy::DefaultFilter, btBroadphaseProxy::StaticFilter|btBroadphaseProxy::DefaultFilter);
}

void transform_directions(Eigen::Affine3f transform, Eigen::Vector3f *f,Eigen::Vector3f *r, Eigen::Vector3f *u)
{
	Eigen::Matrix3f look = transform.linear();
    if(r) *r = Eigen::Vector3f(look(0,0), look(1,0), look(2,0)); //get a, y and z axis
    if(f) *f = Eigen::Vector3f(look(0,1), look(1,1), look(2,1));
    if(u) *u = Eigen::Vector3f(look(0,2), look(1,2), look(2,2));
}


Eigen::Vector3f accelerate(Eigen::Vector3f u, Eigen::Vector3f v, float a)
{
	//linear interpolation
	return u + (a*(v-u));
}

//only work with horizontal velocity
void EPlayer::ground_move()
{
	//calc view direction
	Eigen::Vector3f right, forward, up;
	transform_directions(getTransform(), &forward, &right, &up);

	//move relative to that direction
	Eigen::Vector3f move_dir = (right * local_move_dir.x()) + (forward * local_move_dir.y());
	move_dir.z() = 0;

	Eigen::Vector3f hvel = m_velocity;
	hvel.z() = 0;

	hvel = accelerate(hvel, move_dir * cv_player_max_speed.getNumber(), cv_player_accel.getNumber()*g_game->m_frametime);
	m_velocity.x() = hvel.x();
	m_velocity.y() = hvel.y();
	m_velocity.z() = 0;
}

void EPlayer::air_move()
{
	//calc view direction
	Eigen::Vector3f right, forward, up;
	transform_directions(getTransform(), &forward, &right, &up);

	//move relative to that direction
	Eigen::Vector3f move_dir = (right * local_move_dir.x()) + (forward * local_move_dir.y());
	move_dir.z() = 0;

	Eigen::Vector3f hvel = m_velocity;
	hvel.z() = 0;

	hvel = accelerate(hvel, move_dir * cv_player_max_speed.getNumber(), cv_player_accel.getNumber()*g_game->m_frametime);
	m_velocity.x() = hvel.x();
	m_velocity.y() = hvel.y();
	m_velocity.z() -= utils::get_convar("gravity")->getNumber() * g_game->m_frametime;
}

Entity *EPlayer::picking() const
{
	//calc start and end pos of ray
	Eigen::Vector3f start(0.5f, 0.5f, -1.0f);
	Eigen::Vector3f end(0.5f, 0.5f, 1.0f);

	Eigen::Matrix4f VPInv = (player_view.camera.projection_matrix * (Renderer::yup_to_zup * player_view.camera.transform.matrix().inverse())).inverse();
	start = (VPInv * start.homogeneous()).head(3); //times by inverse view projection matrix to get from ndc to world space
	end = (VPInv * end.homogeneous()).head(3);

	//trace
	btCollisionWorld::ClosestRayResultCallback trace(btVector3(0.0f, 0.0f, 0.0f), btVector3(0.0f,0.0f,0.0f));
	g_game->getWorld()->physics()->getPhysicsWorld()->rayTest(Physics_EToBT_VEC3(start), Physics_EToBT_VEC3(end), trace);

	if(trace.hasHit())
	{
		Entity *e = static_cast<Entity*>(trace.m_collisionObject->getUserPointer());
		if(e)
		{
			return e;
		}
	}

	return NULL;
}

void EPlayer::entity_tick()
{
	//claculate rotation
    int mx, my;
    mx = g_game->getInput()->mouse_rpos_x;
	my = g_game->getInput()->mouse_rpos_y;
    Eigen::Vector2f mdt(mx, my);

    yaw.angle() += degtorad(-mdt.x() * 4.0f * 0.022f); //negate because window origin is top left
    pitch.angle() += degtorad(-mdt.y() * 4.0f * 0.022f);

    //clamp rotation
    if(pitch.angle() > PI/2) //angle in radians
    {
        pitch.angle() = PI/2;    
    }
    else if(pitch.angle() < -(PI/2))
    {
        pitch.angle() = -(PI/2);
    }

	//apply rotations
	if(!cv_fly.getBool()) 
	{
		getTransform().linear() = (yaw).toRotationMatrix();
		m_controller->get_body()->getWorldTransform().getRotation().setEulerZYX(yaw.angle(), 0, 0);
	}
	else 
	{
		getTransform().linear() = (yaw*roll*pitch).toRotationMatrix();
		m_controller->get_body()->getWorldTransform().getRotation().setEulerZYX(yaw.angle(), roll.angle(), pitch.angle());
	}
	player_view.camera.transform.linear() = (yaw*roll*pitch).toRotationMatrix();

	if(m_controller->on_floor())
	{
		ground_move();
	}
	else if(cv_fly.getBool())
	{
		//fly_move();
	}
	else
	{
		air_move();
	}

	m_controller_velocity = m_controller->move(g_game->getWorld()->physics()->getPhysicsWorld(), Physics_EToBT_VEC3(m_velocity*g_game->m_frametime), btVector3(0, 0, 1.0f), degtorad(cv_player_slope_max_angle.getNumber()), false);

	//set transforms after move
	player_view.camera.transform.translation() = getTransform().translation() = Physics_BTToE_VEC3(m_controller->get_body()->getWorldTransform().getOrigin());	
	player_view.camera.transform.translation().z() += cv_player_cam_eye_height.getNumber();
}

void EPlayer::render()
{
    Entity::render();

    g_game->getWorld()->add_view(player_view);
}

void EPlayer::draw_hud()
{
	float width = utils::get_convar("res_width")->getNumber();
	float height = utils::get_convar("res_height")->getNumber();
	float xh_size = cv_crosshair_size.getNumber();
	Material *xh_mat = assets::load_material(xh_full_path);
	nk_context *ctx = g_game->getNuklear();
	char temp_buffer[64];

	struct nk_style *s = &ctx->style;
	//nk_style_push_color(ctx, &s->window.background, nk_rgba(0,0,0,0));
	nk_style_push_style_item(ctx, &s->window.fixed_background, nk_style_item_hide());	
	if(nk_begin(ctx, "hud", nk_rect(0, 0, width, height), NK_WINDOW_NO_SCROLLBAR))
	{
		nk_layout_space_begin(ctx, NK_STATIC, height, INT_MAX);
		{
			//crosshair
			nk_layout_space_push(ctx, nk_rect((width/2)-(xh_size/2), (height/2)-(xh_size/2), xh_size, xh_size));
			nk_image(ctx, nk_image_ptr(xh_mat));

			//player_stats
			if(cv_show_player_stats.getBool())
			{
				
				nk_style_pop_style_item(ctx);	

				nk_layout_space_push(ctx, nk_rect(0, 0, 200, 200));
				if(nk_group_begin(ctx, "Player Stats", NK_WINDOW_NO_SCROLLBAR|NK_WINDOW_BORDER|NK_WINDOW_TITLE))
				{
					nk_layout_row_dynamic(ctx, 0, 1);

					sprintf(temp_buffer, "speed (before character controller):\n%.4f, %.4f, %.4f", m_velocity.x(), m_velocity.y(), m_velocity.z());
					nk_label_wrap(ctx, temp_buffer);

					sprintf(temp_buffer, "speed (after character controller):\n%.4f, %.4f, %.4f", m_controller_velocity.x()/g_game->m_frametime, m_controller_velocity.y()/g_game->m_frametime, m_controller_velocity.z()/g_game->m_frametime);
					nk_label_wrap(ctx, temp_buffer);

					nk_group_end(ctx);
				}
				
				nk_style_push_style_item(ctx, &s->window.fixed_background, nk_style_item_hide());	
			}

		}
		nk_layout_space_end(ctx);
	}
	nk_end(ctx);
	//nk_style_pop_color(ctx);
	nk_style_pop_style_item(ctx);
}

void EPlayer::setForwardMove(int mag)
{
    local_move_dir.y() = mag;
}

void EPlayer::setRightMove(int mag)
{
    local_move_dir.x() = mag;
}
