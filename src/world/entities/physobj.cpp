#include "world/entities/physobj.hpp"
#include "debug/logger.hpp"
#include "world/entity.hpp"
#include "game.hpp"
#include "world/world.hpp"
#include "physics/physics.hpp"

DEFINE_ENTITY(ent_physobj, EPhysObj)

EPhysObj::EPhysObj()
{

}

EPhysObj::~EPhysObj()
{

}

void EPhysObj::spawn(const SpawnArgs &args)
{
	Entity::spawn(args);

	float mass = 1.0f;

	auto arg_mass = args.FindMember("mass");
	if(arg_mass != args.MemberEnd() && arg_mass->value.IsNumber())
	{
		mass = arg_mass->value.GetFloat();
	}

	CollisionObjectInitArgs coargs;
	coargs.mass = mass;
	initCollisionObject(CO_RIGIDBODY_DYNAMIC, &coargs);
}

void EPhysObj::entity_tick()
{
	#if 0 //debug purposes
	Vec3 pos = rigid_body->getPos();
	LOG_INFO("%s: %f, %f, %f", name.c_str(), pos.x, pos.y, pos.z);
	#endif
}
