#include "trigger.hpp"

#include "utils/json_parse.hpp"
#include "game.hpp"
#include "world/world.hpp"
#include "world/entity.hpp"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "debug/logger.hpp"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"

ETriggerBase::ETriggerBase() :
	active(true),
	fired(false)
{

}

ETriggerBase::~ETriggerBase()
{

}

void ETriggerBase::spawn(const SpawnArgs &args)
{
	Entity::spawn(args);
	
	hide();
	initCollisionObject(CO_GHOST);
}

void ETriggerBase::entity_tick()
{
    btGhostObject *ghost = (btGhostObject*)getCollisionObject();
    for(int i = 0; i < ghost->getNumOverlappingObjects(); i++)
    {
        TriggerCollisionInfo info;
        btCollisionObject *rb = ghost->getOverlappingObject(i);
        Entity *other = (Entity*)rb->getUserPointer();
        if(!other) continue;

        info.other = other;
        onTrigger(info);
    }
 

}

DEFINE_ENTITY(trigger, ETrigger)

ETrigger::ETrigger() :
	count(0),
	maxcount(-1),
	wait_time(0)
{

}

ETrigger::~ETrigger()
{

}

void ETrigger::spawn(const SpawnArgs &args)
{
	ETriggerBase::spawn(args);

	FIND_KEY(args, wait_time, Float)
	{
		wait_time = key_wait_time->value.GetFloat();
	}

	FIND_KEY(args, max_refires, Int)
	{
		maxcount = key_max_refires->value.GetInt();
	}

	FIND_KEY(args, once, Int)
	{
		once = key_once->value.GetBool();
	}
}

void ETrigger::onTrigger(TriggerCollisionInfo &info)
{
	static float time = 0;

	if(time >= wait_time)
	{
		time = 0;
		send_event("trigger_activated");

		count++;
		if(count >= maxcount && maxcount != -1)
		{
			g_game->getWorld()->kill_entity(this->name);
		}
	}

	
	time += g_game->m_frametime;
}
