#ifndef ELIGHT_H_
#define ELIGHT_H_

#include "renderer/renderer.hpp"
#include "world/entity.hpp"

class ELight : public Entity
{
    DECLARE_ENTITY(ELight)
public:
    ELight();
    ~ELight();

    void spawn(const SpawnArgs &args);

    void entity_tick();
private:
	int lightinf_handle;
	LightInfo lightinf;
};

#endif
