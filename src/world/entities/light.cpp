#include "light.hpp"

#include "rapidjson/document.h"
#include "game.hpp"
#include "renderer/light_info.hpp"
#include "renderer/renderer.hpp"
#include "utils/json_parse.hpp"
#include "world/entity.hpp"
#include "world/world.hpp"
#include "utils/convar.hpp"

#define LIGHT_MIN_INTENSITY 0.001

DEFINE_ENTITY(ent_light, ELight)

ConVar cv_shadowmap_max_distance("shadowmap_max_distance", 100, "m");
ELight::ELight() :
	lightinf_handle(-1)
{
	lightinf.light_colour = Eigen::Vector4f::Constant(1.0f);
	lightinf.coeffs = Eigen::Vector3f(1.0f, 0.0f, 0.0f);
    float max = cv_shadowmap_max_distance.getNumber();
}

ELight::~ELight() 
{
}

void ELight::spawn(const SpawnArgs &args)
{
	Entity::spawn(args);

    FIND_KEY(args, light_type, String)
    {
    	std::string type = key_light_type->value.GetString();
    	if(type == "DIRECTIONAL")
    	{
    		lightinf.type = LightType::DIRECTIONAL;
    	}
    	else if(type == "POINT")
    	{
    		lightinf.type = LightType::POINT;
    	}
    }

    FIND_KEY(args, colour, Array)
    {
    	Eigen::Vector4f colour(
    		key_colour->value[0].GetFloat(),
    		key_colour->value[1].GetFloat(),
    		key_colour->value[2].GetFloat(),
    		key_colour->value[3].GetFloat()
    	);
    	lightinf.light_colour = colour;
    }

    FIND_KEY(args, coeffs, Array)
    {
    	Eigen::Vector3f coeffs(
    		key_coeffs->value[0].GetFloat(),
    		key_coeffs->value[1].GetFloat(),
    		key_coeffs->value[2].GetFloat()
    	);
    	lightinf.coeffs = coeffs;
		
    }

    FIND_KEY(args, shadow, Bool)
    {
    }

    //only set physics if there is a model available
    if(getCollisionModel() || getModel())
    {
        //just make it static 
        initCollisionObject(CO_RIGIDBODY_STATIC);
    }

	float a = lightinf.coeffs.x();
	float b = lightinf.coeffs.y();
	float c = lightinf.coeffs.z() - (lightinf.light_colour.w()/LIGHT_MIN_INTENSITY);

	float distance = (-b + sqrt((b*b) - (4*a*c)))/(2*a);
	lightinf.bounds.max() = Eigen::Vector3f::Constant(distance);
	lightinf.bounds.min() = Eigen::Vector3f::Constant(-distance);
	
	//add to scene
	g_game->getWorld()->scene().add_light(&lightinf);
}

void ELight::entity_tick() 
{
    Eigen::Transform<float, 3, Eigen::Affine> ent_trans = getTransform();

	switch(lightinf.type)
	{
		case LightType::DIRECTIONAL:
		{
            Eigen::Vector3f forward(-ent_trans(0,2), -ent_trans(1,2), -ent_trans(2,2)); //get up vector then negate it to get forward

            lightinf.pd = forward;
			break;
		}
		case LightType::POINT:
		{
            lightinf.pd = ent_trans.translation();
			//view_trans.translate(ent_trans.translation());
			break;
		}
		default: 
		{
			assert(false);
		}
	}
}
