#ifndef SPAWN_H_
#define SPAWN_H_

#include "world/entity.hpp"

class ESpawner : public Entity
{
    DECLARE_ENTITY(ESpawner);
public:
    ESpawner();
    ~ESpawner();

    void spawn(const SpawnArgs &args);

    void entity_tick();

private:
    std::string spawn_entity;
    rapidjson::Value spawn_entity_args;
    int count;
    int freq;
};
    
#endif
