#include "world/world.hpp"

#include <cstdio>
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "SDL2/SDL.h"
#include "binary_file_def.hpp"
#include "renderer/renderer.hpp"
#include <sstream>
#include <cstring>
#include "utils/convar.hpp"
#include "debug/logger.hpp"
#include "rapidjson/document.h"
#include "rapidjson/rapidjson.h"
#include "renderer/light_info.hpp"
#include "renderer/model.hpp"
#include "rapidjson/stringbuffer.h"
#include "world/entities/player.hpp"
#include "world/entity.hpp"
#include "game.hpp"
#include "renderer/renderer.hpp"
#include "utils/concommand.hpp"
#include <memory>
#include <cmath>
#include <string>
#include "utils/json_parse.hpp"
#include "swap.hpp"
#include <cstdlib>
#include "world/map.hpp"
#include "Eigen/Eigen"

#define MULTILINE(...) #__VA_ARGS__
const char *player_def = MULTILINE({
    "type": "ent_player",
    "args": {
        "collision_model": "models/player_model.glb"
    }
});

ConVar cv_gravity("gravity", 10.0f);
ConVar cv_octree_capacity("world_octree_capacity", 3);
ConVar cv_octree_depth("world_octree_depth", 8);
ConVar cv_debug_draw_model_bounds("debug_draw_model_bounds", false);
ConVar cv_debug_physics("debug_physics", false);

static void _map(int argc, std::string argv[])
{
    if(argc > 1) return;
    if(argc == 0) return;

    g_game->change_map(argv[0]);

    LOG_INFO("Loaded map %s", argv[0].c_str());
}
ConsoleCommand cc_map("map", _map, "Switch map");

World::World() : player_num(0), m_current_map(NULL)
{
	m_physics = new Physics(cv_gravity.getNumber());    
}

World::~World()
{
	clear();
	delete m_physics;
}	

void World::clear()
{
	delete m_current_map;
	m_current_map = NULL;

    for(auto e : m_entities)
    {
        delete e.second;
    }
    m_entities.clear();

    //can clear now since all entities are deleted
    m_lights.clear();

    player_num = 0;
}

bool World::initFromMap(Map *map)
{
    //LOG_INFO("World init %s", filename.c_str());

    clear();
   
	m_current_map = map;

	//load entities
	for(auto &json_entity : map->entities().GetObject())
	{
		Entity *entity = spawn_entity(json_entity.name.GetString(), json_entity.value);
		if(!entity) 
		{
			LOG_WARNING("Failed to load entity &s", json_entity.name.GetString());
		}
	}

	//spawn player now that spawn points are available
	EPlayer *player = spawn_player();
	if(!player) LOG_FATAL_ERROR("failed to spawn player entity");

    return true;
}

void World::tick()
{
    //Octree<Entity *> entity_octree(m_world_size, (int)cv_octree_capacity.getNumber(), (int)cv_octree_depth.getNumber());
    
    m_physics->getPhysicsWorld()->stepSimulation(g_game->m_frametime);

    for(auto e : m_entities)
    {
		e.second->tick();
    }
}	
	    
void World::render()
{
	for(View &v : m_views)
	{
		//set scene
		v.scene = m_scene;
		g_game->getRenderer()->renderView(v);
		
	}
	EPlayer *player = client_player();
	if(player)
	{
		player->draw_hud();
	}
#if 0
	for(const View &v : m_views)
	{
		g_game->getRenderer()->beginViewDef(v);
		{
			for(auto &entity : m_entities)
			{
				g_game->getRenderer()->addModel(entity.second->getModel());
			}

			for(LightInfo &info : m_lights)
			{
				g_game->getRenderer()->addLight(info);
			}
		}
		g_game->getRenderer()->endViewDef();
		
		if(v.framebuffer)
		{
			g_game->getRenderer()->flush(v.framebuffer);
		}
		else
		{
			//this means its a main view
			if(cv_debug_physics.getBool())
			{
				m_physics->getPhysicsWorld()->debugDrawWorld();
			}
		}

		EPlayer *player = client_player();
		if(player)
		{
			player->draw_hud();
		}
	}
#endif
	m_views.clear();
}

Entity *World::spawn_entity(const std::string &name, rapidjson::Value &ent)
{
	if(!ent.IsObject()) return NULL;

    std::string etype;
    rapidjson::Value eargs;

    FIND_KEY(ent, type, String)
    {
        etype = key_type->value.GetString();
    }
    
    FIND_KEY(ent, args, Object)
    {
        eargs = key_args->value.GetObject();
    }

    if(etype.empty())
    {
        LOG_ERROR("Invalid entity definition");
        return NULL;
    }
    
    Entity *entity = Entity::instantiate(etype);
    if(!entity)
    {
        LOG_ERROR("Failed to instantiate entity of type %s", etype.c_str());
        return NULL;
    }

    entity->spawn(eargs);

    m_entities[name] = entity;
    entity->name = name;

    return entity;
}

void World::kill_entity(const std::string &name)
{
    auto ent = m_entities.find(name);
    if(ent != m_entities.end())
    {
        delete ent->second;
        m_entities.erase(ent);
    }
}

EPlayer *World::spawn_player()
{
    rapidjson::Document d;
    d.Parse(player_def);

    //if there is an available start point
    if(m_player_start_points.size())
    {
        //get random start point
        int spawn = rand() % m_player_start_points.size();
        Eigen::Affine3f t = m_player_start_points[spawn];

        //set to player args
        rapidjson::Value pos(rapidjson::kArrayType);
        pos.PushBack(t.translation().x(), d.GetAllocator());
        pos.PushBack(t.translation().y(), d.GetAllocator());
        pos.PushBack(t.translation().z(), d.GetAllocator());

        rapidjson::Value rot(rapidjson::kArrayType);
		Eigen::Vector3f euler = t.rotation().eulerAngles(2, 1, 0);
        rot.PushBack(euler.x(), d.GetAllocator());
        rot.PushBack(euler.y(), d.GetAllocator());
        rot.PushBack(euler.z(), d.GetAllocator());

        d["args"].AddMember("position", pos, d.GetAllocator());
        d["args"].AddMember("rotation", rot, d.GetAllocator());
    }
    

    EPlayer *player = (EPlayer*)spawn_entity("player_"+std::to_string(player_num++), d);
    if(player)
    {
        return player;
    }

    return NULL;
}

Entity *World::get_entity(std::string name)
{
	auto entity = m_entities.find(name);
	if(entity != m_entities.end())
	{
		return entity->second;
	}
	return NULL;
}
