#include "world/map.hpp"

#include "debug/logger.hpp"
#include "utils/file.hpp"
#include "renderer/material.hpp"
#include "renderer/model.hpp"
#include "game.hpp"
#include "renderer/render_vertex.hpp"
#include "utils/file_system.hpp"
#include "Eigen/Eigen"
#include <SDL_rwops.h>
#include <stdio.h>
#include "utils/json_parse.hpp"

#define MAP_FILE_MODEL_SIZE (sizeof(std::uint32_t)*2)
#define MAP_FILE_MESH_SIZE (sizeof(std::uint32_t)*5 + sizeof(float)*6)
#define MAP_FILE_IDX_SIZE (sizeof(std::uint16_t))
#define MAP_FILE_MAT_SIZE (sizeof(char)*32)
#define MAP_FILE_VERT_SIZE (sizeof(float)*8) 

struct MapFileChunk
{
	size_t length, offset;
};

Map::Map() :
	m_entities(NULL)
{
}

Map::~Map()
{
}

static std::string get_material_string(int index, MapFileChunk *chunks, File::Ptr::element_type *file)
{
	fseek(file, chunks[3].offset+(index*MAP_FILE_MAT_SIZE), SEEK_SET);

	char string[32];
	fread(string, sizeof(char), 32, file);

	return string;
}

static Material *get_material(int index, MapFileChunk *chunks, File::Ptr::element_type *file)
{
	std::string mat_string = get_material_string(index, chunks, file);

	Material *mat = assets::load_material(mat_string);

	return mat;
}

static std::vector<unsigned int> get_indices(int index, int count, MapFileChunk *chunks, File::Ptr::element_type *file)
{
	fseek(file, chunks[2].offset+(index*MAP_FILE_IDX_SIZE), SEEK_SET);

	std::vector<unsigned int> idxs;
	idxs.resize(count);
	for(int i = 0; i < count; i++)
	{
		idxs[i] = File::read_u16(file);
	}

	return idxs;
}

static float float_cast(std::uint32_t f)
{
	static_assert(sizeof(float) == sizeof f, "`float` has a weird size.");
    float ret;
    memcpy(&ret, &f, sizeof(float));
    return ret;	
}

static std::vector<RenderVertex> get_vertices(int index, int count, MapFileChunk *chunks, File::Ptr::element_type *file)
{
	fseek(file, chunks[4].offset+(index*MAP_FILE_VERT_SIZE), SEEK_SET);

	std::vector<RenderVertex> verts;
	verts.resize(count);
	for(int i = 0; i < count; i++)
	{
		RenderVertex &v = verts[i];

		v.pos.x() = float_cast(File::read_u32(file)); 
		v.pos.y() = float_cast(File::read_u32(file)); 
		v.pos.z() = float_cast(File::read_u32(file)); 
		v.uv0.x() = float_cast(File::read_u32(file)); 
		v.uv0.y() = float_cast(File::read_u32(file)); 
		v.normal.x() = float_cast(File::read_u32(file)); 
		v.normal.y() = float_cast(File::read_u32(file)); 
		v.normal.z() = float_cast(File::read_u32(file)); 
	}

	return verts;
}

	
static ModelMesh get_mesh(size_t index, MapFileChunk *chunks, File::Ptr::element_type *file)
{
	int temp = fseek(file, chunks[1].offset+(index*MAP_FILE_MESH_SIZE), SEEK_SET);
	Sint64 pos;

	ModelMesh mesh;
	
	std::uint32_t material_id = File::read_u32(file);
	pos = ftell(file);
	mesh.mat = get_material(material_id, chunks, file);
	fseek(file, pos, SEEK_SET);

	std::uint32_t idx_start = File::read_u32(file);
	std::uint32_t idx_num = File::read_u32(file);
	pos = ftell(file);
	mesh.idxs = get_indices(idx_start, idx_num, chunks, file);
	fseek(file, pos, SEEK_SET);

	std::uint32_t vert_start = File::read_u32(file);
	std::uint32_t vert_num = File::read_u32(file);
	pos = ftell(file);
	mesh.verts = get_vertices(vert_start, vert_num, chunks, file);
	fseek(file, pos, SEEK_SET);

	mesh.bounds.max().x() = float_cast(File::read_u32(file));
	mesh.bounds.max().y() = float_cast(File::read_u32(file));
	mesh.bounds.max().z() = float_cast(File::read_u32(file));
	mesh.bounds.min().x() = float_cast(File::read_u32(file));
	mesh.bounds.min().y() = float_cast(File::read_u32(file));
	mesh.bounds.min().z() = float_cast(File::read_u32(file));

	return mesh;
}

static Model *get_model(size_t index, MapFileChunk *chunks, File::Ptr::element_type *file)
{
	fseek(file, chunks[0].offset+(index*MAP_FILE_MODEL_SIZE), SEEK_SET);

	Model *model = new Model;
	model->make_static();
	
	std::size_t mesh_start = File::read_u32(file);
	std::size_t mesh_num = File::read_u32(file);
	for(size_t i = 0; i < mesh_num; i++)
	{
		ModelMesh mesh = get_mesh(mesh_start+i, chunks, file);
		model->meshes().push_back(mesh);
		model->overall_bounds().extend(mesh.bounds);
	}

	return model;
}

bool Map::load_from_file(std::string path)
{
	File::Ptr file = filesystem::open_file(path.c_str(), "rb");
	if(!file)
	{
		LOG_ERROR("Failed to open map file %s", path.c_str());
		return false;
	}

	MapFileChunk chunks[6];

	std::uint32_t magic = File::read_u32(file.get());
	std::uint32_t version = File::read_u32(file.get());

	if(magic != 0x4d464631)
	{
		return false;
	}

	for(int i = 0; i < 6; i++)
	{
		chunks[i].length = File::read_u32(file.get());
		chunks[i].offset = File::read_u32(file.get());
	}

	//TODO: move model bounds to meshes
	int num_model = chunks[0].length / MAP_FILE_MODEL_SIZE;
	for(int i = 0; i < num_model; i++)
	{
		Model *model = get_model(i, chunks, file.get());
		m_static_models.push_back(model);
	}

	const char *m_entities_text= new char[chunks[5].length];
	int m_entities_text_length = chunks[5].length;
	fseek(file.get(), chunks[5].offset, SEEK_SET);
	fread((void*)m_entities_text, sizeof(char), chunks[5].length, file.get());

	//parse json
	if(m_entities.Parse(m_entities_text, m_entities_text_length).HasParseError())
	{
		LOG_ERROR("Failed to parse entity definition for map %s", path.c_str());
		return false;
	}

	if(!m_entities.IsObject())
	{
		return false;
	}

	return true;
}

