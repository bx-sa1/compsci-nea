#include "material.hpp"

#include <cstdio>
#include "utils/convar.hpp"
#include <string>
#include <sstream>
#include "debug/logger.hpp"
#include "rapidjson/document.h"
#include "utils/file_system.hpp"
#include "shader.hpp"
#include "texture/texture.hpp"
#include "game.hpp"
#include <vector>
#include <memory>
#include "utils/json_parse.hpp"
#include "default_resources.hpp"
#include "rapidjson/error/en.h"
#include "utils/asset_management_utils.hpp"
#include "shader.hpp"
#include "default_resources.hpp"

//format:
/**
 * {
 *		"vs_shader": "vertex_shader_path",
 * 		"fs_shader": "fragment_shader_path",
 * 		"params": {
 * 			"texture_uniform_name": {
 * 				"texture": "texture_image_path",
 * 				"filtering": "NO_TEXTURING" | "BILINEAR" | "TRILINEAR",
 * 				"wrapping": ,
 * 			},
 * 			"uniform_name": [x, y, z, w],
 * 			"flag_name": true | false
 * 		}
 * }
 **/

namespace assets
{
	Material *load_material(std::string path)
	{
		auto &cache = internal::get_asset_cache<Material>();
		Material *mat = internal::find_in_cache<Material>(cache, path);
		if(mat)
		{
			return mat;
		}

		mat = new Material;
		std::vector<unsigned char> buffer;
		size_t read;
		if(internal::load_file(path, "r", buffer, read))
		{
			Material *mat = load_material(buffer.data(), read);
			if(mat)
			{
				cache.insert({path, mat});
				return mat;
			}
		}
		
		delete mat;
		return material_unavailable;
	}

	Material *load_material(unsigned char *data, size_t size)
	{
		//parse json
		rapidjson::Document d;
		if(d.Parse((char*)data, size).HasParseError())
		{
			LOG_ERROR("%s JSON PARSE ERROR(offset %u): %s", "reserved", d.GetErrorOffset(), GetParseError_En(d.GetParseError()));
			return NULL;
		}

		//check if root json element is array
		if(!d.IsObject())
		{
			LOG_ERROR("Invalid JSON file.");
			return NULL;
		}

		Material *mat = new Material;

		Defines defs;
		std::string vs_name, fs_name;
		FIND_KEY(d, vs_shader, String)
		{
			vs_name = key_vs_shader->value.GetString();
			VertexShader *vs = load_shader<VertexShader>(vs_name, defs); //add defines
			if(!vs) return NULL;
			mat->set_vertex_shader(vs);
		}

		FIND_KEY(d, fs_shader, String)
		{
			fs_name = key_fs_shader->value.GetString();
			FragmentShader *fs = load_shader<FragmentShader>(fs_name, defs);
			if(!fs) return NULL;
			mat->set_fragment_shader(fs);
		}

		FIND_KEY(d, params, Object)
		{
			for(const auto &param : key_params->value.GetObject())
			{
				switch(param.value.GetType())
				{
					case rapidjson::kArrayType: //uniform
					{
						std::array<float, 4> data = {0};
						for(int i = 0; i < 4 && i < param.value.GetArray().Size(); i++)
						{
							const auto &elem = param.value.GetArray()[i];
							data[i] = elem.IsNumber() ? elem.GetFloat() : 0;
						}
						mat->uniforms().insert(UniformMap::value_type(param.name.GetString(), data));
						break;
					}
					case rapidjson::kObjectType: //sampler
					{
						const auto &sampler = param.value.GetObject();

						TextureFiltering filtering;
						TextureWrapping wrapping;
						std::string texture_type;

						FIND_KEY(sampler, type, String)
						{
							texture_type = key_type->value.GetString();
						}
						else
						{
							texture_type = "2D";
						}

						FIND_KEY(sampler, filtering, String)
						{
							std::string j_filtering = key_filtering->value.GetString();
							if(j_filtering == "NONE")
							{
								filtering = TextureFiltering::NO_FILTERING;
							}
							else if(j_filtering == "BILINEAR")
							{
								filtering = TextureFiltering::BILINEAR;
							}
							else if(j_filtering == "TRILINEAR")
							{
								filtering = TextureFiltering::TRILINEAR;
							}
						}
						else
						{
							filtering = TextureFiltering::NO_FILTERING;
						}

						FIND_KEY(sampler, wrapping, String)
						{
							std::string j_wrapping = key_wrapping->value.GetString();
							if(j_wrapping == "REPEAT")
							{
								wrapping = TextureWrapping::REPEAT;
							}
							else if(j_wrapping == "CLAMP_WHITE")
							{
								wrapping = TextureWrapping::CLAMP_WHITE;
							}
							else if(j_wrapping == "CLAMP_BLACK")
							{
								wrapping = TextureWrapping::CLAMP_BLACK;
							}
							else if(j_wrapping == "CLAMP_EDGE")
							{
								wrapping = TextureWrapping::CLAMP_EDGE;
							}
						}
						else
						{
							wrapping = TextureWrapping::CLAMP_EDGE;
						}

						Texture *texture = NULL;
						FIND_KEY(sampler, texture, String)
						{
							if(texture_type == "2D")
							{
								texture = load_texture<Texture2D>(key_texture->value.GetString());
							}
							else
							{
								LOG_ERROR("Not a valid type name for texture: %s", texture_type.c_str());
							}
						}
						else
						{
							LOG_WARNING("Texture %s defined without a name", key_texture->value.GetString());
							break;
						}

						//texture->set_filtering(filtering);
						//texture->set_wrapping(wrapping);
						mat->textures().insert(TextureMap::value_type(param.name.GetString(), texture));
						break;
					}
					case rapidjson::kTrueType:
					case rapidjson::kFalseType:
					{
						mat->flags().insert(FlagMap::value_type(param.name.GetString(), param.value.GetBool()));
						break;
					}
					default:
					{
						break;
					}
				}
			}
		}

		return mat;
	}

	Material *gen_material_unavailable()
	{
		//Material *mat = load_material((unsigned char *)::internal::default_material, ::internal::default_material_len);
		Material *mat = new Material;
		Defines defs;
		VertexShader *vs = load_shader<VertexShader>("shaders/generic.vert", defs);
		FragmentShader *fs = load_shader<FragmentShader>("shaders/unlit.frag", defs);
		Texture *tex = load_texture_2d(::internal::default_texture_2d, 2, 2, TextureFormat::RGBA);
		if(!vs || !fs || !tex)
		{
			LOG_FATAL_ERROR("material_unavailable generated unsuccessfully");
			delete mat;
			return NULL;
		}

		mat->set_vertex_shader(vs);
		mat->set_fragment_shader(fs);
		mat->add_texture("base_texture", tex);	

		LOG_INFO("material_unavailable generated successfully");

		return mat;
	}
	Material *material_unavailable = gen_material_unavailable();

	Material *gen_material_white()
	{
		return NULL;
		Material *mat = new Material;
		unsigned char white_pixel[] = {1,1,1,1};
		Texture *texture = load_texture_2d(white_pixel, 1, 1, TextureFormat::RGBA);
	}
	Material *material_white = NULL;

	Material *material_black = NULL;
};

Material::Material()
{
}

Material::~Material() 
{
	if(m_handle) 
		glDeleteProgram(m_handle);
}

GLuint Material::get_handle() 
{
	if(!m_handle_ready)
	{
		if(!m_handle)
		{
			m_handle = glCreateProgram();
		}

		m_handle = internal::create_program(m_shaders.vs->get_handle(), 
			m_shaders.fs->get_handle()
		);

		m_handle_ready = true;
	}

	return m_handle;
}
