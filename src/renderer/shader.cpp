#include "shader.hpp"

#include "SDL2/SDL_rwops.h"
#include "SDL2/SDL_stdinc.h"
#include <cstdio>
#include <vector>
#include <sstream>
#include "debug/logger.hpp"
#include "utils/file_system.hpp"
#include "game.hpp"
#include <memory>
#include <algorithm>
#include "utils/parser.hpp"
#include <cstring>
#include "default_resources.hpp"
#include <sstream>
#include "game.hpp"
#include "utils/asset_management_utils.hpp"

static const char *shader_version = "#version 150\n";
struct attribInfo
{
    int index;
    std::string glsl_name;
} attribs[] = {
    {ATTRIB_INDEX_POSITION, "in_position"},
    {ATTRIB_INDEX_NORMAL, "in_normal"},
    {ATTRIB_INDEX_COLOUR0, "in_colour_0"},
    {ATTRIB_INDEX_COLOUR1, "in_colour_1"},
    {ATTRIB_INDEX_UV0, "in_uv_0"},
    {ATTRIB_INDEX_UV1, "in_uv_1"},
    {0, ""}
};

#define CHECK_ERROR(SP, shader, status) \
	GLint isCompiled = 0; \
	glGet##SP##iv(shader, GL_##status##_STATUS, &isCompiled); \
	if(isCompiled == GL_FALSE) \
	{ \
		GLint maxLength = 0; \
		glGet##SP##iv(shader, GL_INFO_LOG_LENGTH, &maxLength); \
		std::vector<GLchar> infoLog(maxLength); \
		glGet##SP##InfoLog(shader, maxLength, &maxLength, &infoLog[0]); \
		LOG_ERROR("%s", infoLog.data()); \
	} \
	if(isCompiled == GL_FALSE)

namespace internal
{
	GLuint create_program(GLuint vs, GLuint fs)
	{
		GLuint program = glCreateProgram();
		glAttachShader(program, vs);
		glAttachShader(program, fs);

		//set vert attrib locations
		for(int i = 0; !attribs[i].glsl_name.empty(); i++)
		{
			glBindAttribLocation(program, attribs[i].index, attribs[i].glsl_name.c_str());
		}

		glLinkProgram(program);
		glDetachShader(program, vs);
		glDetachShader(program, fs);
		CHECK_ERROR(Program, program, LINK)
		{
			glDeleteProgram(program);
			return 0;
		}

		return program;
	}

	GLuint create_shader(GLenum shader_type, const char *src, const char *defines)
	{
		GLuint shader = glCreateShader(shader_type);
		{
			const char *sources[3] = { shader_version, defines, src };
			int lengths[3] = { static_cast<int>(strlen(shader_version)), static_cast<int>(strlen(defines)), static_cast<int>(strlen(src)) };
			LOG_INFO("Compiling vertex shader %d", shader);
			glShaderSource(shader, 3, sources, lengths);
			glCompileShader(shader);
			CHECK_ERROR(Shader, shader, COMPILE)
			{
				glDeleteShader(shader);
				return 0;
			}
			return shader;
		}
	}

	static std::string remove_quote(std::string s)
	{
		size_t found = s.find("\"");
		while(found != s.npos)
		{
			s.erase(found, 1);
			found = s.find("\"");
		}
		return s;
	}

	static std::string get_path(std::string s)
	{
		size_t found = s.find_last_of("/\\");
		return s.substr(0, found + 1);
	}

	const char *include_identifier = "#include";
	std::string process_shader_source(std::string data)
	{
		bool end = false;
		std::string new_source;
		std::string line;
		std::stringstream stream(data);
		while(std::getline(stream, line))
		{
			if(line.find(include_identifier) != line.npos)
			{
				line.erase(0, strlen(include_identifier)); 
				line.erase(std::remove(line.begin(), line.end(), '\"'), line.end());
				line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
				std::string path;
				//HACK: default shader has no name so path it empty: hardcode to shaders
				path = "shaders/";
				path += line;

				Defines defs;
				GenericShader *shader = NULL;
				shader = assets::load_shader<GenericShader>(path, defs);
				new_source += shader->source() + '\n';

			}
			else
			{
				new_source += line + '\n';
			}
		}

		return new_source;
	}

	std::string process_shader_defines(const Defines &defines)
	{
		std::string define_string;
		for(const std::string &define : defines)
		{
			define_string += "#define " + define + "\n";
		}
		return define_string;
	}
};

Shader::Shader()
{
}

Shader::~Shader()
{
}

const std::string include_identifier = "#include";

#if 0
static bool get_line(std::string &str, char *&char_data)
{
	str.clear();

	while(1)
	{
		char c = *(char_data++);
		if(c == '\r')
		{
			c = *(char_data++);
			if(c == '\n')
			{
				return true;
			}
		}
		if(c == '\n')
		{
			return true;
		}
		if(c == '\0')
		{
			if(str.empty())
			{
				return false;
			}
			return true;
		}
		str += c;
	}
}
#endif

GLuint VertexShader::get_handle()
{
	if(m_handle_ready == false)
	{
		m_handle = internal::create_shader(GL_VERTEX_SHADER, m_source.c_str(), m_defines.c_str());
		m_handle_ready = true;
	}

	return m_handle;
}

GLuint FragmentShader::get_handle()
{
	if(m_handle_ready == false)
	{
		m_handle = internal::create_shader(GL_FRAGMENT_SHADER, m_source.c_str(), m_defines.c_str());
		m_handle_ready = true;
	}

	return m_handle;
}
