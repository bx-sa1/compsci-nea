#ifndef MODEL_H_
#define MODEL_H_

#include <string>
#include "renderer/vertex_buffer.hpp"
#include "material.hpp"
#include "renderer/render_vertex.hpp"
#include "renderer/render_triangle.hpp"
#include <map>
#include <vector>
#include "renderer/mesh_vao_ptr.hpp"
#include "Eigen/Eigen"

class Model;

namespace assets
{
	Model *load_model(std::string path, bool is_static = false);
	Model *load_model(unsigned char *data, size_t size, bool is_static = false); //not cached
};

struct ModelJoint
{
    Eigen::Transform<float, 3, Eigen::Affine> joint_matrix;
};


struct ModelMesh
{
	Material *mat;
	
	Eigen::AlignedBox3f bounds;

	std::vector<RenderVertex> verts;
    std::vector<unsigned int> idxs;

	MeshVAOPtr vao_ptr;
};

/**
 * A model that can be used for rendering,
 * It is just made up of a bunch of meshes.
**/
class Model
{
public:
    Model();
    ~Model(); 

	//Get the joints of the model
	std::vector<ModelJoint> &joints() { return m_joints; }
	const std::vector<ModelJoint> &joints() const { return m_joints; }

	//Get the meshes of the model
    const std::vector<ModelMesh> &meshes() const { return m_meshes; }
	std::vector<ModelMesh> &meshes() { return m_meshes; }

	//Get the total bound of all the meshes in the model
    const Eigen::AlignedBox3f &overall_bounds() const { return m_overall_bounds; }
	Eigen::AlignedBox3f &overall_bounds() { return m_overall_bounds; }

	bool is_static() { return m_static_model; }
	void make_static() { m_static_model = true; }
	void make_dynamic() { m_static_model = false; }

	Eigen::Matrix4f model_matrix;

private:
    std::vector<ModelMesh> m_meshes;
    std::vector<ModelJoint> m_joints;
    Eigen::AlignedBox3f m_overall_bounds;
	
	bool m_static_model;
};


#endif
