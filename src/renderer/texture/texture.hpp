#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <string>
#include "glad/glad.h"
#include <map>
#include "renderer/gpu_resource.hpp"

/**
 * OpenGL texture wrapper
 **/
enum TextureWrapping
{
	REPEAT,
	CLAMP_WHITE,
	CLAMP_BLACK,
	CLAMP_EDGE
};

enum TextureFiltering
{
	NO_FILTERING,
	BILINEAR,
	TRILINEAR
};
	
enum class TextureFormat
{
	R = 1,
	RG,
	RGB,
	RGBA
};

namespace internal
{
	GLenum get_texture_min_filtering(TextureFiltering filtering);
	GLenum get_texture_mag_filtering(TextureFiltering filtering);
	GLenum get_texture_format(TextureFormat format);
	bool texture_has_border(TextureWrapping wrapping);
	GLenum get_texture_wrap(TextureWrapping wrapping);
	std::array<float, 4> get_texture_border(TextureWrapping wrapping);
};

class Texture;

namespace assets 
{
	template<class T>
	T *load_texture(std::string name);
};

class Texture : public GPUResource
{
public:
    Texture();
    virtual ~Texture();

	void set_pixels(unsigned char *pixels, size_t num_pixels)
	{
		m_pixels = pixels;
		m_num_pixels = num_pixels;
		m_handle_ready = false;
	}
	
	void set_format(TextureFormat format)
	{
		m_format = format;
		m_handle_ready = false;
	}

	unsigned char *pixels() { return m_pixels; }
	int num_pixels() { return m_num_pixels; }
	TextureFormat format() { return m_format; }
	TextureFiltering filtering() { return m_filtering; }
	TextureWrapping wrapping() { return m_wrapping; }
	GLenum texture_type() { return m_texture_type; }

	virtual GLuint get_handle() = 0;

protected:
	GLenum m_texture_type;

	unsigned char *m_pixels;
	int m_num_pixels;

	TextureFormat m_format;
	TextureFiltering m_filtering;
	TextureWrapping m_wrapping;
};

#endif
