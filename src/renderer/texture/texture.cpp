#include "texture.hpp"

#include "stb_image.h"
#include "game.hpp"
#include "debug/logger.hpp"
#include <cassert>
#include <vector>

/*
 * TEXTURE
 */
GLenum internal::get_texture_min_filtering(TextureFiltering filtering)
{
	switch(filtering)
	{
		case NO_FILTERING:
			return GL_NEAREST;
		case BILINEAR:
			return GL_LINEAR;
		case TRILINEAR:
			return GL_LINEAR_MIPMAP_LINEAR;
		default:
			assert(false);
			return GL_NEAREST; //it will never reach here, but this removes warning
	}
}

GLenum internal::get_texture_mag_filtering(TextureFiltering filtering)
{
	switch(filtering)
	{
		case NO_FILTERING:
			return GL_NEAREST;
		case BILINEAR:
		case TRILINEAR:
			return GL_LINEAR;
		default:
			assert(false);
			return GL_NEAREST;
	}
}

GLenum internal::get_texture_format(TextureFormat format)
{
	switch(format)
	{
		case TextureFormat::R:
			return GL_RED;
		case TextureFormat::RG:
			return GL_RG;
		case TextureFormat::RGB:
			return GL_RGB;
		case TextureFormat::RGBA:
			return GL_RGBA;
		default:
			assert(false);
			return GL_RGBA;
	}
}

bool internal::texture_has_border(TextureWrapping wrapping)
{
	return wrapping == CLAMP_BLACK || wrapping == CLAMP_WHITE;
}

GLenum internal::get_texture_wrap(TextureWrapping wrapping)
{
	switch(wrapping)
	{
		case REPEAT:
			return GL_REPEAT;
		case CLAMP_WHITE:
		case CLAMP_BLACK:
			return GL_CLAMP_TO_BORDER;
		case CLAMP_EDGE:
			return GL_CLAMP_TO_EDGE;
		default:
			assert(false);
			return GL_REPEAT;
	}
}

std::array<float, 4> internal::get_texture_border(TextureWrapping wrapping)
{
	if(wrapping == CLAMP_WHITE)
	{
		return { 0.0, 0.0, 0.0, 1.0 };
	}
	else if(wrapping == CLAMP_BLACK)
	{
		return { 1.0, 1.0, 1.0, 1.0 };
	}

	return { 0, 0, 0, 0 };
}


Texture::Texture() : 
	m_pixels(NULL),
	m_num_pixels(0),
	m_filtering(TRILINEAR),
	m_wrapping(REPEAT),
	m_format(TextureFormat::RGBA)
{
}


Texture::~Texture()
{
	glDeleteTextures(1, &m_handle);
}
