#include "texture2d.hpp"

#include "debug/logger.hpp"
#include "renderer/texture/texture.hpp"
#include "utils/asset_management_utils.hpp"
#include <vector>
#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#include "stb_image.h"
#include "default_resources.hpp"

namespace assets
{
	template<>
	Texture2D *load_texture(std::string path)
	{
		auto &cache = internal::get_asset_cache<Texture2D>();
		Texture2D *texture = internal::find_in_cache<Texture2D>(cache, path);
		if(texture)
		{
			return texture;
		}

		std::vector<unsigned char> buffer;
		size_t read;
		if(internal::load_file(path, "rb", buffer, read))
		{
			texture = load_texture_2d(buffer.data(), read);
			if(texture)
			{
				cache.insert({path, texture});
				return texture;
			}
		}

		delete texture;
		return NULL;
	}

	Texture2D *load_texture_2d(unsigned char *data, size_t size)
	{
		int width, height, components;
		unsigned char *img_pixels = stbi_load_from_memory(data, size, &width, &height, &components, 0);
		if(!img_pixels)
		{
			LOG_ERROR("%s", stbi_failure_reason());
			return NULL;
		}

		TextureFormat format = (TextureFormat)components;
		return load_texture_2d(img_pixels, width, height, format);
	}

	Texture2D *load_texture_2d(unsigned char *data, int width, int height, TextureFormat format)
	{
		Texture2D *texture = new Texture2D;

		unsigned char *pixels = new unsigned char[width*height*static_cast<int>(format)];
		if(data)
		{
			memcpy(pixels, data, width*height*static_cast<int>(format)*sizeof(unsigned char));
		}

		texture->set_width(width);
		texture->set_height(height);
		texture->set_format(format);
		texture->set_pixels(pixels, width*height*(int)format);

		return texture;
	}
};

Texture2D::Texture2D() : 
	Texture(),
	m_width(1), m_height(1)
{
	m_texture_type = GL_TEXTURE_2D;
}


Texture2D::~Texture2D()
{
	if(m_handle)
	{
		glDeleteTextures(1, &m_handle);
	}
}

GLuint Texture2D::get_handle()
{
	if(!m_handle_ready)
	{
		if(!m_handle)
		{
			glGenTextures(1, &m_handle);
		}

		glBindTexture(GL_TEXTURE_2D, m_handle);
		glTexImage2D(GL_TEXTURE_2D,
			0, internal::get_texture_format(m_format),
			m_width, m_height,
			0, internal::get_texture_format(m_format),
			GL_UNSIGNED_BYTE,
			m_pixels
		);

		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);

		m_handle_ready = true;
	}

	return m_handle;
}
