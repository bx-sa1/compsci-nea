#ifndef TEXTURE2D_H_
#define TEXTURE2D_H_

#include "texture.hpp"
#include <string>

class Texture2D;

namespace assets
{
	//load from png file in memory
	Texture2D *load_texture_2d(unsigned char *data, size_t size);
	//load from raw image data
	Texture2D *load_texture_2d(unsigned char *data, int width, int height, TextureFormat format);
};

class Texture2D : public Texture
{
public:
	Texture2D();
	~Texture2D();

	void set_width(int width)
	{
		m_width = width;
		m_handle_ready = false;
	}

	void set_height(int height)
	{
		m_height = height;
		m_handle_ready = false;
	}

	int width() { return m_width; }
	int height() { return m_height; }

	GLuint get_handle();
private:
    int m_width, m_height;
};

#endif
