#ifndef VIEW_H_
#define VIEW_H_

#include "utils/convar.hpp"
#include "camera.hpp"
#include "scene.hpp"

struct Viewport
{
	Viewport();
	Viewport(float x, float y, float width, float height);

	void apply();

	float x, y, width, height;
	float scissor_x, scissor_y, scissor_width, scissor_height;
};

/** A view holds the viewport information, 
 * projection matrix and transformation in camera and 
 * all the models and lights to be rendered in scene.
 **/
struct View
{
    Viewport viewport;

	Scene scene;
	Camera camera;
};

#endif
