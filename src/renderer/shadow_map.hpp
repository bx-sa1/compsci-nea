#ifndef SHADOW_MAP_H_
#define SHADOW_MAP_H_

#include "renderer/texture/texture.hpp"

class Renderer;
class Framebuffer;
class View;
class LightInfo;
struct ShadowMapUpdateInfo
{
	View *current_view;
	LightInfo *current_light;
	int cascade;
	int cascade_count;
};
void ShadowMap_createshadowMap(Framebuffer **framebuffer, Texture **texture);
View ShadowMap_createShadowMapView_DIRECTIONAL(const ShadowMapUpdateInfo &info, int *cascade_far_z);

#endif
