#include "vertex_buffer.hpp"

#include "debug/logger.hpp"
#include <cassert>
#include "material.hpp"

VertexArrayObject::VertexArrayObject(size_t vbo_size, size_t ibo_size, GLenum usage)
{
    // set up vbo for gpu
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

    glGenBuffers(1, &m_vbo.buffer);
	glGenBuffers(1, &m_ibo.buffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo.buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo.buffer);
    glBufferData(GL_ARRAY_BUFFER, vbo_size, NULL, usage);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ibo_size, NULL, usage);

	m_vbo.offset = 0;
	m_vbo.reserved = vbo_size;
	m_vbo.usage = usage;
	m_ibo.offset = 0;
	m_ibo.reserved = ibo_size;
	m_ibo.usage = usage;

	glEnableVertexAttribArray(ATTRIB_INDEX_POSITION);
    glVertexAttribPointer(ATTRIB_INDEX_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(RenderVertex), (void*)RENDER_VERTEX_POS_OFFSET);
    glEnableVertexAttribArray(ATTRIB_INDEX_NORMAL);
    glVertexAttribPointer(ATTRIB_INDEX_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(RenderVertex), (void*)RENDER_VERTEX_NORMAL_OFFSET);
    glEnableVertexAttribArray(ATTRIB_INDEX_COLOUR0);
    glVertexAttribPointer(ATTRIB_INDEX_COLOUR0, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(RenderVertex), (void*)RENDER_VERTEX_COLOUR0_OFFSET);
    glEnableVertexAttribArray(ATTRIB_INDEX_COLOUR1);
    glVertexAttribPointer(ATTRIB_INDEX_COLOUR1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(RenderVertex), (void*)RENDER_VERTEX_COLOUR1_OFFSET);
    glEnableVertexAttribArray(ATTRIB_INDEX_UV0);
    glVertexAttribPointer(ATTRIB_INDEX_UV0, 2, GL_FLOAT, GL_FALSE, sizeof(RenderVertex), (void*)RENDER_VERTEX_UV0_OFFSET);
    glEnableVertexAttribArray(ATTRIB_INDEX_UV1);
    glVertexAttribPointer(ATTRIB_INDEX_UV1, 2, GL_FLOAT, GL_FALSE, sizeof(RenderVertex), (void*)RENDER_VERTEX_UV1_OFFSET);
	
    glBindVertexArray(0);
}

VertexArrayObject::~VertexArrayObject()
{
    glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vbo.buffer);
	glDeleteBuffers(1, &m_ibo.buffer);
}

void generic_alloc(GLenum target, GLuint handle, GLintptr &offset, GLsizeiptr size, void *data)
{
	assert(data != NULL);
    assert(size != 0);

    glBindBuffer(target, handle);
    glBufferSubData(target, offset, size, data);
    glBindBuffer(target, 0);

	offset += size;
}

void VertexArrayObject::vbo_alloc(GLsizeiptr size, void *data)
{
	if(m_vbo.offset + size > m_vbo.reserved) return;
    generic_alloc(GL_ARRAY_BUFFER, m_vbo.buffer, m_vbo.offset, size, data);
}

void VertexArrayObject::ibo_alloc(GLsizeiptr size, void *data)
{
	if(m_ibo.offset + size > m_ibo.reserved) return;
	generic_alloc(GL_ELEMENT_ARRAY_BUFFER, m_ibo.buffer, m_ibo.offset, size, data);
}

void VertexArrayObject::clear()
{
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo.buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo.buffer);
    glBufferData(GL_ARRAY_BUFFER, m_vbo.reserved, NULL, m_vbo.usage);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_ibo.reserved, NULL, m_ibo.usage);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    m_vbo.offset = 0;
	m_ibo.offset = 0;
}

void VertexArrayObject::bind()
{
    glBindVertexArray(m_vao);
}
