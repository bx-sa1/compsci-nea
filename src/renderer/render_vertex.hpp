#ifndef RENDER_VERTEX_H_
#define RENDER_VERTEX_H_

#include "Eigen/Core"

//struct representation of a vertex that will be sent to opengl
//subject to change
struct RenderVertex
{
    Eigen::Vector3f pos; //12 bytes
    Eigen::Vector3f normal; //12 bytes
    Eigen::Vector2f uv0; //8 bytes
    Eigen::Vector2f uv1; // 8bytes
    unsigned char colour0[4]; //4 bytes;
    unsigned char colour1[4];// 4 bytes;
};

#define RENDER_VERTEX_POS_OFFSET offsetof(RenderVertex, pos)
#define RENDER_VERTEX_NORMAL_OFFSET offsetof(RenderVertex, normal)
#define RENDER_VERTEX_UV0_OFFSET offsetof(RenderVertex, uv0)
#define RENDER_VERTEX_UV1_OFFSET offsetof(RenderVertex, uv1)
#define RENDER_VERTEX_COLOUR0_OFFSET offsetof(RenderVertex, colour0)
#define RENDER_VERTEX_COLOUR1_OFFSET offsetof(RenderVertex, colour1)

#endif
