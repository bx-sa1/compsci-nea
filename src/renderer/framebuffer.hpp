#ifndef FRAMEBUFFER_H_
#define FRAMEBUFFER_H_

#include "glad/glad.h"
#include "renderer/texture/texture.hpp"
#include <vector>

//OpenGL framebuffer wrapper
class Framebuffer
{
public:
    Framebuffer();
    Framebuffer(float width, float height);
    ~Framebuffer();

    void bind();

    void attachColourBuffer(int index);
    void attachDepthBuffer();
    void attachTexture(Texture *texture, int index);

    void checkCompleteness();
private:
	GLuint id;
	std::vector<GLuint> render_buffers;

	float width;
	float height;
};

#endif
