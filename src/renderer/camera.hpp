#ifndef CAMERA_H_
#define CAMERA_H_

#include "eigen/Eigen"
#include "utils/frustum.hpp"

class Camera
{
public:
    Camera();
    Camera(float near, float far, float vfov, float aspect);
    Camera(float near, float far, float left, float right, float top, float bottom);
    ~Camera();

    //all the planes face inwards
    Frustum frustum(const Eigen::Matrix4f &model_matrix);

    Eigen::Matrix4f projection_matrix;

	float near;
    float far;

    Eigen::Affine3f transform;
};

#endif
