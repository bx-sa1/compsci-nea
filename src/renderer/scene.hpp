#ifndef SCENE_H_
#define SCENE_H_

#include "model.hpp"
#include "light_info.hpp"
#include "draw_mesh.hpp"
#include "camera.hpp"

struct PerFrameSceneData
{
    std::vector<std::pair<uint64_t, DrawMesh>> meshes;
    std::vector<std::pair<LightInfo*, std::vector<int>>> lights;
};

class Scene
{
public:
    Scene();
    ~Scene();

	void clear()
	{
		models.clear();
		lights.clear();
	}

    void add_model(Model *model) { models.push_back(model); }
    void add_light(LightInfo *info) { lights.push_back(info); }

	void remove_model(Model *model);
	void remove_light(LightInfo *info);

    PerFrameSceneData *generate_frame_data(Camera &camera);

    std::vector<Model*> models;
    std::vector<LightInfo*> lights;
};

#endif
