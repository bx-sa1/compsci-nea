#include "debug/logger.hpp"
#include "utils/file.hpp"
#include "utils/file_system.hpp"

#define NK_IMPLEMENTATION
#include "nuklear_backend.hpp"

#include "renderer/material.hpp"
#include "renderer/model.hpp"
#include <SDL_keycode.h>
#include <SDL_mouse.h>
#include <SDL_rwops.h>
#include "renderer/texture/texture.hpp"
#include "renderer/texture/texture2d.hpp"
#include "renderer/view.hpp"

namespace
{
	const char *font_mat_src =	"{" \
								"	\"vs_shader\": \"shaders/ui.vert\"," \
								"	\"fs_shader\": \"shaders/ui.frag\"," \
								"	\"params\": {"
								"		\"translucent\": true"
								"	}"
								"}";
	struct nk_backend
	{
		nk_context ctx;

		Model *model;
		View view;

		nk_buffer command_buffer;
		nk_convert_config convert_config;
		nk_draw_null_texture null_texture;
	};

	struct nk_vertex
	{
		float pos[2];
		float uv[2];
		unsigned char col[4];
	};

	bool nk__init_resources(nk_context *ctx)
	{
		SDL_RWops *mat_nuklear = NULL;

		nk_backend *bck = (nk_backend*)ctx->userdata.ptr;

		//init vertex config
		static const nk_draw_vertex_layout_element vert_layout[] = 
		{
			{NK_VERTEX_POSITION, NK_FORMAT_FLOAT, NK_OFFSETOF(nk_vertex, pos)},
			{NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, NK_OFFSETOF(nk_vertex, uv)},
			{NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8, NK_OFFSETOF(nk_vertex, col)},
			{NK_VERTEX_LAYOUT_END}
		};
		bck->convert_config.shape_AA = NK_ANTI_ALIASING_ON;
		bck->convert_config.line_AA = NK_ANTI_ALIASING_ON;
		bck->convert_config.vertex_layout = vert_layout;
		bck->convert_config.vertex_size = sizeof(nk_vertex);
		bck->convert_config.vertex_alignment = NK_ALIGNOF(nk_vertex);
		bck->convert_config.circle_segment_count = 22;
		bck->convert_config.curve_segment_count = 22;
		bck->convert_config.arc_segment_count = 22;
		bck->convert_config.global_alpha = 1.0f;

		nk_buffer_init_default(&bck->command_buffer);

		bck->model = new Model;
		bck->model->model_matrix = Eigen::Matrix4f::Identity();

		return true;
	}
};

nk_context *nk_create_context()
{
	nk_backend *bck = new nk_backend;
	if(bck == NULL)
	{
		return NULL;
	}

	if(!nk_init_default(&bck->ctx, NULL))
	{
		delete bck;
		return NULL;
	}
	nk_set_user_data(&bck->ctx, nk_handle_ptr(bck));
	
	nk__init_resources(&bck->ctx);

	return &bck->ctx;
}

//TODO: free font mat as well
void nk_delete_context(nk_context *ctx)
{
	nk_backend *bck = (nk_backend*)ctx->userdata.ptr;

	nk_free(ctx);
	delete bck->model;
	delete bck;
}

void nk_render(nk_context *ctx, Renderer *renderer)
{
	nk_backend *bck = (nk_backend*)ctx->userdata.ptr;

	bck->view.scene.clear();
	bck->model->meshes().clear();

	nk_buffer verts, idxs;
	nk_buffer_init_default(&verts);
	nk_buffer_init_default(&idxs);
	nk_convert(ctx, &bck->command_buffer, &verts, &idxs, &bck->convert_config);

    int offset = 0;
	const nk_draw_command *cmd;
	nk_draw_foreach(cmd, ctx, &bck->command_buffer)
	{
		if(!cmd->elem_count) continue;
		
		ModelMesh mesh;
		mesh.mat = (Material*)cmd->texture.ptr;

		std::vector<RenderVertex> mesh_verts;
		std::vector<unsigned int> mesh_indices;
        
		for(int i = 0; i < cmd->elem_count; i++)
		{
			unsigned short idx = ((unsigned short*)idxs.memory.ptr)[i+offset];
            nk_vertex vert = ((nk_vertex*)verts.memory.ptr)[idx];
            
            RenderVertex new_vert = 
			{
				Eigen::Vector3f(vert.pos[0], 0, vert.pos[1]), //swap y and z to conform with engine coordinate system
				Eigen::Vector3f::Zero(),
				Eigen::Vector2f(vert.uv[0], vert.uv[1]),
				Eigen::Vector2f::Zero(),
				{vert.col[0], vert.col[1], vert.col[2], vert.col[3]},
				{0,0,0,0}
			};
			mesh.bounds.extend(new_vert.pos);
			bck->model->overall_bounds().extend(new_vert.pos);

            mesh.verts.push_back(new_vert);
			mesh.idxs.push_back(i);
		}

		bck->model->meshes().push_back(mesh);
        
        offset += cmd->elem_count;
	}

	bck->view.scene.add_model(bck->model);
	renderer->renderView(bck->view); //dont sort the draw calls
	
	//free buffers
	nk_buffer_free(&verts);
	nk_buffer_free(&idxs);

	//clear nuklear frame
	nk_clear(ctx);
	nk_buffer_clear(&bck->command_buffer);
}

void nk_set_camera(nk_context *ctx, Camera &cam)
{
	nk_backend *bck = (nk_backend*)ctx->userdata.ptr;

	bck->view.camera = cam;
}

void nk_font_stash_begin(nk_context *ctx, nk_font_atlas *atlas)
{
	nk_font_atlas_init_default(atlas);
	nk_font_atlas_begin(atlas);
}

void nk_font_stash_end(nk_context *ctx, nk_font_atlas *atlas)
{
	nk_backend *bck = (nk_backend *)ctx->userdata.ptr;

	const void *image;
	int width, height;
	Material *font_mat;
	Texture2D *texture;

	image = nk_font_atlas_bake(atlas, &width, &height, NK_FONT_ATLAS_RGBA32);

	//create the texture
	//texture->set_filtering(BILINEAR);
	//texture->set_wrapping(CLAMP_EDGE);
	texture = assets::load_texture_2d((unsigned char *)image, width, height, TextureFormat::RGBA);

	//create the material
	font_mat = assets::load_material((unsigned char *)font_mat_src, strlen(font_mat_src));
	font_mat->add_texture("ui_tex", texture);

	nk_font_atlas_end(atlas, nk_handle_ptr(font_mat), &bck->null_texture);
	bck->convert_config.null = bck->null_texture;
}

nk_font *nk_font_stash_add_from_file(nk_font_atlas *atlas, int height, std::string path)
{
	nk_font *font = NULL;
	File::Ptr file = filesystem::open_file(path.c_str(), "r");
	if(!file)
	{
		LOG_ERROR("Failed to open file");
		return NULL;
	}

	size_t size = filesystem::get_file_size(file.get());
	unsigned char *buffer = new unsigned char[size];
	if(!fread(buffer, sizeof(char), size, file.get()))
	{
		LOG_ERROR("FAILED TO READ FILE");
		goto error;
	}

	font = nk_font_atlas_add_from_memory(atlas, buffer, size, height, NULL);
	delete[] buffer;
	return font;

error:
	delete[] buffer;
	return NULL;
}
		
nk_keys nk_input_key_sdl_to_nk(SDL_Keycode code, Uint16 mod)
{
	switch(code)
	{
		case SDLK_RSHIFT:
		case SDLK_LSHIFT:
			return NK_KEY_SHIFT;
		case SDLK_DELETE:
			return NK_KEY_DEL;
		case SDLK_RETURN:
			return NK_KEY_ENTER;
		case SDLK_TAB:
			return NK_KEY_TAB;
		case SDLK_BACKSPACE:
			return NK_KEY_BACKSPACE;
		case SDLK_HOME:
			return NK_KEY_TEXT_START;
		case SDLK_END:
			return NK_KEY_TEXT_END;
		case SDLK_PAGEDOWN:
			return NK_KEY_SCROLL_DOWN;
		case SDLK_PAGEUP:
			return NK_KEY_SCROLL_UP;
		case SDLK_z:
			if(mod & KMOD_LCTRL) return NK_KEY_TEXT_UNDO;
			else return NK_KEY_MAX;
		case SDLK_y:
			if(mod & KMOD_LCTRL) return NK_KEY_TEXT_REDO;
			else return NK_KEY_MAX;
		case SDLK_c:
			if(mod & KMOD_LCTRL) return NK_KEY_COPY;
			else return NK_KEY_MAX;
		case SDLK_v:
			if(mod & KMOD_LCTRL) return NK_KEY_PASTE;
			else return NK_KEY_MAX;
		case SDLK_p:
			if(mod & KMOD_LCTRL) return NK_KEY_CUT;
			else return NK_KEY_MAX;
		case SDLK_b:
			if(mod & KMOD_LCTRL) return NK_KEY_TEXT_LINE_START;
			else return NK_KEY_MAX;
		case SDLK_e:
			if(mod & KMOD_LCTRL) return NK_KEY_TEXT_LINE_END;
			else return NK_KEY_MAX;
		case SDLK_UP:
			return NK_KEY_UP;
		case SDLK_DOWN:
			return NK_KEY_DOWN;
		case SDLK_LEFT:
			if (mod & KMOD_LCTRL) return NK_KEY_TEXT_WORD_LEFT;
			else return NK_KEY_LEFT;
		case SDLK_RIGHT:
			if (mod & KMOD_LCTRL) return NK_KEY_TEXT_WORD_RIGHT;
			else return NK_KEY_RIGHT;
		default:
			return NK_KEY_MAX;
	}
}

nk_buttons nk_input_button_sdl_to_nk(Uint8 button)
{
	switch(button)
	{
		case SDL_BUTTON_LEFT:
			return NK_BUTTON_LEFT;
		case SDL_BUTTON_MIDDLE:
			return NK_BUTTON_MIDDLE;
		case SDL_BUTTON_RIGHT:
			return NK_BUTTON_RIGHT;
		default:
			return NK_BUTTON_MAX;
	}
}
	
