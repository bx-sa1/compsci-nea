#ifndef GPU_RESOURCE_H_
#define GPU_RESOURCE_H_

#include "glad/glad.h"

class GPUResource
{
public:
    GPUResource() : 
        m_handle(0),
        m_handle_ready(false)
    {}
    virtual ~GPUResource(){}

    /**
     * Gets the opengl handle to a resoure.
     * This function can perform an upload to GPU
     * of some data.
     **/
    virtual GLuint get_handle() = 0;

protected:
    //Handle
    GLuint m_handle;
    //Is the handle ready to use?
    bool m_handle_ready;
};

#endif
