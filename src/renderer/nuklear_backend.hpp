#ifndef	NUKLEAR_BACKEND_H_
#define NUKLEAR_BACKEND_H_

#include "renderer/renderer.hpp"
#include "renderer/view.hpp"
#include "ui/nuklear_with_defs.h"
#include <SDL_keycode.h>

nk_context *nk_create_context();
void nk_delete_context(nk_context *ctx);
void nk_render(nk_context *ctx, Renderer *renderer);
void nk_set_camera(nk_context *ctx, Camera &cam);
void nk_font_stash_begin(nk_context *ctx, nk_font_atlas *atlas);
void nk_font_stash_end(nk_context *ctx, nk_font_atlas *atlas);
nk_font *nk_font_stash_add_from_file(nk_font_atlas *atlas, int height, std::string path);
nk_keys nk_input_key_sdl_to_nk(SDL_Keycode code, Uint16 mod);
nk_buttons nk_input_button_sdl_to_nk(Uint8 button);

#endif
