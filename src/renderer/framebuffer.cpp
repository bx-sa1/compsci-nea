#include "framebuffer.hpp"

#include "debug/logger.hpp"
#include <cassert>
#include "renderer/texture/texture2d.hpp"

Framebuffer::Framebuffer() :
	width(1),
	height(1)
{
	glGenFramebuffers(1, &id);
}

Framebuffer::Framebuffer(float width, float height) :
	width(width),
	height(height)
{
	glGenFramebuffers(1, &id);
}

Framebuffer::~Framebuffer()
{
	glDeleteRenderbuffers(render_buffers.size(), render_buffers.data());
	glDeleteFramebuffers(1, &id);
}

void Framebuffer::bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void Framebuffer::attachColourBuffer(int index)
{
	GLuint rb_id;
	glGenRenderbuffers(1, &rb_id);

    glBindRenderbuffer(GL_RENDERBUFFER, rb_id);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+index, GL_RENDERBUFFER, rb_id);

	render_buffers.push_back(rb_id);
}

void Framebuffer::attachDepthBuffer()
{
	GLuint rb_id;
	glGenRenderbuffers(1, &rb_id);

    glBindRenderbuffer(GL_RENDERBUFFER, rb_id);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb_id);

	render_buffers.push_back(rb_id);
}

void Framebuffer::attachTexture(Texture *texture, int index)
{
	int face_index = index;
	GLenum type, attachment;
	if(dynamic_cast<Texture2D*>(texture))
	{
		type = GL_TEXTURE_2D; face_index = 0;
	}
	//else if(dynamic_cast<Cubemap*>(texture))
	//{
		//type = GL_TEXTURE_CUBE_MAP_POSITIVE_X;
	//}
	else
	{
		assert(false);
	}

	attachment = GL_COLOR_ATTACHMENT0;

	if(type == GL_TEXTURE_2D) 
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, attachment+index, type+face_index, texture->get_handle(), 0);
	}
	else
	{
		glFramebufferTextureLayer(GL_FRAMEBUFFER, attachment+index, texture->get_handle(), 0, face_index);
	}
}

void Framebuffer::checkCompleteness()
{
	bool t = glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE;
	(void)0;
}
