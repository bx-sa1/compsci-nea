#ifndef DRAW_MESH_H_
#define DRAW_MESH_H_

#include "Eigen/Eigen"
#include "model.hpp"

/**
 * This structure represents a single draw call to GPU
 * (a very high level representation).
 * It is used internally by the Renderer class as description of
 * what will be renderered.
 **/
struct DrawMesh
{
	//Is this draw call affected by light?
	bool lit;

	//Reference to the owning model matrix.
	//Since models are kept in memory over the course of a
	//a level, this will never be a dangling pointer
	Eigen::Matrix4f *model_matrix;

	//Reference to one of a models meshes.
	//This follows the same rules as the model matrix
	const ModelMesh *mesh;


	const std::vector<ModelJoint> *joints; //pointer to the array of joints of the model the mesh came from
};

#endif
