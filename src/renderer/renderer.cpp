#include "renderer/renderer.hpp"

#include "Eigen/Eigen"
#include "renderer/scene.hpp"
#include "utils/concommand.hpp"
#include "utils/convar.hpp"
#include "debug/logger.hpp"
#include "renderer/framebuffer.hpp"
#include "glad/glad.h"
#include "renderer/texture/texture2d.hpp"
#include "renderer/material.hpp"
#include "renderer/render_triangle.hpp"
#include "renderer/vertex_buffer.hpp"
#include <cmath>
#include "renderer/shader.hpp"
#include <cassert>
#include "renderer/render_vertex.hpp"
#include "game.hpp"
#include "renderer/view.hpp"
#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <string>
#include <utility>
#include "shadow_map.hpp"
#include "renderer/model.hpp"
#include "utils/sort.hpp"
#include "scene.hpp"

const size_t BUFFER_SIZE = 4 * (2 << 20); 

static RenderVertex screen_quad_verts[4] = {
    {Eigen::Vector3f(-1, 1, 0), Eigen::Vector3f(), Eigen::Vector2f(), Eigen::Vector2f(), {0}, {0}},
    {Eigen::Vector3f(-1, -1, 0), Eigen::Vector3f(), Eigen::Vector2f(), Eigen::Vector2f(), {0}, {0}},
    {Eigen::Vector3f(1, -1, 0), Eigen::Vector3f(), Eigen::Vector2f(), Eigen::Vector2f(), {0}, {0}},
    {Eigen::Vector3f(1, 1, 0), Eigen::Vector3f(), Eigen::Vector2f(), Eigen::Vector2f(), {0}, {0}}
};

static unsigned int screen_quad_tris[6] = {
    0, 2, 1,
    0, 3, 2
};

const Eigen::Matrix4f Renderer::yup_to_zup = (Eigen::Matrix4f() << 1,0,0,0, 0,0,1,0, 0,-1,0,0, 0,0,0,1).finished();

static ConVar cv_shadow_debug_draw("shadowmap_debug_draw", false, "debug rendering of shadow map");
static ConVar cv_graphics_shadows("graphics_shadows", true, "enable shadow rendering");
static ConVar cv_ambient_colour("ambient_colour", 0.1, "ambient colour");

Renderer::Renderer() :
    m_begun(false)
{
	vao = new VertexArrayObject(BUFFER_SIZE/4, BUFFER_SIZE/4, GL_DYNAMIC_DRAW);
	static_vao = new VertexArrayObject(BUFFER_SIZE/4, BUFFER_SIZE/4, GL_STATIC_DRAW);
	colorbuffer_vao = new VertexArrayObject(4*sizeof(RenderVertex), 6*sizeof(unsigned int), GL_STATIC_DRAW);
	colorbuffer_vao->vbo_alloc(4*sizeof(RenderVertex), screen_quad_verts);
	colorbuffer_vao->ibo_alloc(6*sizeof(unsigned int), screen_quad_tris);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//TODO: add back DD
    float res_scale = utils::get_convar("res_scale")->getNumber();
    float width = utils::get_convar("res_width")->getNumber()/res_scale;
    float height = utils::get_convar("res_height")->getNumber()/res_scale;

	//init colour buffer
	m_colourbuffer_tex = assets::load_texture_2d(NULL, width, height, TextureFormat::RGBA);
	//m_colourbuffer_tex->set_wrapping(REPEAT);
	//m_colourbuffer_tex->set_filtering(NO_FILTERING);

    m_colourbuffer = new Framebuffer(width, height);
    m_colourbuffer->bind();
    m_colourbuffer->attachTexture(m_colourbuffer_tex, 0);
    m_colourbuffer->attachDepthBuffer();
    m_colourbuffer->checkCompleteness();
#if 0
    if(cv_graphics_shadows.getBool()
    {
        ShadowMap_createshadowMap(&m_shadow_map_atlas_fbo, &m_shadow_map_atlas_tex);
    }
#endif
}

Renderer::~Renderer()
{
	delete m_colourbuffer_tex;
	delete m_colourbuffer;
}

static void draw_mesh(const ModelMesh &mesh)
{
	mesh.vao_ptr.vao_id->bind();

	glDrawElements(GL_TRIANGLES, (mesh.vao_ptr.size/sizeof(unsigned int)), GL_UNSIGNED_INT, (void*)mesh.vao_ptr.offset);
	//mat->shader->validate();

	glBindVertexArray(0);
}

void add_to_vao(VertexArrayObject *vao, Model *model, ModelMesh &mesh)
{
	if(model->is_static() && mesh.vao_ptr.size != 0) return;
	
	mesh.vao_ptr.offset = vao->ibo().offset;
	mesh.vao_ptr.size = mesh.idxs.size()*sizeof(unsigned int);
	mesh.vao_ptr.vao_id = vao;

	unsigned int *new_idx = new unsigned int[mesh.idxs.size()];
	for(int i = 0; i < mesh.idxs.size(); i++)
	{
		new_idx[i] = mesh.idxs[i] + (vao->vbo().offset/sizeof(RenderVertex));
	}
	vao->vbo_alloc(mesh.verts.size()*sizeof(RenderVertex), mesh.verts.data());
	vao->ibo_alloc(mesh.idxs.size()*sizeof(unsigned int), new_idx);
	delete[] new_idx;

}

void Renderer::beginRender()
{
	assert(!m_begun);
	m_begun = true;
}

void Renderer::renderView(const View &view)
{
	m_views.push_back(view);

	for(Model *m : view.scene.models)
	{
		for(ModelMesh &mesh : m->meshes())
		{
			if(m->is_static())
			{
				add_to_vao(static_vao, m, mesh);
			}
			else
			{
				add_to_vao(vao, m, mesh);
			}
		}
	}
}

void Renderer::endRender()
{
	assert(m_begun);

	flush(m_colourbuffer);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	//draw colourbuffer
	Material *post_process_mat = assets::load_material("materials/post_process.mat");
	post_process_mat->textures()["colourbuffer"] = m_colourbuffer_tex;
	use_material(post_process_mat, true);

	ModelMesh colorbuffer_mesh;
	colorbuffer_mesh.vao_ptr.offset = 0;
	colorbuffer_mesh.vao_ptr.size = 6*sizeof(unsigned int);
	colorbuffer_mesh.vao_ptr.vao_id = colorbuffer_vao;
	draw_mesh(colorbuffer_mesh);

	glUseProgram(0);

	m_begun = false;
}

void set_pass_opengl_state(GLenum depth_func = GL_LEQUAL,
		bool color_mask = true,
		bool depth_mask = true,
		GLenum blend_func_a = GL_ONE,
		GLenum blend_func_b = GL_ZERO)
{
	glDepthFunc(depth_func);
	if(color_mask) glColorMask(1,1,1,1);
	else glColorMask(0,0,0,0);
	glDepthMask(depth_mask ? GL_TRUE : GL_FALSE);
	glBlendFunc(blend_func_a, blend_func_b);
}

void Renderer::flush(Framebuffer *fb)
{
	assert(m_begun);
	assert(fb);

	fb->bind();

	glEnable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	
	//default
	set_pass_opengl_state();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	//ui and models go through here
	for(View &v : m_views)
	{
		PerFrameSceneData *pfsd = v.scene.generate_frame_data(v.camera);

		v.viewport.apply(); 

		depth_prepass(v, pfsd);
		ambient_pass(v, pfsd);
		render_lit(v, pfsd);
		render_non_lit(v, pfsd);

		delete pfsd;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	//clear views
	m_views.clear();

	//clear vao
	vao->clear();

}



/**************************
 * MATERIAL UNIFORM UTILS *
 **************************/
static void set_mvp(GLuint prog, Camera &cam, Eigen::Matrix4f &model_matrix)
{
	Eigen::Matrix4f proj_mat = cam.projection_matrix;
	Eigen::Matrix4f view_mat = cam.transform.matrix().inverse();
	view_mat = Renderer::yup_to_zup * view_mat;

	GLuint u_proj = glGetUniformLocation(prog, "g_projectionMatrix");
	glUniformMatrix4fv(u_proj, 1, GL_FALSE, proj_mat.data());

	GLuint u_view = glGetUniformLocation(prog, "g_viewMatrix");
	glUniformMatrix4fv(u_view, 1, GL_FALSE, view_mat.data());

	GLuint u_model = glGetUniformLocation(prog, "g_modelMatrix");
	glUniformMatrix4fv(u_model, 1, GL_FALSE, model_matrix.data());
}

static void set_view_pos(GLuint prog, Camera &cam)
{

	GLuint view_pos = glGetUniformLocation(prog, "view_pos");
	glUniform3fv(view_pos, 1, cam.transform.translation().data());
}

static bool set_light_uniforms(GLuint prog, const LightInfo &light)
{
	GLuint light_type = glGetUniformLocation(prog, "light.type");
	GLuint light_pd = glGetUniformLocation(prog, "light.pd");
	GLuint light_colour = glGetUniformLocation(prog, "light.colour");
	GLuint light_coeffs = glGetUniformLocation(prog, "light.coeffs");

	if(light_type == -1) return false;
	glUniform1i(light_type, (int)light.type);
	if(light_pd == -1) return false;
	glUniform3fv(light_pd, 1, light.pd.data());
	if(light_colour == -1) return false;
	glUniform4fv(light_colour, 1, light.light_colour.data());
	if(light_coeffs == -1) return false;
	glUniform3fv(light_coeffs, 1, light.coeffs.data());

	return true;
}

/*****************
 * RENDER PASSES *
 *****************/
void Renderer::depth_prepass(View &view, PerFrameSceneData *pfsd)
{
	Material *depth_prepass_mat = assets::load_material("materials/depth_prepass.mat");
	use_material(depth_prepass_mat, false);
	GLuint dpm_prog = depth_prepass_mat->get_handle();

	set_pass_opengl_state(GL_LESS, false);

	for(auto &dm : pfsd->meshes)
	{
		//dont render transparent materials to depth map
		auto translucent = dm.second.mesh->mat->flags().find("translucent");
		if(translucent != dm.second.mesh->mat->flags().end() && translucent->second)
		{
			continue;
		}

		set_mvp(dpm_prog, view.camera, *dm.second.model_matrix);

		draw_mesh(*dm.second.mesh);
	}

	glUseProgram(0);
}

void Renderer::ambient_pass(View &view, PerFrameSceneData *pfsd)
{
	Material *ambient_pass_mat = assets::load_material("materials/ambient_pass.mat");
	use_material(ambient_pass_mat, true);
	GLuint apm_prog = ambient_pass_mat->get_handle();

	set_pass_opengl_state(GL_LEQUAL, true, false, GL_ONE, GL_ZERO);

	for(auto &dm : pfsd->meshes)
	{
		set_mvp(apm_prog, view.camera, *dm.second.model_matrix);

		//ambient cubes?
		
		//set ambient colour
		GLuint ac_id = glGetUniformLocation(ambient_pass_mat->get_handle(), "ambient_colour");
		Eigen::Vector3f ambi = Eigen::Vector3f::Constant(cv_ambient_colour.getNumber());
		glUniform3fv(ac_id, 1, ambi.data());

		draw_mesh(*dm.second.mesh);
	}

	glUseProgram(0);
}

void Renderer::render_lit(View &view, PerFrameSceneData *pfsd)
{
	Material *current_mat = NULL;

	glDepthFunc(GL_LEQUAL);
	glColorMask(1,1,1,1);
	glDepthMask(GL_FALSE);

	for(auto &light_pair : pfsd->lights)
	{
		//update the shadow map 
		//

		for(int dm_idx : light_pair.second)
		{
			DrawMesh &dm = pfsd->meshes[dm_idx].second;
			if(current_mat != dm.mesh->mat)
			{
				if(use_material(dm.mesh->mat, true) == -1)
				{
					continue;
				}
				current_mat = dm.mesh->mat;
			}

			GLuint prog = current_mat->get_handle();

			//set light uniforms
			if(set_light_uniforms(prog, *light_pair.first))
			{
				//this means that the shader does use any lighting
				dm.lit = true; 
			}

			set_mvp(prog, view.camera, *dm.model_matrix);
			set_view_pos(prog, view.camera);

			auto translucent = current_mat->flags().find("translucent");
			if(translucent != current_mat->flags().end() && translucent->second)
			{
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			}
			else
			{
				glBlendFunc(GL_ONE, GL_ONE);
			}
			
			//render pair
			draw_mesh(*dm.mesh);
		}
	}

	glUseProgram(0);
}

void Renderer::render_non_lit(View &view, PerFrameSceneData *pfsd)
{
	Material *current_mat = NULL;

	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);


	for(auto &draw_pair : pfsd->meshes)
	{
		const DrawMesh &dm = draw_pair.second;
		if(dm.lit) return;
		if(current_mat != dm.mesh->mat)
		{
			if(use_material(dm.mesh->mat, true) == -1) //shaders not ready yet, skip
			{
				continue;
			}
			current_mat = dm.mesh->mat;
		}

		GLuint prog = current_mat->get_handle();

		set_mvp(prog, view.camera, *dm.model_matrix);
		set_view_pos(prog, view.camera);

		auto translucent = current_mat->flags().find("translucent");
		if(translucent != current_mat->flags().end())
		{
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
		else
		{
			glBlendFunc(GL_ONE, GL_ZERO);
		}

		//render pair
		draw_mesh(*dm.mesh);
	}

	glUseProgram(0);
}

int Renderer::use_material(Material *mat, bool set_params)
{
	GLuint prog = mat->get_handle();

	glUseProgram(prog);

	int tex_used = 0;
	if(set_params)
	{
		for(const auto &texture : mat->textures())
		{
			std::string tex_name = texture.first;
			Texture *tex = texture.second;

			GLuint id = glGetUniformLocation(prog, tex_name.c_str());
			glUniform1i(id, tex_used);

			if(texture.second)
			{
				GLuint tex_id = tex->get_handle();
				bind_texture(tex->texture_type(), tex_id, tex_used++);
			}
			else
			{
#if 0
				LOG_WARNING("null texture for texture %s, in material %s", 
						tex_name.c_str(),) 
#endif
			}
		}

		for(const auto &uniform : mat->uniforms())
		{
			std::string uname = uniform.first;
			auto udata = uniform.second;
			set_uniform(prog, uname, udata);
		}
	}
	
	return tex_used;
}

void Renderer::bind_texture(GLenum texture_type, GLuint texture, int unit)
{
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(texture_type, texture);
}

void Renderer::set_uniform(GLuint prog, std::string name, std::array<float, 4> data)
{
	GLuint id = glGetUniformLocation(prog, name.c_str());
	glUniform4fv(id, 1, data.data());
}


