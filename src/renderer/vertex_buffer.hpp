#ifndef VERTEX_CACHE_H_
#define VERTEX_CACHE_H_

#include <inttypes.h>
#include "glad/glad.h"
#include "renderer/render_vertex.hpp"

/**
 * OpenGL buffer (vertex and index) implementation.
 * Handles STATIC data and DYNAMIC data.
**/

class VertexArrayObject;

struct GLBuffer
{
	GLuint buffer;
	GLintptr offset;
	GLsizeiptr reserved;
	GLenum usage;
};

class VertexArrayObject
{
public:
    VertexArrayObject(size_t vbo_size, size_t ibo_size, GLenum usage);
    ~VertexArrayObject();

    void bind();
    void unbind();

	void clear();
    
	void vbo_alloc(GLsizeiptr size, void *data);
	void ibo_alloc(GLsizeiptr size, void *data);

	const GLBuffer vbo() const { return m_vbo; }
	const GLBuffer ibo() const { return m_ibo; }

private:
	GLuint m_vao;
	GLBuffer m_vbo, m_ibo;
};

#endif
