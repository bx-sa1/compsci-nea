#ifndef LIGHT_H_
#define LIGHT_H_

#include <cstring>
#include "Eigen/Eigen"

enum class LightType { DIRECTIONAL, POINT, SPOT };

struct LightInfo
{
    LightType type;

    Eigen::Vector3f pd = Eigen::Vector3f::Zero(); //position or direcion
    Eigen::Vector4f light_colour = Eigen::Vector4f::Zero(); //brightness is last
    Eigen::Vector3f coeffs = Eigen::Vector3f::Zero(); //light attentuation coefficients

	Eigen::AlignedBox3f bounds;
};

#endif
