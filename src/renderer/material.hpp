#ifndef MATERIAL_H_
#define MATERIAL_H_

#include <array>
#include <vector>
#include <string>
#include "gpu_resource.hpp"
#include "shader.hpp"
#include "texture/texture.hpp"
#include "texture/texture2d.hpp"
#include <map>
#include <queue>
#include <utility>

typedef std::map<std::string, Texture *> TextureMap;
typedef std::map<std::string, std::array<float, 4>> UniformMap;
typedef std::map<std::string, bool> FlagMap;

class Material;

namespace assets
{
	Material *load_material(std::string path);
	Material *load_material(unsigned char *data, size_t read);

	extern Material *material_unavailable;
	extern Material *material_white;
	extern Material *material_black;
};

/**
 * Material resource class.
 * A material is the interface for textures and shaders:
 * they cannot be used without a material.
 **/
class Material : public GPUResource
{
public:
	Material();
	~Material();

	GLuint get_handle();

	void add_texture(std::string name, Texture *tex) { m_textures[name] = tex; }
	void set_vertex_shader(VertexShader *shader) 
	{ 
		m_shaders.vs = shader; 
		m_handle_ready = false;
	}

	void set_fragment_shader(FragmentShader *shader) 
	{ 
		m_shaders.fs = shader;
		m_handle_ready = false;
	}

	TextureMap &textures() { return m_textures; }
	UniformMap &uniforms() { return m_uniforms; }
	FlagMap &flags() { return m_flags; }
private:
	struct ShaderSources
	{
		VertexShader *vs = NULL;
		FragmentShader *fs = NULL;
	} m_shaders;
	
    TextureMap m_textures;
	UniformMap m_uniforms;
	FlagMap m_flags;
};

#endif
