#ifndef SHADER_H_
#define SHADER_H_

#include <string>
#include "glad/glad.h"
#include "gpu_resource.hpp"
#include <vector>
#include "utils/asset_management_utils.hpp"

//defines for the indices of different attributes in a
//vertex
#define ATTRIB_INDEX_POSITION 0
#define ATTRIB_INDEX_NORMAL 2
#define ATTRIB_INDEX_COLOUR0 3
#define ATTRIB_INDEX_COLOUR1 4
#define ATTRIB_INDEX_UV0 8
#define ATTRIB_INDEX_UV1 9

typedef std::vector<std::string> Defines;

namespace internal
{
	GLuint create_program(GLuint vs, GLuint fs);
	GLuint create_shader(GLenum type, const char *source, const char *defines);

	/**
	 * This function gets a GLSL source file,
	 * finds any lines that begin with #include
	 * ans replaces it with the file that #include is pointing 
	 * at; exactly the same as #include in c/c++
	 **/
	std::string process_shader_source(std::string source);
	/**
	 * This function gets a list of define strings,
	 * puts #define in front of them and appends them to
	 * a new string, separated by a line feed.
	 **/
	std::string process_shader_defines(const Defines &defines);
};

namespace assets
{
	template <class T>
	T *load_shader(unsigned char *data, size_t size, Defines &defines)//not cached, owned by caller
	{
		std::string shader_data((char *)data, size);

		std::string source_string = ::internal::process_shader_source(shader_data);
		if(source_string.empty()) return NULL;

		std::string defines_string = ::internal::process_shader_defines(defines);

		T *shader = new T;
		shader->set_source(source_string);
		shader->set_defines(defines_string);

		return shader;
	}

	template <class T>
	T *load_shader(std::string path, Defines &defines)
	{
		auto &cache = internal::get_asset_cache<T>();
		T *shader = internal::find_in_cache<T>(cache, path);
		if(shader)
		{
			return shader;
		}

		std::vector<unsigned char> buffer;
		size_t read;
		if(internal::load_file(path, "r", buffer, read))
		{
			shader = load_shader<T>(buffer.data(), read, defines);
			if(shader)
			{
				cache.insert({path, shader});
				return shader;
			}
		}

		delete shader;
		return NULL;
	}

};


/**
 * Shader resource class.
 * Holds the compiled shader and its source code.
 * A shader can be given extra, compile time defines
 * that can change how the shader functions; exactly the same as 
 * c/c++ defines.
 **/
class Shader : public GPUResource
{
public:
    Shader();
    ~Shader();

	/**
	 * If called for the first time on a loaded shader,
	 * it compiles the shader and returns the handle.
	 **/
	virtual GLuint get_handle() = 0; 

	void set_source(std::string source) { m_source = source; }
	void set_defines(std::string defines) { m_defines = defines; }
	std::string source() { return m_source; }
protected:
	std::string m_source;
	std::string m_defines;
};

class VertexShader : public Shader
{
public:
	GLuint get_handle();	
};

class FragmentShader : public Shader
{
public:
	GLuint get_handle();
};

class GenericShader : public Shader
{
public:
	GLuint get_handle() { return 0; }
};

#endif
