#include "model.hpp"

#include "utils/file_system.hpp"

#define TINYGLTF_IMPLEMENTATION
#define TINYGLTF_NO_EXTERNAL_IMAGE
#define TINYGLTF_USE_RAPIDJSON 
#define TINYGLTF_NO_INCLUDE_JSON
#define TINYGLTF_NO_STB_IMAGE
#define TINYGLTF_NO_STB_IMAGE_WRITE
#define TINYGLTF_USE_RAPIDJSON_CRTALLOCATOR 
#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/rapidjson.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "tiny_gltf.h"

#include "SDL2/SDL_rwops.h"
#include "binary_file_def.hpp"
#include "debug/logger.hpp"
#include <cstring>
#include <algorithm>
#include <memory>
#include "game.hpp"
#include <algorithm>
#include <cmath>
#include <limits>
#include "material.hpp"
#include "renderer/render_vertex.hpp"
#include "default_resources.hpp"
#include "utils/asset_management_utils.hpp"

namespace assets
{
    static ModelMesh processMesh(const tinygltf::Model &gltf, const tinygltf::Mesh &mesh)
    {
        Eigen::Vector3f maxe = Eigen::Vector3f::Constant(std::numeric_limits<float>::min());
		Eigen::Vector3f mine = Eigen::Vector3f::Constant(std::numeric_limits<float>::max());
        ModelMesh model_mesh;
        for(const tinygltf::Primitive &prim : mesh.primitives)
        {
            tinygltf::Material mat = gltf.materials[prim.material];
            std::string material_name = mat.name.empty() ? "default_material" : mat.name;
            model_mesh.mat = assets::load_material(material_name);

            int vert_count;
            
            const unsigned char *posBuffer = NULL;
            const unsigned char *uvBuffer = NULL;
            const unsigned char *normalBuffer = NULL;

            int posElemSize = 0;
            int uvElemSize = 0;
            int normalElemSize = 0;

            int posStride = -1;
            int uvStride = -1;
            int normalStride = -1;

            auto pos_it = prim.attributes.find("POSITION");
            if(pos_it != prim.attributes.end())
            {
                const tinygltf::Accessor &pos_acc = gltf.accessors[pos_it->second];
                const tinygltf::BufferView &pos_bv = gltf.bufferViews[pos_acc.bufferView];

                posBuffer = &(gltf.buffers[pos_bv.buffer].data[pos_acc.byteOffset + pos_bv.byteOffset]);
                posStride = pos_acc.ByteStride(pos_bv);
                vert_count = pos_acc.count;
                posElemSize = tinygltf::GetComponentSizeInBytes(pos_acc.componentType) * tinygltf::GetNumComponentsInType(pos_acc.type);
            }

            auto uv_it = prim.attributes.find("TEXCOORD_0");
            if(uv_it != prim.attributes.end())
            {
                const tinygltf::Accessor &uv_acc = gltf.accessors[uv_it->second];
                const tinygltf::BufferView &uv_bv = gltf.bufferViews[uv_acc.bufferView];

                uvBuffer = &(gltf.buffers[uv_bv.buffer].data[uv_acc.byteOffset + uv_bv.byteOffset]);
                uvStride = uv_acc.ByteStride(uv_bv);
                uvElemSize = tinygltf::GetComponentSizeInBytes(uv_acc.componentType) * tinygltf::GetNumComponentsInType(uv_acc.type);
            }

            auto normal_it = prim.attributes.find("NORMAL");
            if(normal_it != prim.attributes.end())
            {
                const tinygltf::Accessor &norm_acc = gltf.accessors[normal_it->second];
                const tinygltf::BufferView &norm_bv = gltf.bufferViews[norm_acc.bufferView];

                normalBuffer = &(gltf.buffers[norm_bv.buffer].data[norm_acc.byteOffset + norm_bv.byteOffset]);
                normalStride = norm_acc.ByteStride(norm_bv);
                normalElemSize = tinygltf::GetComponentSizeInBytes(norm_acc.componentType) * tinygltf::GetNumComponentsInType(norm_acc.type);
            }

            //add animation stuff

            model_mesh.verts.resize(vert_count);
            for(int i = 0; i < vert_count; i++)
            {
                RenderVertex &vert = model_mesh.verts[i];
                
                if(posBuffer) memcpy(vert.pos.data(), posBuffer + i * posStride, posElemSize);
                if(uvBuffer) memcpy(vert.uv0.data(), uvBuffer + i * uvStride, uvElemSize);
                if(normalBuffer) memcpy(vert.normal.data(), normalBuffer + i *normalStride, normalElemSize);

                maxe = maxe.cwiseMax(vert.pos);
                mine = mine.cwiseMin(vert.pos);
            }

            if(prim.indices >= 0)
            {
                const tinygltf::Accessor &idx_acc = gltf.accessors[prim.indices];
                model_mesh.idxs.resize(idx_acc.count);

                const tinygltf::BufferView &idx_bv = gltf.bufferViews[idx_acc.bufferView];
                const void *idx_buffer = &(gltf.buffers[idx_bv.buffer].data[idx_bv.byteOffset + idx_acc.byteOffset]);

                switch(idx_acc.componentType)
                {
                    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
                    {
                        const unsigned int *buf = reinterpret_cast<const unsigned int*>(idx_buffer);
                        for(int i = 0; i < idx_acc.count; i++)
                        {
                            model_mesh.idxs[i] = buf[i];
                        }
                        break;
                    }
                    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
                    {
                        const unsigned short *buf = reinterpret_cast<const unsigned short*>(idx_buffer);
                        for(int i = 0; i < idx_acc.count; i++)
                        {
                            model_mesh.idxs[i] = buf[i];
                        }
                        break; 
                    }
                    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
                    {
                        const unsigned char *buf = reinterpret_cast<const unsigned char*>(idx_buffer);
                        for(int i = 0; i < idx_acc.count; i++)
                        {
                            model_mesh.idxs[i] = buf[i];
                        }
                        break; 
                    }
                    default:
                    {
                        assert(false);
                    }
                }
            }
            else
            {
                model_mesh.idxs.resize(model_mesh.verts.size());
                unsigned int *current_idx = model_mesh.idxs.data();
                for(int i = 2; i < model_mesh.idxs.size(); i++)
                {
                    current_idx[0] = 0;
                    current_idx[1] = i;
                    current_idx[2] = i-1;
                    current_idx += 3;
                }             
            }
        }

        model_mesh.bounds.extend(maxe);
        model_mesh.bounds.extend(mine);

        return model_mesh;
    }

    static void processNode(const tinygltf::Model &gltf, Model *model, const tinygltf::Node &node)
    {
        if(node.mesh != -1)
        {
            ModelMesh mesh = processMesh(gltf, gltf.meshes[node.mesh]);
            model->meshes().push_back(mesh);
            Eigen::AlignedBox3f &b = model->overall_bounds();
            b.extend(mesh.bounds.max());
            b.extend(mesh.bounds.min());
        }

        for(int child : node.children)
        {
            const tinygltf::Node &n = gltf.nodes[child];
            processNode(gltf, model, n);
        }
    }

    bool LoadImageData(tinygltf::Image *image, const int image_idx, std::string *err,
                    std::string *warn, int req_width, int req_height,
                    const unsigned char *bytes, int size, void *usr)
    {
        return true;
    }

    Model *load_model(std::string path, bool is_static)
    {
        auto &cache = internal::get_asset_cache<Model>();
        Model *model = internal::find_in_cache<Model>(cache, path);
        if(model)
        {
            return model;
        }

        std::vector<unsigned char> buffer;
        size_t read;
        if(internal::load_file(path, "rb", buffer, read))
        {
            model = load_model(buffer.data(), read);
			if(model)
            {
                cache.insert({path, model});
                return model;
            }
        }

        delete model;
        return NULL;
    }

	Model *load_model(unsigned char *data, size_t size, bool is_static)
	{
		tinygltf::Model gltf;
        tinygltf::TinyGLTF loader;
        std::string err, warn;
        loader.SetImageLoader(LoadImageData, NULL);

        bool ret = loader.LoadBinaryFromMemory(&gltf, &err, &warn, data, size);
        if (!warn.empty()) {
            LOG_WARNING("gltf: %s\n", warn.c_str());
            return NULL;
            
        }
        if (!err.empty()) {
            LOG_ERROR("gltf: %s\n", err.c_str());
            return NULL;
        }

		Model *model = new Model;

        tinygltf::Scene &scene = gltf.scenes[0];
        tinygltf::Node &root_node = gltf.nodes[scene.nodes[0]]; //only the first root node is read, makes sure only one object is exported
        //if there is a mesh in a node then any children of that node are ignored
        //look for a mesh
		processNode(gltf, model, root_node);

		if(is_static) model->make_static();
		else model->make_dynamic();

        return model;
	}
};

Model::Model() :
	m_static_model(false),
	m_overall_bounds()
{
}

Model::~Model()
{
}
