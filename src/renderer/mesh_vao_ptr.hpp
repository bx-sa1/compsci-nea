#ifndef MESH_VAO_PTR_H_
#define MESH_VAO_PTR_H_

#include "renderer/vertex_buffer.hpp"

struct MeshVAOPtr
{
	size_t offset = 0, size = 0;
	VertexArrayObject *vao_id = NULL;
};

#endif
