#include "scene.hpp"

#include "utils/sort.hpp"

Scene::Scene()
{
}

Scene::~Scene()
{
}

void Scene::remove_model(Model *model)
{
	models.erase(std::remove(models.begin(), models.end(), model), models.end());
}

void Scene::remove_light(LightInfo *l)
{
	lights.erase(std::remove(lights.begin(), lights.end(), l), lights.end());
}

static float calc_depth(const Camera &cam, const ModelMesh &mesh)
{
	return (cam.transform.translation() - mesh.bounds.center()).cwiseAbs().y();
}

/*
 * 1) static (smallest first)
 * 2) translucent (smallest first)
 * 3) depth (greatest first)
 * 4) index (smallest first)
 */
static std::uint64_t create_id(int index, Model *model, ModelMesh &mesh, const Camera &camera)
{
	bool is_static = model->is_static();
	auto t_find = mesh.mat->flags().find("translucent");
	bool translucent = t_find != mesh.mat->flags().end() ? t_find->second : 0;
	//make depth negative to make use of two's complement
	float depth = -calc_depth(camera, mesh);

	std::int64_t i = ((std::int64_t)index & 0xFFFF);
	std::int64_t d = ((std::int64_t)depth & 0xFFFFFFFF) << 16;
	std::int64_t t = ((std::int64_t)translucent & 0b1) << 32;
	std::int64_t s = ((std::int64_t)is_static & 0b1) << 1;

	return i | d | t | s;
}

PerFrameSceneData *Scene::generate_frame_data(Camera &camera)
{
	PerFrameSceneData *data = new PerFrameSceneData;


	for(Model *model : models)
	{
		Frustum frustum = camera.frustum(model->model_matrix);
		if(!frustum.aabb_test(model->overall_bounds())) 
				continue;

		int index = 0;
		for(ModelMesh &mesh : model->meshes())
		{
			DrawMesh dm = {
				false,
				&model->model_matrix,
				&mesh,
				&model->joints()
			};

			int64_t id = create_id(index, model, mesh, camera);
			auto dm_pair = std::make_pair(id, dm);

			data->meshes.push_back(dm_pair);

			index++;
		}
	}

	//sort draw meshes
	struct
	{
		bool operator()(const std::pair<int64_t, DrawMesh> &a, const std::pair<int64_t, DrawMesh> &b) const 
		{ 
			return a.first < b.first; 
		}
	} draw_call_sort;
	merge_sort(data->meshes.data(), 0, data->meshes.size()-1, draw_call_sort);

	for(LightInfo *light : lights)
	{
		Eigen::Affine3f trans;
		trans.translate(light->pd);
		Frustum frustum = camera.frustum(trans.matrix());
		if(!frustum.aabb_test(light->bounds))
			continue;

		std::vector<int> dm_indices;

		for(int dmi = 0; dmi < data->meshes.size(); dmi++)
		{
			const DrawMesh &dm = data->meshes[dmi].second;

			if(light->bounds.intersects(dm.mesh->bounds))
			{
				dm_indices.push_back(dmi);
			}
		}

		data->lights.push_back(std::make_pair(light, dm_indices));
	}

	return data;
}
