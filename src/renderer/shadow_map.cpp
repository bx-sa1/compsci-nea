#include "shadow_map.hpp"

#include "utils/convar.hpp"
#include "light_info.hpp"
#include "renderer/renderer.hpp"
#include "texture/texture.hpp"

#if 0

static Eigen::Vector3f view_frustum_corners[8] = { Eigen::Vector3f(-1,-1,-1),
	                                                Eigen::Vector3f(-1, 1,-1),
	                                                Eigen::Vector3f( 1, 1,-1),
	                                                Eigen::Vector3f( 1,-1,-1),
	                                                Eigen::Vector3f(-1,-1, 1),
	                                                Eigen::Vector3f(-1, 1, 1),
	                                                Eigen::Vector3f( 1, 1, 1),
	                                                Eigen::Vector3f( 1,-1, 1) };
static int ndc_cube_idx[24] = 
{
    0,1,
    1,2,
    2,3,
    3,0,

    4,5,
    5,6,
    6,7,
    7,4,

    0,4,
    1,5,
    2,6,
    3,7
};

static ConVar cv_shadowmap_atlas_size("shadowmap_atlas_size", 1024, "The total size of the shadow map atlas");
static ConVar cv_shadowmap_cascades("shadowmap_cascades", 3, "nuber of shadow map cascades for directional lights");

static int calcCascadeFarZ(const ShadowMapUpdateInfo &info, int cascade, int cascade_count)
{
	return info.current_view->m_near + ((info.current_view->m_far-info.current_view->m_near)/(float)cascade_count) * (cascade+1);
}

static Eigen::Matrix4f getCascadeProjection(const ShadowMapUpdateInfo &info, int cascade_far_z)
{
	View cascade_view(info.current_view->m_near, cascade_far_z, info.current_view->m_vfov, Viewport());
	return cascade_view.getProjectionMatrix();
}

void ShadowMap_createshadowMap(Framebuffer **framebuffer, Texture **texture)
{
    float size = cv_shadowmap_atlas_size.getNumber();

	ResourceParams params = RES_TEXTURE_FILTERING_NONE|RES_TEXTURE_ARRAY_2D|RES_TEXTURE_FORMAT_DEPTH|RES_TEXTURE_WRAP_CLAMP_WHITE|RES_UPLOAD_TO_OPENGL;
    *texture = new Texture;;
    (*texture)->init(size, size, params);
	(*texture)->params = params;

    *framebuffer = new Framebuffer(size, size);
    (*framebuffer)->bind();
    glReadBuffer(GL_NONE);
    glDrawBuffer(GL_NONE);
    //dont attach anything yet
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

View ShadowMap_createShadowMapView_DIRECTIONAL(const ShadowMapUpdateInfo &info, int *cascade_far_z)
{
    //tightly fit this view inside the light frustum
    //done in opengl cooridnate system so no unnecasry matrix multiplication  happens
    //calc cascade projection matrix
    Eigen::Matrix4f projection = Eigen::Matrix4f::Identity();
    *cascade_far_z = calcCascadeFarZ(info, info.cascade, info.cascade_count);
    getCascadeProjection(info, *cascade_far_z);

    //calc light bounds
    Eigen::Matrix4f clip_to_world = (projection * (Renderer::yup_to_zup * info.current_view->getTransform().matrix().inverse())).inverse(); //y up
    Eigen::Vector3f view_frustum_center = Eigen::Vector3f::Zero();
    float miny = nanf(""), maxy = nanf("");
    for(int i = 0; i < 8; i++)
    {

        Eigen::Vector4f world_space = (clip_to_world * view_frustum_corners[i].homogeneous());
        view_frustum_corners[i] = world_space.head(3) / world_space.w();

        view_frustum_center += view_frustum_corners[i];

        miny = fmin(miny, view_frustum_corners[i].y());
        maxy = fmax(maxy, view_frustum_corners[i].y());
    }
    
#if 0
    Eigen::Vector3f s = Eigen::Vector3f::Constant(5.0f);
    debug_draw_line(view.getTransform().translation()-s, view.getTransform().translation()+s, Eigen::Vector3f(1.0f, 0.0f,1.0f));
    debug_draw_from_points(info.renderer, view_frustum_corners, 8, ndc_cube_idx, 24, Eigen::Vector3f(1.0f, 0.0f, 1.0f));

    view_frustum_center /= 8.0;
    debug_draw_line(view_frustum_center-s, view_frustum_center+s, Eigen::Vector3f(1.0f, 1.0f, 0.0f));
#endif

    //and calc viewport for shadowmap atlas
    Viewport vp;
    vp.x = 0; vp.y = 0; vp.width = 1024; vp.height = 1024;
    View light_view;
    light_view.m_vp_params = vp;

    //calc the poisition for the light view matrix
    Eigen::Translation3f pos;
    Eigen::Vector3f light_dir = (info.current_light->pd.homogeneous()).head(3).normalized();
    pos.translation() = view_frustum_center + (light_dir * (maxy-miny));
    //debug_draw_line(pos.translation()-s, pos.translation()+s, Eigen::Vector3f(1.0f, 0.0f, 1.0f));

    //calc rotation for light view matrixx
    Eigen::Quaternionf rot = Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f::UnitY(), light_dir);

    //construct view matrix and transform matrix
    Eigen::Affine3f light_view_transform(pos*rot);
    Eigen::Matrix4f light_view_mat = (Renderer::yup_to_zup * light_view_transform.matrix().inverse());

    //calc ortho projection matrix from world space light bounds
    Eigen::AlignedBox3f light_ortho_box; //light space
    for(int i = 0; i < 8; i++)
    {
        Eigen::Vector3f p = (light_view_mat * view_frustum_corners[i].homogeneous()).head(3);
        if(i == 0) light_ortho_box = Eigen::AlignedBox3f(p);
        else light_ortho_box.extend(p);
    }
    //debug_draw_bounds(light_view_mat.inverse(), light_ortho_box, Eigen::Vector3f(1.0f, 0.0f, 0.0f));
    light_view.ortho = true;
    light_view.m_left = light_ortho_box.min().x(); light_view.m_right = light_ortho_box.max().x();
    light_view.m_top = light_ortho_box.max().y(); light_view.m_bottom = light_ortho_box.min().y();
    light_view.m_near = -light_ortho_box.max().z(); light_view.m_far = -light_ortho_box.min().z();

    //applyt to view
    light_view.getTransform() = light_view_transform;

    return light_view;
}
#endif
