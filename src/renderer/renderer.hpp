#ifndef RENDERER_H_
#define RENDERER_H_

#include <utility>
#include <vector>
#include <list>
#include "renderer/camera.hpp"
#include "utils/concommand.hpp"
#include "debug/debug_draw_implementation.hpp"
#include "renderer/render_triangle.hpp"
#include "renderer/render_vertex.hpp"
#include "renderer/vertex_buffer.hpp"
#include <map>
#include "utils/convar.hpp"
#include "renderer/texture/texture.hpp"
#include "renderer/texture/texture2d.hpp"
#include "view.hpp"
#include "framebuffer.hpp"
#include "light_info.hpp"
#include "renderer/model.hpp"
#include "mesh_vao_ptr.hpp"
#include "scene.hpp"
	
/**
 * The renderer class.
 * Inputs: The view, models, lights.
 * Output: Everything strictly inside the view drawn to the screen.
 * 
 * To start rendering, begin() should be called, this resets render state back to default,
 * then beginViewDef() can be called which allocates a new view definition. After this, addModel(), 
 * addLight() and setAmbientColor() can be called to either add a model to the view definition, add a 
 * light to the view definition or set the view definitions ambient color, respectfully. This must then 
 * all be ended by a call to endViewDef which closes the view def from modification and adds it to a list.
 * Another view definition can be started if you want to render objects from another view. Finally end() must 
 * be called to actually render everything. This calls flush() internally, passing the colorbuffer as an argument.
 **/
class Renderer
{
public:
    Renderer();
    ~Renderer();

	//clears and resets renderer state
	void beginRender();
	//render a view
	void renderView(const View &view);
	//render to colorbuffer
	void endRender();
	//render to a texture
	//if NULL, doesnt render but only deletes views
	void flush(Framebuffer* framebuffer); 

	//The world is defined with a z-up convention. This matrix transforms
	//a vector from z-up to OpenGL's y-up convention
    static const Eigen::Matrix4f yup_to_zup;
private:
	void depth_prepass(View &view, PerFrameSceneData *sd);
	void ambient_pass(View &view, PerFrameSceneData *sd);
	void render_lit(View &view, PerFrameSceneData *sd);
	void render_non_lit(View &view, PerFrameSceneData *sd);

	/**
	 * Bind a materials shader program.
	 * If set_params is true.
	 * It also bind textures and sets uniforms
	 **/
	int use_material(Material *mat, bool set_params);
	//Bind a texture to a specific unit
	void bind_texture(GLenum texture_type, GLuint texture, int unit);
	void set_uniform(GLuint prog, std::string name, std::array<float, 4> data);

	//
	VertexArrayObject *vao, *static_vao, *colorbuffer_vao;

	//The colour buffer.
	//Instead of using the default colour buffer
	//assigned when a window is created,
	//a custom one is made so that I can do 
	//post processing effects.
	Framebuffer *m_colourbuffer;
	//The texture taht the above framebuffer is attached to
	Texture2D *m_colourbuffer_tex;

	//Has begin() been called?
	bool m_begun;

	std::vector<View> m_views;
	PerFrameSceneData *m_current_scene_data;
};

#endif
