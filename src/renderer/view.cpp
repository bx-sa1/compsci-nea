#include "renderer/view.hpp"

#include "utils/convar.hpp"
#include <cmath>
#include "glad/glad.h"

/**
 * useful maths
 * http://ogldev.atspace.co.uk/www/tutorial12/tutorial12.html
 **/

Viewport::Viewport() :
	x(0), scissor_x(0),
	y(0), scissor_y(0),
	width(0), scissor_width(0),
	height(0), scissor_height(0)
{
	width = scissor_width = utils::get_convar("res_width")->getNumber();
	height = scissor_height = utils::get_convar("res_height")->getNumber();
}

Viewport::Viewport(float x, float y, float w, float h) : 
	x(x), scissor_x(x),
	y(y), scissor_y(y),
	width(w), scissor_width(w),
	height(h), scissor_height(h)
{}

void Viewport::apply()
{
	glViewport(x, y, width, height);
	glScissor(scissor_x, scissor_y, scissor_width, scissor_height);
}
