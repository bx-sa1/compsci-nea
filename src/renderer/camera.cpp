#include "camera.hpp"

#include "Eigen/src/Core/Matrix.h"
#include "utils/defines.hpp"
#include "renderer.hpp"

Camera::Camera() :
    projection_matrix(Eigen::Matrix4f::Identity()),
    near(0),
    far(0),
    transform(Eigen::Affine3f::Identity())
{
}

Camera::Camera(float n, float f, float vfov, float aspect) :
    projection_matrix(Eigen::Matrix4f::Identity()),
    near(n),
    far(f),
    transform(Eigen::Affine3f::Identity())
{
    float fov = degtorad(vfov);
    float yscale = 1.0f/(tan(fov * 0.5f));
    float xscale = (yscale/aspect);
    float zscale = -(far + near) / (far - near);
    float zpos = -(2.0f * far * near)/(far - near);

    projection_matrix(0, 0) = xscale;
    projection_matrix(1, 1) = yscale;
    projection_matrix(2, 2) = zscale;
    projection_matrix(3, 2) = -1.0f;
    projection_matrix(2, 3) = zpos;
    projection_matrix(3, 3) = 0.0;
}

Camera::Camera(float near, float far, float left, float right, float top, float bottom) :
    projection_matrix(Eigen::Matrix4f::Identity()),
    near(near),
    far(far),
    transform(Eigen::Affine3f::Identity())
{
    projection_matrix(0, 0) = 2.0f/(right-left);
    projection_matrix(1, 1) = 2.0f/(top-bottom);
    projection_matrix(2, 2) = -2.0f/(far-near);
    projection_matrix(0, 3) = -(right+left)/(right-left);
    projection_matrix(1, 3) = -(top+bottom)/(top-bottom);
    projection_matrix(2, 3) = -(far+near)/(far-near);
}

Camera::~Camera()
{
}

Frustum Camera::frustum(const Eigen::Matrix4f &model_matrix)
{
    Frustum f;
    Eigen::Matrix4f view = transform.matrix().inverse();
	view = Renderer::yup_to_zup * view;

    Eigen::Matrix4f combo = projection_matrix * view * model_matrix;

    //left clipping plane
    f.planes[0].coeffs().x() = combo(3,0) + combo(0,0);
    f.planes[0].coeffs().y() = combo(3,1) + combo(0,1);
    f.planes[0].coeffs().z() = combo(3,2) + combo(0,2);
    f.planes[0].coeffs().w() = combo(3,3) + combo(0,3);

    //right clipping plane
    f.planes[1].coeffs().x() = combo(3,0) - combo(0,0);
    f.planes[1].coeffs().y() = combo(3,1) - combo(0,1);
    f.planes[1].coeffs().z() = combo(3,2) - combo(0,2);
    f.planes[1].coeffs().w() = combo(3,3) - combo(0,3);

    //top clipping plane
    f.planes[2].coeffs().x() = combo(3,0) - combo(1,0);
    f.planes[2].coeffs().y() = combo(3,1) - combo(1,1);
    f.planes[2].coeffs().z() = combo(3,2) - combo(1,2);
    f.planes[2].coeffs().w() = combo(3,3) - combo(1,3);

    //bottom clipping plane
    f.planes[3].coeffs().x() = combo(3,0) + combo(1,0);
    f.planes[3].coeffs().y() = combo(3,1) + combo(1,1);
    f.planes[3].coeffs().z() = combo(3,2) + combo(1,2);
    f.planes[3].coeffs().w() = combo(3,3) + combo(1,3);

    //near clipping plane
    f.planes[4].coeffs().x() = combo(3,0) + combo(2,0);
    f.planes[4].coeffs().y() = combo(3,1) + combo(2,1);
    f.planes[4].coeffs().z() = combo(3,2) + combo(2,2);
    f.planes[4].coeffs().w() = combo(3,3) + combo(2,3);

    //far clipping plane
    f.planes[5].coeffs().x() = combo(3,0) - combo(2,0);
    f.planes[5].coeffs().y() = combo(3,1) - combo(2,1);
    f.planes[5].coeffs().z() = combo(3,2) - combo(2,2);
    f.planes[5].coeffs().z() = combo(3,3) - combo(2,3);

    return f;
}
