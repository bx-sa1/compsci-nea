#ifndef CHARACTER_CONTROLLER
#define CHARACTER_CONTROLLER

#include "BulletCollision/CollisionDispatch/btCollisionWorld.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "BulletDynamics/Dynamics/btDynamicsWorld.h"
#include "LinearMath/btTransform.h"
#include "LinearMath/btVector3.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

class CharacterController
{
public:
	CharacterController(btCollisionShape *shape);
	~CharacterController();

	bool on_floor() { return m_on_floor; }
	bool on_wall() { return m_on_wall; }
	btPairCachingGhostObject *get_body() { return m_body; }
	btVector3 move(btCollisionWorld *world, btVector3 velocity, btVector3 up, float max_floor_angle,  bool noclip); //returns velocity for debugging
private:
	bool recover_penetration(btCollisionWorld *world, btVector3 &recovery);

	btPairCachingGhostObject *m_body;
	bool m_on_floor, m_on_wall;

	btVector3 sweep_movement;
	btVector3 recovery; 
};

#endif
