#ifndef MOTION_STATE_H_
#define MOTION_STATE_H_

#include "btBulletDynamicsCommon.h"
#include "game.hpp"
#include "physics/physics.hpp"
#include "world/entity.hpp"

struct EntityMotionState : public btMotionState
{
    EntityMotionState(Entity *ent) : m_ent(ent) 
    {
        rot.setFromOpenGLSubMatrix(g_game->getRenderer()->yup_to_zup.data());
    }

    ///synchronizes world transform from user to physics
    //dont call getWorldTransform from user, just get it from motion state
    virtual void getWorldTransform(btTransform &trans) const
    {
        trans = Physics_EToBT_TRANSFORM(m_ent->getTransform());
    }

    ///synchronizes world transform from physics to user
    ///Bullet only calls the update of worldtransform for active objects
    virtual void setWorldTransform(const btTransform &trans)
    {
        m_ent->getTransform() = Physics_BTToE_TRANSFORM(trans);
    }

    Entity *m_ent;
    btMatrix3x3 rot;
};

#endif
