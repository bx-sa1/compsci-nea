#include "BulletCollision/BroadphaseCollision/btBroadphaseProxy.h"
#include "BulletCollision/BroadphaseCollision/btCollisionAlgorithm.h"
#include "BulletCollision/BroadphaseCollision/btOverlappingPairCache.h"
#include "BulletCollision/CollisionDispatch/btCollisionObject.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "BulletCollision/NarrowPhaseCollision/btManifoldPoint.h"
#include "BulletCollision/NarrowPhaseCollision/btPersistentManifold.h"
#include "character_controller.hpp"

#include "debug/logger.hpp"
#include "game.hpp"
#include "physics/physics.hpp"
#include <cmath>

#define MAX_PENETRATION_LOOPS 4
#define REC_MOVEMENT_SCALE 0.4
#define MAX_FLOOR_ANGLE_EPSILON 0.1

class ClosestConvexResultCallbackIgnoreSelf : public btCollisionWorld::ClosestConvexResultCallback
{
public:
    ClosestConvexResultCallbackIgnoreSelf(btCollisionObject *self) : 
        btCollisionWorld::ClosestConvexResultCallback(btVector3(0,0,0), btVector3(0,0,0)),
        m_self(self)
    {

    }

    virtual btScalar addSingleResult(btCollisionWorld::LocalConvexResult& convexResult, bool normalInWorldSpace)
    {
        if(convexResult.m_hitCollisionObject == m_self)
        {
            return 1;
        }

		if(!convexResult.m_hitCollisionObject->hasContactResponse())
		{
			return 1;
		}

        return btCollisionWorld::ClosestConvexResultCallback::addSingleResult(convexResult, normalInWorldSpace);
    }

    btCollisionObject *m_self;
};

CharacterController::CharacterController(btCollisionShape *shape) :
	m_body(NULL),
	m_on_floor(false),
	m_on_wall(false)
{
	assert(shape->isConvex());
	m_body = new btPairCachingGhostObject;
	m_body->setCollisionShape(shape);
	btTransform t;
	t.setIdentity();
	m_body->setWorldTransform(t);
	m_body->setCollisionFlags(m_body->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);

}

CharacterController::~CharacterController()
{
	delete m_body;
}

btVector3 CharacterController::move(btCollisionWorld *world, btVector3 velocity, btVector3 up, float max_floor_angle,  bool noclip)
{
	m_on_floor = false;
	m_on_wall = false;

	if(noclip)
	{
		m_body->getWorldTransform().getOrigin() += velocity;
		return velocity;
	}

	//step 1: resolve any penetrations from previous frame
	recovery.setZero();
	{
		int i;
		for(i = 0; i < MAX_PENETRATION_LOOPS; i++) //done multiple times incase you are penetrating multiple surfaces
		{
			if(!recover_penetration(world, recovery))
			{
				break;
			}
		}
		m_body->getWorldTransform().getOrigin() += recovery;
		if(i >= MAX_PENETRATION_LOOPS) LOG_WARNING("Couldnt recover from penetration before sweep");
	}


	sweep_movement = velocity;
	{
		//step 2: convex sweep test to test if body can move to new positiona and slide
		ClosestConvexResultCallbackIgnoreSelf result(m_body);
		for(int i = 0; i < 4; i++)
		{
			btTransform start = m_body->getWorldTransform();
			btTransform end = btTransform(start.getBasis(), start.getOrigin() + sweep_movement);
			
			if(!(end.getOrigin()-start.getOrigin()).fuzzyZero()) m_body->convexSweepTest((btConvexShape*)m_body->getCollisionShape(), start, end, result, world->getDispatchInfo().m_allowedCcdPenetration);

			if(result.hasHit())
			{
				sweep_movement *= result.m_closestHitFraction;
				sweep_movement = sweep_movement - result.m_hitNormalWorld * sweep_movement.dot(result.m_hitNormalWorld);
				if(acos(result.m_hitNormalWorld.dot(up)) <= max_floor_angle + MAX_FLOOR_ANGLE_EPSILON)
				{
					m_on_floor = true;
				}
				else
				{
					m_on_wall = true;
				}
			}
		}
		m_body->getWorldTransform().getOrigin() += sweep_movement;
	}

	return sweep_movement;

	//step 3: re-resolve any penetrations from previous frame
#if 0
	{
		int i;
		for(i = 0; i < MAX_PENETRATION_LOOPS; i++) //done multiple times incase you are penetrating multiple surfaces
		{
			if(!recover_penetration(world))
			{
				break;
			}
		}
		if(i >= MAX_PENETRATION_LOOPS) LOG_WARNING("Couldnt recover from penetration after sweep");
	}
#endif
}

bool CharacterController::recover_penetration(btCollisionWorld *world, btVector3 &recovery)
{
	btVector3 minAabb, maxAabb;
	m_body->getCollisionShape()->getAabb(m_body->getWorldTransform(), minAabb, maxAabb);
	world->getBroadphase()->setAabb(m_body->getBroadphaseHandle(),
											 minAabb,
											 maxAabb,
											 world->getDispatcher());
	
	bool penetration = false;

	world->getDispatcher()->dispatchAllCollisionPairs(m_body->getOverlappingPairCache(), world->getDispatchInfo(), world->getDispatcher());

	btManifoldArray manifoldArray;
	btBroadphasePairArray &pairArray = m_body->getOverlappingPairCache()->getOverlappingPairArray();
	int numPairs = pairArray.size();

	for(int i = 0; i < numPairs; i++)
	{
		manifoldArray.clear();

		const btBroadphasePair &collisionPair = pairArray[i];

		btCollisionObject* obj0 = static_cast<btCollisionObject*>(collisionPair.m_pProxy0->m_clientObject);
		btCollisionObject* obj1 = static_cast<btCollisionObject*>(collisionPair.m_pProxy1->m_clientObject);

		if ((obj0 && !obj0->hasContactResponse()) || (obj1 && !obj1->hasContactResponse()))
			continue;

		if(collisionPair.m_algorithm) collisionPair.m_algorithm->getAllContactManifolds(manifoldArray);

		for(int j = 0; j < manifoldArray.size(); j++)
		{
			btPersistentManifold *manifold = manifoldArray[j];

			btScalar dirSign = manifold->getBody0() == m_body ? btScalar(-1.0) : btScalar(1.0);
			for(int k = 0; k < manifold->getNumContacts(); k++)
			{
				const btManifoldPoint &pt = manifold->getContactPoint(k);
				if(pt.getDistance()<-REC_MOVEMENT_SCALE)
				{
					recovery += pt.m_normalWorldOnB * pt.getDistance() * dirSign * REC_MOVEMENT_SCALE;
					penetration = true;
					//LOG_INFO("COLLISION");
				}
			}
		}
	}

	return penetration;
}

