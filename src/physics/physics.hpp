#ifndef PHYSICS_H_
#define PHYSICS_H_

#include "Eigen/Eigen"
#include <memory>
#include <vector>
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

class Model;

class BulletDebugger : public btIDebugDraw
{
	int m_debugmode;

public:
	void drawLine(const btVector3& from, const btVector3& to, const btVector3& color);
	void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color);
	void reportErrorWarning(const char* warningString);
	void draw3dText(const btVector3& location, const char* textString);

	void setDebugMode(int debugMode) { m_debugmode = debugMode; }
	int getDebugMode() const { return m_debugmode; }
};

class Physics
{
public:
	Physics(float gravity);
	~Physics();

	btDiscreteDynamicsWorld *getPhysicsWorld() const { return m_phys_world; }
	btCollisionDispatcher *getPhysicsDispatcher() const { return m_dispatcher; }
	btBroadphaseInterface *getPhysicsBroadphase() const { return m_broadphase; } 
	btSequentialImpulseConstraintSolver *getPhysicsSolver() const { return m_solver; }

	void addToWorld(btCollisionObject *obj, int group = btBroadphaseProxy::DefaultFilter, int mask = btBroadphaseProxy::DefaultFilter) const;
	void removeFromWorld(btCollisionObject *obj) const;
	void clearWorld() const;

private:
	btDiscreteDynamicsWorld *m_phys_world;
	btDefaultCollisionConfiguration* m_collisionConfiguration;
	btCollisionDispatcher* m_dispatcher;
	btBroadphaseInterface* m_broadphase;
	btSequentialImpulseConstraintSolver* m_solver;
	BulletDebugger debug;
};

btCollisionShape *Physics_ModelToStaticCollisionShape(Model *model);
btCollisionShape *Physics_ModelToDynamicCollisionShape(Model *model);

btCollisionObject *Physics_CreateStaticObject(btCollisionShape *shape, btMotionState *motion);
btCollisionObject *Physics_CreateKinematicObject(btCollisionShape *shape, btMotionState *motion);
btCollisionObject *Physics_CreateDynamicObject(float mass, btCollisionShape *shape, btMotionState *motion);
btCollisionObject *Physics_CreateGhostObject(btCollisionShape *shape, btTransform transform);

Eigen::Vector3f Physics_BTToE_VEC3(const btVector3 &b);
Eigen::Matrix3f Physics_BTToE_MAT3(const btMatrix3x3 &b);
Eigen::Affine3f Physics_BTToE_TRANSFORM(const btTransform &b);
btVector3 Physics_EToBT_VEC3(const Eigen::Vector3f &e);
btMatrix3x3 Physics_EToBT_MAT3(const Eigen::Matrix3f &e);
btTransform Physics_EToBT_TRANSFORM(const Eigen::Affine3f &e);

#endif
