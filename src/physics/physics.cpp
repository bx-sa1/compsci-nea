#include "physics.hpp"

#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "debug/logger.hpp"
#include "game.hpp"
#include "renderer/model.hpp"
#include <algorithm>
#include "renderer/render_vertex.hpp"
#include "world/entity.hpp"
#include "debug_draw.hpp"

void BulletDebugger::drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color)
{
	const ddVec3 bfrom = { from.x(), from.y(), from.z() };
	const ddVec3 bto = { to.x(), to.y(), to.z() };
	const ddVec3 bcolor = { color.x(), color.y(), color.z() };
	dd::line(bfrom, bto, bcolor);
}

void BulletDebugger::drawContactPoint(const btVector3 &PointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime, const btVector3 &color)
{
	btVector3 to = PointOnB+normalOnB*distance;

	drawLine(PointOnB, to, color);
}

void BulletDebugger::reportErrorWarning(const char *warningString)
{
	LOG_WARNING("%s", warningString);
}

void BulletDebugger::draw3dText(const btVector3 &location, const char *textString)
{

}

Physics::Physics(float gravity)
{
	m_collisionConfiguration = new btDefaultCollisionConfiguration;
	m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);
	m_broadphase = new btDbvtBroadphase;
	m_solver = new btSequentialImpulseConstraintSolver;
	m_phys_world = new btDiscreteDynamicsWorld(m_dispatcher, m_broadphase, m_solver, m_collisionConfiguration);
	m_phys_world->setGravity(btVector3(0, 0, -gravity));

	m_broadphase->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());	    

	debug.setDebugMode(btIDebugDraw::DBG_DrawWireframe | btIDebugDraw::DBG_DrawContactPoints | btIDebugDraw::DBG_DrawNormals);
	m_phys_world->setDebugDrawer(&debug);
}

Physics::~Physics()
{
	clearWorld();

	delete m_phys_world;
	delete m_solver;
	delete m_broadphase;
	delete m_dispatcher;
	delete m_collisionConfiguration;
}

void Physics::addToWorld(btCollisionObject *obj, int group, int mask) const
{
	assert(obj);

	if(obj->getInternalType() == btCollisionObject::CO_RIGID_BODY)
	{
		m_phys_world->addRigidBody(btRigidBody::upcast(obj), group, mask);
	}
	else 
	{
		m_phys_world->addCollisionObject(obj, group, mask);
	}
}

void Physics::removeFromWorld(btCollisionObject *obj) const
{
	if(!obj) return;

	if(obj->getInternalType() == btCollisionObject::CO_RIGID_BODY)
	{
		m_phys_world->removeRigidBody(btRigidBody::upcast(obj));
	}
	else
	{
		m_phys_world->removeCollisionObject(obj);
	}
}

void Physics::clearWorld() const
{
	for(int i = 0; i < m_phys_world->getNumCollisionObjects(); i++)
	{
		btCollisionObject *obj = m_phys_world->getCollisionObjectArray()[i];
		removeFromWorld(obj);
	}
}

btCollisionShape *Physics_ModelToStaticCollisionShape(Model *model)
{
	assert(model);

	int tri_count = 0;
	btTriangleIndexVertexArray *btmodel = new btTriangleIndexVertexArray();
	for(int mesh = 0; mesh < model->meshes().size(); mesh++)
	{
	    ModelMesh &model_mesh = model->meshes()[mesh]; //only allow 1 mesh for collision model
    	btIndexedMesh btmesh;

    	btmesh.m_numTriangles = model_mesh.idxs.size() / 3;
    	btmesh.m_numVertices = model_mesh.verts.size();
    	btmesh.m_triangleIndexBase = (unsigned char *)model_mesh.idxs.data();
    	btmesh.m_triangleIndexStride = sizeof(unsigned int) * 3;
    	btmesh.m_vertexBase = (unsigned char *)model_mesh.verts.data();
    	btmesh.m_vertexStride = sizeof(RenderVertex);
    	btmesh.m_indexType = PHY_INTEGER;
    	btmesh.m_vertexType = PHY_FLOAT;

    	tri_count += btmesh.m_numTriangles;

    	btmodel->addIndexedMesh(btmesh);
	}

	return new btBvhTriangleMeshShape(btmodel, tri_count <= 1000000); //problems with quantization if tri count is large
}

btCollisionShape *Physics_ModelToDynamicCollisionShape(Model *model)
{
	assert(model);

	btConvexHullShape *chull = new btConvexHullShape();
	for(int mesh = 0; mesh < model->meshes().size(); mesh++)
	{
		ModelMesh model_mesh = model->meshes()[mesh];
		for(int v = 0; v < model_mesh.verts.size(); v++)
    	{

			btVector3 v1(model_mesh.verts[v].pos.x(), model_mesh.verts[v].pos.y(), model_mesh.verts[v].pos.z());
			chull->addPoint(v1);
    	}
	}
	chull->optimizeConvexHull();
	return chull;
}

btCollisionObject *Physics_CreateStaticObject(btCollisionShape *shape, btMotionState *motion)
{
	btRigidBody::btRigidBodyConstructionInfo info(0.0f, motion, shape, btVector3(0,0,0));
	btCollisionObject *co = new btRigidBody(info);
	return co;
}
	

btCollisionObject *Physics_CreateKinematicObject(btCollisionShape *shape, btMotionState *motion)
{
	btRigidBody::btRigidBodyConstructionInfo info(0.0f, motion, shape, btVector3(0,0,0));
	btCollisionObject *co = new btRigidBody(info);
	co->setCollisionFlags(co->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
	return co;
}

btCollisionObject *Physics_CreateDynamicObject(float mass, btCollisionShape *shape, btMotionState *motion)
{
	btVector3 inertia;
	shape->calculateLocalInertia(mass, inertia);

	btRigidBody::btRigidBodyConstructionInfo info(mass, motion, shape, inertia);
	btCollisionObject *co = new btRigidBody(info);
	return co;
}

btCollisionObject *Physics_CreateGhostObject(btCollisionShape *shape, btTransform transform)
{
	btCollisionObject *co = new btGhostObject;
	co->setCollisionShape(shape);
	co->setWorldTransform(transform);
	return co;
}

Eigen::Vector3f Physics_BTToE_VEC3(const btVector3 &b)
{
	return Eigen::Vector3f(b.x(), b.y(), b.z());
}

Eigen::Matrix3f Physics_BTToE_MAT3(const btMatrix3x3 &b)
{
	Eigen::Matrix3f m;
	b.getOpenGLSubMatrix(m.data());
	return m;
}

Eigen::Affine3f Physics_BTToE_TRANSFORM(const btTransform &b)
{
	Eigen::Affine3f t;
	b.getOpenGLMatrix(t.data());
	return t;
}

btVector3 Physics_EToBT_VEC3(const Eigen::Vector3f &e)
{
	return btVector3(e.x(), e.y(), e.z());
}

btMatrix3x3 Physics_EToBT_MAT3(const Eigen::Matrix3f &e)
{
	btMatrix3x3 m;
	m.setFromOpenGLSubMatrix(e.data());
	return m;
}

btTransform Physics_EToBT_TRANSFORM(const Eigen::Affine3f &e)
{
	btTransform t;
	t.setFromOpenGLMatrix(e.data());
	return t;
}
