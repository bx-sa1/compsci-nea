#include "SDL2/SDL_rwops.h"
#include "utils/convar.hpp"
#include "game.hpp"
#include "utils/thread.hpp"
#include <queue>
#include "utils/concommand.hpp"
#include "debug/logger.hpp"
#include "utils/parser.hpp"
#include <algorithm>
#include <string>
#include <cstring>
#include <cstdlib>
#include <vector>

#if 0
void cmd_set(int argc, std::string argv[])
{
    if(argc != 2) return;
    
    ConVar *cv = ConVarManager::getConVar(argv[0]);
    if(cv)
    {
    	if(cv->isNumber())
    	{
    	    char *end;
    	    cv->setNumber(strtod(argv[1].c_str(), &end));
    	}
    	else if(cv->isString())
    	{
    	    cv->setString(argv[1]);
    	}
    }
}

void cmd_exec(int argc, std::string argv[])
{
    if(argc < 1) return;
    
    SLD_RWops *file = RWFileFromBasedir(argv[0].c_str(), "r");
    if(!file)
    {
    	LOG_ERROR("Failed to open file %s", argv[0].c_str());
    	return;
    }
    
    size_t size = SDL_RWsize(file);
    std::vector<char> text;
    text.reserve(size);
    size_t read = SDL_RWread(file, text.data(), size, 1);
    if(read != size)
    {
    	LOG_ERROR("Failed to read config");
    	return;
    }

    char *start = text.data();
    char *end = start;

    while(*end != '\0')
    {
        //skip_whitespace
        while(*(end) <= ' ' || *end == 127)
        {
            if(*end == '\0') goto exit_loop;
            end++;
        }

        //get everything until next newline
        start = end;
        int count = 0;
        while(*end != '\n' && *end != '\r' && *end != '\0')
        {
            count++;
            end++;
        }

        std::string command = std::string(start, count);

        TokenisedCommand *cmd = ConsoleCommand_TokenizeCommand(command);
        if(!cmd) break; //couldn't get a cmd, must be end of file

        ConsoleCommand *cc = ConsoleCommandManager::getConsoleCommand(cmd->command);
        if(cc)
        {
            cc->getCommand()(cmd->argc, cmd->argv);
        }

        delete cmd;
    }
    exit_loop:
    (void)0;
}

ConsoleCommand exec("exec", cmd_exec, "Execute a config file");
ConsoleCommand set("set", cmd_set, "Set Convar");
#endif

int main(int argc, char *argv[])
{    
    LOG_INFO("Starting Game...");
    g_game = new Game;
    g_game->init();

	if(g_game->m_initialized)
	{
		LOG_INFO("Running Game...");
		g_game->loop();
	}

    delete g_game;
    return 0;
}
