#ifndef GAME_H_
#define GAME_H_

#include "SDL2/SDL.h"
#include <string>
#include "ui/nuklear_with_defs.h"
#include <thread>

enum GameState
{
	LOADING_LEVEL,
	PLAYING,
	PAUSED,
	MAIN_MENU
};

typedef void (*UIFunc)(nk_context *ctx);

class World;
class Renderer;
class FileSystem;
class Mouse;
class Keyboard;
class UI;
class Input;
class ResourceManager;
class Game
{
public:
    Game();
    ~Game();

    void init();
    void loop();
    void shutdown();

	void set_relative_mouse_mode(bool b); //does nothing if nk isnt drawing anything
	
	//load map from file
	//show loading screen at the same time
	//show loaded map at the end
	void change_map(std::string map);

	void toggle_pause();
	void pause();
	void toggle_console();
    int list_maps(const char ***maps);

	void set_state(GameState state);
	GameState get_state();

    SDL_Window *getWindow() { return m_window; }
	SDL_GLContext getGLContext() { return m_gl_context; }

    World *getWorld() { return m_world; }
    Renderer *getRenderer() { return m_renderer; }
	Input *getInput() { return m_input; }
	ResourceManager *getResourceManager() { return m_res_manager; }
	nk_context *getNuklear() { return m_nuklear; }

	float m_frametime;
	bool m_initialized;
	bool m_paused;
	bool m_show_console;

private:
    SDL_Window *m_window;
	SDL_GLContext m_gl_context;

    World *m_world;
    Renderer *m_renderer;
	Input *m_input;

	ResourceManager *m_res_manager;

	nk_context *m_nuklear;
	UIFunc m_current_ui_func;

	GameState m_state;

	std::thread m_loading_level_thread;
	void loading_level_func(std::string map_path);
	std::string m_loading_status;
};

extern Game *g_game;

#endif
