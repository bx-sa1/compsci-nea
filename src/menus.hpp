#ifndef MENUS_H_
#define MENUS_H_

#include "ui/nuklear_with_defs.h"
#include <string>

namespace Menus
{
	void draw_main_menu(nk_context *ctx);
	void draw_console(nk_context *ctx);
	void draw_pause_menu(nk_context *ctx);
	void draw_loading_screen(nk_context *ctx, std::string loading_status);
};

#endif
