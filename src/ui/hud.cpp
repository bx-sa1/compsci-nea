#include "hud.hpp"
#include "convar.hpp"

HUD::HUD() :
	CONSTRUCT_HUD_ITEM(test, true)
{}

HUD::~HUD()
{}

void HUD::render(nk_context *ctx)
{
	int width = ConVarManager::getConVar("res_width")->getNumber();
	int height = ConVarManager::getConVar("res_height")->getNumber();
	if(nk_begin(ctx, "_hud", nk_rect(0, 0, width, height), NK_WINDOW_NOT_INTERACTIVE))
	{
		nk_layout_row_dynamic(ctx, height, 1);
		nk_layout_space_begin(ctx, NK_STATIC, height, int widget_count);
		nk_layout
		for(HudItem hi : m_hud_items)
		{
			hi.m_draw_func(ctx, hi.m_value);
		}
	}
	nk_end(ctx);
