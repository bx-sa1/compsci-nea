#include "common_menu.hpp"

#include "concommand.hpp"
#include "game.hpp"
#include "renderer/image.hpp"
#include "world/world.hpp"
#include "ui/ui.hpp"
#include "convar.hpp"
#include "resource/resource_locator.hpp"

void draw_play_btn(nk_context *ctx)
{
	if(nk_button_label(ctx, "Play"))
	{
		g_game->getUI()->add_window(draw_level_select);
	}
}

void draw_options_btn(nk_context *ctx)
{
	if(nk_button_label(ctx, "Options"))
	{
		g_game->getUI()->add_window(draw_options);
	}
}

void draw_exit_btn(nk_context *ctx)
{
	if(nk_button_label(ctx, "Exit"))
	{
		g_game->getWorld()->clear();
	}
}

void draw_quit_btn(nk_context *ctx)
{
	if(nk_button_label(ctx, "Quit"))
	{
		g_game->shutdown();
	}
}

void draw_resume_btn(nk_context *ctx)
{
	if(nk_button_label(ctx, "Resume"))
	{
		g_game->getUI()->clear_windows();
	}
}

void draw_logo(nk_context *ctx)
{
}

void draw_level_select(nk_context *ctx, UIWindowFuncUserData *usr_data)
{
    static const char **maps = NULL;
    static int num_maps = g_game->list_maps(&maps);
    static int chosen_map = 0;
    
    int width = ConVarManager::getConVar("res_width")->getNumber();
	int height = ConVarManager::getConVar("res_height")->getNumber();
    
	if(nk_begin(ctx, "Level Select", nk_rect(width/2.0f, height/2.0f, 200, 200), NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_CLOSABLE|NK_WINDOW_NO_SCROLLBAR))
    {
            nk_layout_row_static(ctx, 25, 100, 2);
            nk_label(ctx, "Level:", NK_TEXT_LEFT);
            chosen_map =nk_combo(ctx, maps, num_maps, chosen_map, 25, nk_vec2(200,200));
            
            if(nk_button_label(ctx, "Refresh List"))
            {
                //free the current list
                for(int i = 0; i < num_maps; i++)
                {
                    free((char*)maps[i]);
                }
                delete maps;
                
                //get new list
                num_maps = g_game->list_maps(&maps);
            }
            if(nk_button_label(ctx, "Play"))
            {
                //load map
                std::string map_path = "maps/";
                map_path += maps[chosen_map];
                	
				g_game->changeMap(map_path);
            }
    }
	nk_end(ctx);
}

bool draw_options(nk_context *ctx, UIWindowFuncUserData *usr_data)
{
	return true;
}
