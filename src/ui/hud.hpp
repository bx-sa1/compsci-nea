#ifndef HUD_H_
#define HUD_H_

#define DECLARE_HUD_ELEMENT
#define DEFINE_HUD_ELEMENT

typedef void (*HUDDrawFunc)(nk_context *ctx, void *value);
struct HUDElement
{
	void *value;
	HUDDrawFunc draw_func;
};

class HUD
{
public:
	HUD();
	~HUD();

	void draw();
private:
	std::vector<HUDElement> m_elements; 
};

#endif
