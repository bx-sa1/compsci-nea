#include "main_menu.hpp"

#include "convar.hpp"
#include "ui/ui.hpp"

bool draw_main_menu(nk_context *ctx, UIWindowFuncUserData *usr_data)
{
	int width = ConVarManager::getConVar("res_width")->getNumber();
	int height = ConVarManager::getConVar("res_height")->getNumber();

	//nk_style *s = &ctx->style;

	//push style
	//nk_style_push_style_item(ctx, &s->window.fixed_background, nk_style_item_image(nk_image_ptr(bg_tex)));
	//nk_style_push_color(ctx, &s->window.background, nk_rgba(255, 255, 255, 255));

	//make widow
	if(nk_begin(ctx, "main_menu", nk_rect(0, 0, width, height), 0))
	{
		//draw buttons
		nk_layout_row_dynamic(ctx, height/16.0f, 1);
		draw_play_btn(ctx);
        draw_options_btn(ctx);
        draw_quit_btn(ctx);
        
        
	}
	nk_end(ctx);
	
	//pop style
	//nk_style_pop_style_item(ctx);
	//nk_style_pop_color(ctx);
	return nk_window_is_closed(ctx, "main_menu");
}
