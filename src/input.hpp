#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include <map>
#include <set>
#include <string>
#include "SDL2/SDL.h"
#include "utils/concommand.hpp"
#include <vector>
#include <memory>

class Input 
{
public:
    void reset_rel_mouse();
    void execute_held_keys();
    
    void bind(const std::string &name, const std::string &command); //bind a command to a key

    void handleKey(const SDL_Event &event);
	void handleMouseButton(const SDL_Event &event);
	void handleMouseMotion(const SDL_Event &event);

	int mouse_pos_x, mouse_pos_y, mouse_rpos_x, mouse_rpos_y;
private:
    //key to command string bindings
    std::map<SDL_Keycode, std::string> command_binding;
	std::vector<SDL_Keycode> down_keys;

};


#endif
