add states to resources
fix all the resource types loading funcs
	must make it async for deps

I decided to make the resource loading only async because:
	1. If a resource is loaded async, its dependecies have to be loaded async
	2. If a resource is loaded non-async, I would have to make a special load case to load the dependecies non-async
In order to reduce complexity throughout the code, I feel it is better to make resource loading only async
