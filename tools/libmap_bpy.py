import bpy
import os
import math
import json
import imp
import base64
from mathutils import Vector, Euler
import struct
import bmesh

"""
class CStruct:
    def __init__(self, format):
        self.format = ""

    def sizeof(self):
        return struct.calcsize(self.format)

    def dump(self):
        pass

class Chunk(CStruct):
    def __init__(self):
        super().__init__("<2I")
        self.length = 0
        self.data_ptr = 0

    def dump(self):
        return struct.pack(self.format, self.length, self.data_ptr)

class Header(CStruct):
    def __init__(self):
        super().__init__("<2I")
        self.magic = 0x4d464631
        self.version = 1
        self.chunks = [Chunk()] * 6
        self.chunk_data = [[]] * 6

    def sizeof(self):
        size = super().sizeof()
        for chunk in self.chunks:
            size += chunk.sizeof()
        return size

    def dump(self):
        data = struct.pack(self.format, self.magic, self.version)
        for chunk in chunks:
            data += chunk.dump()
        for chunk in chunk_data:
            for d in chunk:
                data += d.dump()
        return data

class Model(CStruct):
    def __init__(self):
        super().__init__("<8I")
        self.minx = 0
        self.maxx = 0
        self.miny = 0
        self.maxy = 0
        self.minz = 0
        self.maxz = 0
        self.mesh_start = 0
        self.mesh_num = 0

    def dump(self):
        return struct.pack(self.format, self.minx, self.maxx, self.miny, self.maxy, self.minz, self.maxz, self.mesh_start, self.mesh_num)

class Mesh(CStruct):
    def __init__(self):
        super().__init__("<5I")
        self.material = 0
        self.idx_start = 0
        self.idx_num = 0
        self.vert_start = 0
        self.vert_num = 0

    def dump(self):
        return struct.pack(self.format, self.material, self.idx_start, self.idx_num, self.vert_start, self.vert_num)

class Index(CStruct):
    def  __init__(self):
        super().__init__("<I")
        self.idx = 0

    def dump(self):
        return struct.pack(self.format, self.idx)

class Material(CStruct):
    def __init__(self):
        super().__init__("<32c")
        self.mat = ""

    def dump(self):
        return struct.pack(self.format, self.mat)

class Vertex(CStruct):
    def __init__(self):
        super().__init__("<8f")
        self.x = 0
        self.y = 0
        self.z = 0
        self.u = 0
        self.v = 0
        self.nx = 0
        self.ny = 0
        self.nz = 0

    def dump(self):
        return struct.pack(self.format, self.x, self.y, self.z, self.u, self.v, self.nx, self.ny, self.nz)

class EntityDef(CStruct):
    def __init__(self):
        super().__init__("<s")
        self.json = ""

    def dump(self):
        return struct.pack(self.format, self.json)

def get_model(context, models, object):
    model = libmodel_bpy.Model()
    meshes = []
    model_available = False
    final_fe = [
        Vector((0,0,0)),
        Vector((0,0,0))
    ]
            
    for child in object.children:
        if child.type == "MESH":
            model_available = True
            meshes.extend(libmodel_bpy.get_meshes(context, child))

    if model_available:
        model.meshes = meshes
        data = model.dump()
        d_encoded = base64.b64encode(data)
        models.append(d_encoded.decode('ascii'))
        
        a = len(models)-1
        b = -1
        
        return (a, b)
    
    return (-1, -1)
"""
class Header:
    def __init__(self):
        self.modelchunk = []
        self.meshchunk = []
        self.matchunk = []
        self.idxchunk = []
        self.vertchunk = []

class Model:
    def __init__(self, meshes):
        self.meshes = meshes

    def create_refs(self, header):
        for i, mesh in enumerate(self.meshes):
            if mesh in header.meshchunk:
                self.meshes[i] = header.meshchunk.index(mesh)
            else:
                idx = len(header.meshchunk)
                header.meshchunk.append(mesh)
                mesh.create_refs(header)
                self.meshes[i] = idx

class Mesh:
    def __init__(self, material, idxs, verts, max, min):
        self.material = material
        self.idxs = idxs
        self.verts = verts
        self.max = max
        self.min = min

    def create_refs(self, header):
        if self.material in header.matchunk:
            self.material = header.matchunk.index(self.material)
        else:
            midx = len(header.matchunk)
            header.matchunk.append(self.material)
            self.material = midx

        for i, idx in enumerate(self.idxs):
            if idx in header.idxchunk:
                self.idxs[i] = header.idxchunk.index(idx)
            else:
                iidx = len(header.idxchunk)
                header.idxchunk.append(idx)
                self.idxs[i] = iidx

        for i, vert in enumerate(self.verts):
            if vert in header.vertchunk:
                self.verts[i] = header.vertchunk.index(vert)
            else:
                vidx = len(header.vertchunk)
                header.vertchunk.append(vert)
                self.verts[i] = vidx

class Material:
    def __init__(self, mat):
        self.mat = mat

class Index:
    def __init__(self, idx):
        self.idx = idx

class Vertex:
    def __init__(self, x,y,z, u,v, nx,ny,nz):
        self.xyz = [x,y,z]
        self.uv = [u,v]
        self.nxyz = [nx,ny,nz]

class EntityDef:
    def __init__(self, json):
        self.json = json
        
    def create_refs(self, header):
        for entity in self.json.values():
            model = entity["args"].get("model")
            if model is not None:
                if type(model) is Model:
                    if model in header.modelchunk:
                        entity["args"]["model"] = header.modelchunk.index(model)
                    else:
                        modelidx = len(header.modelchunk)
                        header.modelchunk.append(model)
                        model.create_refs(header)
                        entity["args"]["model"] = modelidx


def get_meshes(obj, ctx, scene):
    meshes = []

    materials = [m for m in obj.material_slots]
    material_names = [None if mat is None else mat.name for mat in materials]
    print(material_names)

    data = obj.to_mesh(scene, False, 'PREVIEW')
    data.calc_normals_split()

    vert_pos = [None] * (len(data.vertices) * 3)
    data.vertices.foreach_get('co', vert_pos)

    mat_cache = {}
    for face in data.polygons:
        try:
            mat = material_names[face.material_index]
        except:
            mat = "default"
        
        if mat in mat_cache:
            current_mesh = mat_cache[mat]
        else:
            mesh = Mesh(Material(mat), [], [], ([0]*3), ([0]*3))
            mat_cache[mat] = mesh
            current_mesh = mesh
              
        mverts = []
        idxs = []
        mat_id = face.material_index

        for loop_idx in face.loop_indices:
            loop = data.loops[loop_idx]
            
            uv_layer = data.uv_layers.active.data
            mvert = Vertex(
                vert_pos[loop.vertex_index * 3 + 0], vert_pos[loop.vertex_index * 3 + 1], vert_pos[loop.vertex_index * 3 + 2], 
                uv_layer[loop_idx].uv[0], uv_layer[loop_idx].uv[1], 
                loop.normal[0], loop.normal[1], loop.normal[2])
            mvert.uv[1] *= -1
            mvert.uv[1] += 1
            mverts.append(mvert)
            current_mesh.verts.append(mvert)

        for i in range(2, len(mverts)):
            current_mesh.idxs.append(Index(current_mesh.verts.index(mverts[0])))
            current_mesh.idxs.append(Index(current_mesh.verts.index(mverts[i])))
            current_mesh.idxs.append(Index(current_mesh.verts.index(mverts[i-1])))
            print(current_mesh)
            print([a.idx for a in current_mesh.idxs])
        
        for vert in mverts:
            current_mesh.max[0] = vert.xyz[0] if vert.xyz[0] > current_mesh.max[0] else current_mesh.max[0]
            current_mesh.max[1] = vert.xyz[1] if vert.xyz[1] > current_mesh.max[1] else current_mesh.max[1]
            current_mesh.max[2] = vert.xyz[2] if vert.xyz[2] > current_mesh.max[2] else current_mesh.max[2]
            current_mesh.min[0] = vert.xyz[0] if vert.xyz[0] < current_mesh.min[0] else current_mesh.min[0]
            current_mesh.min[1] = vert.xyz[1] if vert.xyz[1] < current_mesh.min[1] else current_mesh.min[1]
            current_mesh.min[2] = vert.xyz[2] if vert.xyz[2] < current_mesh.min[2] else current_mesh.min[2]

    
    for mesh in mat_cache.values():
        meshes.append(mesh)    
        
    bpy.data.meshes.remove(data)

    return meshes

def get_entities(ctx, scene):
    entities = {}
    arg_exclude = ["_RNA_UI", 'cycles_visibility', 'cycles', "type"]    

    for obj in scene.objects:
        if obj.parent: continue
    
        model = None
        ltype = None
        coeffs = None
        lcolour = None
        
        entity = {
            "type": obj.get("type", "ent_null"),
            "args": { 
                "position": [xyz for xyz in obj.location],
                "rotation": [xyz for xyz in obj.rotation_euler],
                
            }
        }
        entity["args"].update({key:value for (key,value) in obj.items() if key not in arg_exclude})
        
        if obj.type == "MESH":
            meshes = get_meshes(obj, ctx, scene)
            entity["args"]["model"] = Model(meshes)
        elif obj.type == "LAMP":
            entity["args"]["coeffs"] = [
                obj.data.quadratic_coefficient,
                obj.data.linear_coefficient,
                obj.data.constant_coefficient
            ]        
            entity["args"]["colour"] = [
                obj.data.color[0],
                obj.data.color[1],
                obj.data.color[2],
                obj.data.energy
            ]
            ltype = obj.data.type
            if ltype == "SUN": ltype = "DIRECTIONAL"
            entity["args"]["light_type"] = ltype
        
        entities[obj.name] = entity

    return EntityDef(entities)

def scene_to_map(context, scene):
    header = Header()

    entitydef = get_entities(context, scene)
    entitydef.create_refs(header)
    entity_json = json.dumps(entitydef.json, indent=0, sort_keys=True)
    print(entity_json)

    header_fmt = "<14I"
    chunk_0_fmt = "<2I"
    chunk_1_fmt = "<5I6f"
    chunk_2_fmt = "<H"
    chunk_3_fmt = "<32s"
    chunk_4_fmt = "<8f"
    chunk_5_fmt = ""

    length_header = struct.calcsize(header_fmt)
    length_chunk_0 = struct.calcsize(chunk_0_fmt)*len(header.modelchunk)
    length_chunk_1 = struct.calcsize(chunk_1_fmt)*len(header.meshchunk)
    length_chunk_2 = struct.calcsize(chunk_2_fmt)*len(header.idxchunk)
    length_chunk_3 = struct.calcsize(chunk_3_fmt)*len(header.matchunk)
    length_chunk_4 = struct.calcsize(chunk_4_fmt)*len(header.vertchunk)
    length_chunk_5 = len(entity_json.encode("utf-8"))

    def incr(a, b):
        a+=b
        return a

    #header
    total_ofs = 0
    file = struct.pack(header_fmt, 
        0x4d464631, 
        1,
        length_chunk_0,
        length_header,
        length_chunk_1,
        length_header + length_chunk_0,
        length_chunk_2,
        length_header + length_chunk_0 + length_chunk_1,
        length_chunk_3,
        length_header + length_chunk_0 + length_chunk_1 + length_chunk_2,
        length_chunk_4,
        length_header + length_chunk_0 + length_chunk_1 + length_chunk_2 + length_chunk_3,
        length_chunk_5,
        length_header + length_chunk_0 + length_chunk_1 + length_chunk_2 + length_chunk_3 + length_chunk_4
    )

    #chunk 0
    for model in header.modelchunk:
        file += struct.pack(chunk_0_fmt,
            model.meshes[0], len(model.meshes)
        )

    #chunk 1
    for mesh in header.meshchunk:
        print(mesh.material)
        file += struct.pack(chunk_1_fmt,
            mesh.material,
            mesh.idxs[0], len(mesh.idxs),
            mesh.verts[0], len(mesh.verts),
            mesh.max[0], mesh.max[1], mesh.max[2],
            mesh.min[0], mesh.min[1], mesh.min[2]
        )

    #chunk 2
    for idx in header.idxchunk:
        file += struct.pack(chunk_2_fmt,
            idx.idx
        )

    #chunk 3
    for mat in header.matchunk:
        file += struct.pack(chunk_3_fmt,
            mat.mat.encode("utf-8")
        )

    #chunk 4
    for v in header.vertchunk:
        file += struct.pack(chunk_4_fmt,
            v.xyz[0], v.xyz[1], v.xyz[2],
            v.uv[0], v.uv[1],
            v.nxyz[0], v.nxyz[1], v.nxyz[2]
        )
        
    #chunk 5
    file += entity_json.encode("utf-8")


    return file

if __name__ == "__main__":
    scene_to_map(bpy.context, bpy.context.scene)
