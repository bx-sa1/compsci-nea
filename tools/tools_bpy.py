import bpy
import libmap_bpy
import imp
imp.reload(libmap_bpy)
import json

bpy.types.Scene.map_name = bpy.props.StringProperty(
        name="Map name",
        default=""
        )
        
bpy.types.Scene.map_export_dir = bpy.props.StringProperty(
        name="Save Directory",
        default="",
        subtype="DIR_PATH"
        )
    
bpy.types.Scene.map_size = bpy.props.IntVectorProperty(
        name="Map Size",
        size=3
        )
    
class MapPanel(bpy.types.Panel):
    bl_label = "Tools"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "Tools"

    def draw(self, context):
        lo = self.layout
        props = context.scene
        
        lo.label("Map Tools")
        lo.prop(props, "map_name")
        lo.prop(props, "map_export_dir")
        lo.prop(props, "map_size")
        lo.operator("export.export_map", text="Export Map")

class ExportMap(bpy.types.Operator):
    bl_label = "Export Map"
    bl_idname = "export.export_map"

    def execute(self, context):
        props = context.scene
        
        dir = props.map_export_dir 
        name = props.map_name
        ext = ".map"
        
        obj = libmap_bpy.scene_to_map(context, context.scene)
        with open(dir + name + ext, "wb") as f:
            f.write(obj)
        
        return {'FINISHED'}


def register():
    bpy.utils.register_module(__name__)
    #bpy.types.Scene.props = bpy.props.PointerProperty(type=Props)
    
def unregister():
    bpy.utils.unregister_module(__name__)
    #del bpy.types.Scene.props
    
if __name__ == "__main__":
    register()
