##Blender script to export model file
##help from this on getting blender mesh information https://github.com/lsalzman/iqm/blob/master/blender-2.74/iqm_export.py
import bpy
import os
import struct
import math
from mathutils import Vector

#Vertex class that stores vertex information and saves it to file
class Vertex:
    
    def __init__(self):
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        self.u = 0.0
        self.v = 0.0
        self.nx = 0.0
        self.ny = 0.0
        self.nz = 0.0
        self.bw0 = 0.0
        self.bw1 = 0.0
        self.bi0 = 0
        self.bi1 = 0
        
        self.b_format = "<10f2H"
    
    def get_size(self):
        return struct.calcsize(self.b_format)
    
    def dump(self):
        data = struct.pack(self.b_format, self.x, self.y, self.z, self.u, self.v, self.nx, self.ny, self.nz, self.bw0, self.bw1, self.bi0, self.bi1)
        return data

#Triangle class that stores triangle information and saves it to file
class Triangle:
        
    def __init__(self):
        self.a = 0
        self.b = 0
        self.c = 0
        self.b_format = "<3H"
    
    def get_size(self):
        return struct.calcsize(self.b_format)
    
    def dump(self):
        data = struct.pack(self.b_format, self.a, self.b, self.c)
        return data

#Joint class    
class Joint:

    def __init__(self):
        self.posx = 0
        self.posy = 0
        self.posz = 0

        self.rotx = 0
        self.roty = 0
        self.rotz = 0

        self.b_format = "<6f"
        
    def get_size():
        return struct.calcsize(self.b_format)
    
    def dump(self):
        data = struct.pack(self.b_format, self.posx, self.posy, self.posz, self.rotx, self.roty, self.rotz)
        return data
     
#Mesh class that holds mesh information, calculate offsets and saves to file
class Mesh:
    
    def __init__(self):
        self.material = ""
    
        self.ofs_verts = 0
        self.num_verts = 0
    
        self.ofs_tris = 0
        self.num_tris = 0
    
        self.ofs_end = 0
    
        self.b_format = "<32s5I" #might change
    
        self.verts = []
        self.tris = []
    
    def get_size(self):
        ofs = struct.calcsize(self.b_format)
        
        self.ofs_verts = ofs
        self.num_verts = len(self.verts)
        for v in self.verts:
            ofs += v.get_size()
        
        self.ofs_tris = ofs
        self.num_tris = len(self.tris)
        for t in self.tris:
            ofs += t.get_size()
        
        self.ofs_end = ofs
        
        return self.ofs_end 
    
    def dump(self):
        self.get_size()
        data = struct.pack(self.b_format, str.encode(self.material, "ascii"), self.num_verts, self.ofs_verts, self.num_tris, self.ofs_tris, self.ofs_end)
        
        for v in self.verts:
            data += v.dump()
            
        for t in self.tris:
            data += t.dump()

        return data

#Model file header that holds all the joint, mesh and file information, calculates offsets and saves to file           
class Model:
    
    def __init__(self):
        self.magic = "MFF1"
    
        self.filesize = 0 
    
        self.ofs_joints = 0
        self.num_joints = 0
    
        self.ofs_meshes = 0
        self.num_meshes = 0
    
        self.joints = []
        self.meshes = []
    
        self.b_format = "<4s5I"
    
    def get_size(self):
        #basically go through whole file setting offsets at points
        ofs = struct.calcsize(self.b_format)
        
        self.ofs_joints = ofs
        self.num_joints = len(self.joints)
        for j in self.joints:
            ofs += j.get_size()
        
        self.ofs_meshes = ofs
        self.num_meshes = len(self.meshes)
        for m in self.meshes:
            ofs += m.get_size()
        
        print(ofs)
        self.filesize = ofs
        print(self.filesize)
        
        return self.filesize
    
    def dump(self):
        self.get_size()
        data = struct.pack(self.b_format, 
                           str.encode(self.magic), 
                           self.filesize, self.num_joints, 
                           self.ofs_joints, 
                           self.num_meshes, 
                           self.ofs_meshes)
        
        for j in self.joints:
            data += j.dump()
        
        for m in self.meshes:
            data += m.dump()

        return data


def get_joints(context, obj):
    joints = []
    
    armature = obj.find_armature()
    if armature is not None:
        for bone in armature.data.bones:
            joint = Joint()
            
            bone_mat = armature.matrix_world * bone.matrix_local
            bonepos = bone_mat.to_translation()
            bonescale = bone_mat.to_scale()
            bonerot = bone_mat.to_euler()
            
            joint.posx = bonepos.x * bonescale.x
            joint.posy = bonepos.y * bonescale.y
            joint.posz = bonepos.z * bonescale.z
            
            joint.rotx = bonerot.x
            joint.roty = bonerot.y
            joint.rotz = bonerot.z

            joints.append(joint)

    return joints

def calc_center(o, gspace = False):
    local_bbox_center = 0.125 * sum((Vector(b) for b in o.bound_box), Vector())
    if not gspace: return local_bbox_center
    global_bbox_center = o.matrix_world * local_bbox_center  
    return global_bbox_center
            
def get_meshes(context, obj, bones=False):
    meshes = []
    
    mesh_dict = {}
    data = obj.to_mesh(bpy.context.scene, False, 'PREVIEW')
    data.calc_normals_split()
    uv_layer = data.uv_layers.active and data.uv_layers.active.data
    for face in data.polygons:
        if len(face.vertices) < 3:
            continue
        
        try:
            current_mesh = mesh_dict[face.material_index]
        except:
            current_mesh = Mesh()
            name = data.materials[face.material_index].name
            if len(name) > 32: 
                print("Name is bigger than 32 chars, using default")
                name = "default"
            elif name == "Untitled" or name == "Material": 
                name = "default"
            current_mesh.material = name
            
            meshes.append(current_mesh)
            mesh_dict[face.material_index] = current_mesh
            
        face_verts = []
        for loop_idx in face.loop_indices:
            loop = data.loops[loop_idx]
            
            obj_mat = obj.matrix_basis
            print(obj_mat)
            obj_loc = obj_mat * data.vertices[loop.vertex_index].co
            
            #get vertex data
            vert = Vertex()
            vert.x = obj_loc.x
            vert.y = obj_loc.y
            vert.z = obj_loc.z
            
            if uv_layer:
                vert.u = uv_layer[loop_idx].uv[0]
                vert.v = 1-uv_layer[loop_idx].uv[1] #flip y
            else:
                vert.u = 0
                vert.v = 0
            
            if face.use_smooth:
                vert.nx = loop.normal[0]
                vert.ny = loop.normal[1]
                vert.nz = loop.normal[2]    
            else:
                vert.nx = face.normal[0]
                vert.ny = face.normal[1]
                vert.nz = face.normal[2]
        
            if bones:
                pass
            
            current_mesh.verts.append(vert)
            face_verts.append(vert)
            
            
        ##triangulate
        ##0--1 must beclockwise or anti-clockwise winding order
        ##|\ |
        ##| \|
        ##3--2
        
        #     0
        #    ,'.
        # 4,' ^ `.1   triangulation for a pentagon looks sort of like this 0, 2, 1, 0, 3, 2, 0, 4, 3
        #  \ / \ /
        #  3\___/2
        ##works for any polygon with more than 3 vertices
        for i in range(2, len(face_verts)):
            tri = Triangle()
            tri.a = current_mesh.verts.index(face_verts[0])
            tri.b = current_mesh.verts.index(face_verts[i])
            tri.c = current_mesh.verts.index(face_verts[i-1])
            current_mesh.tris.append(tri)

    return meshes

def optimise_meshes(meshes):
    pass

#writes model to file and triangulates meshes so they are acceptable
def object_to_binary(context, obj):
    model = Model()
    
    joints = get_joints(context, obj)
    meshes = get_meshes(context, obj, len(joints) != 0)
    
    optimise_meshes(meshes)
    
    model.meshes = meshes
    model.joints = joints
    
    data = model.dump()
    
    return data
