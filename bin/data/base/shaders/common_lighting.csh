#ifndef COMMON_LIGHTING_H_
#define COMMON_LIGHTING_H_

struct Light
{
	int type;
	vec3 pd;
	vec4 colour;
	vec3 coeffs;
};

uniform Light light;
uniform vec4 specularity_props;

#if 0
float calcVisibility(Light light, vec4 lightspace_pos, vec3 worldspace_pos, vec3 worldspace_normal)
{
	vec3 ldir = light.type == 1 ? normalize(light.pd-worldspace_pos) : normalize(-light.pd);

	vec3 pos = lightspace_pos.xyz / lightspace_pos.w; //perspectrive divide, noop for orthographics projection
	pos = pos * 0.5 + 0.5; //transform pos from [-1,1] to [0,1]
	pos.z = min(pos.z, 1.0);

	float closest = texture(shadow_map_atlas_array, vec3(pos.xy, 0.0)).r; //sample shadow map
	float bias = max(0.05 * (1.0 - dot(worldspace_normal, ldir)), 0.005);  
	float visible = closest < pos.z - bias ? 0.0 : 1.0; //fragment is visible if current depth is gt value in shadowmap

	return visible;
}
#endif

float calcAttenuation(const Light light, const vec3 vpos)
{
	if(light.type == 0) return 1.0;
#if 0 
	vec3 ldir = light.pd-vpos;
	float dist = dot(ldir, ldir);
	vec3 cle = vec3(1.0, dist/sqrt(dist), dist);
	float att = 1.0/dot(light.coeffs, cle);
	return att;
#endif

	float dist = length(light.pd-vpos);
	return light.colour.a / (light.coeffs.z + light.coeffs.y*dist + light.coeffs.x*(dist*dist));
}

float calcDiffuseComponent(const Light light, const vec3 vpos, const vec3 vnormal)
{
	vec3 ldir = light.type == 1 ? normalize(light.pd-vpos) : normalize(-light.pd);
	
    float d = max(dot(vnormal, ldir), 0.0); 
	
	return d;
}

float calcSpecularComponent(const Light light, const vec3 viewpos, const vec3 vpos, const vec3 vnormal, const float exponent)
{
	vec3 ldir = light.type == 1 ? normalize(light.pd-vpos) : normalize(-light.pd);
	
	vec3 vdir = normalize(viewpos-vpos);
	vec3 reflectdir = reflect(-ldir, vnormal);

	float spec = pow(max(dot(vdir, reflectdir), 0.0), exponent);

	return spec;
}
    
#endif
