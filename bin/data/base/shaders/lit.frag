#include "common.csh"
#include "common_lighting.csh"

in vec3 out_world_pos;
in vec3 out_pos;
in vec3 out_normal;
in vec2 out_uv;

out vec4 out_colour;

uniform sampler2D base_texture;
uniform sampler2D specular_mask;

void main()
{
	vec3 norm = normalize(out_normal);

	//diffuse
	vec3 diffuse = light.colour.rgb * (calcDiffuseComponent(light, out_world_pos, norm) * texture(base_texture, out_uv).rgb);

	//specular
	vec3 spec = light.colour.rgb * calcSpecularComponent(light, view_pos, out_world_pos, norm, 512);
	
	//attenuation
	float atten = calcAttenuation(light, out_world_pos);
	diffuse *= atten;
	spec *= atten;

	vec3 res = (diffuse + spec);
	out_colour = vec4(res, 1.0);
}
