#include "common.csh"

in vec3 in_position;
in vec3 in_normal;
in vec2 in_uv_0;

out vec3 out_world_pos;
out vec3 out_pos;
out vec3 out_normal;
out vec2 out_uv;

void main()
{
    gl_Position = g_projectionMatrix * g_viewMatrix  * g_modelMatrix * vec4(in_position, 1.0);
    out_world_pos = vec3(g_modelMatrix * vec4(in_position, 1.0));
    out_pos = in_position;
    out_normal = mat3(g_modelMatrix) * in_normal;
    out_uv = in_uv_0;
}
