in vec3 in_position;
in vec4 in_colour_0;

out vec3 out_colour;
out vec3 out_pos;

uniform mat4 mvp;

void main()
{
    gl_Position = mvp * vec4(in_position, 1.0);
    out_pos = in_position;
    out_colour = in_colour_0.rgb;
}