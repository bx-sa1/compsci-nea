#include "common.csh"

in vec3 out_uv;

out vec4 out_colour;

uniform samplerCube sky;

void main()
{
	out_colour = texture(sky, out_uv);
} 