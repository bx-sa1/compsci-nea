#ifndef COMMON_H_
#define COMMON_H_

uniform mat4 g_projectionMatrix;
uniform mat4 g_viewMatrix;
uniform mat4 g_modelMatrix;
uniform vec3 view_pos; //current view pos

const float gamma = 2.2;
#define BIAS 0.01

vec3 srgbToLinear(const vec3 c)
{
	return pow(c, vec3(gamma));
}

vec3 linearToSRGB(const vec3 c)
{
	return pow(c, vec3(1.0/gamma));
}

float backfaceCull(vec3 pos, vec3 normal)
{
	vec3 n = normalize(normal);
	vec3 vdir = normalize(view_pos-pos);
    float should_draw = dot(vdir, n);
    return should_draw;
}

#endif