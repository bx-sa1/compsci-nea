#include "common.csh"

in vec3 out_world_pos;
in vec3 out_pos;
in vec3 out_normal;
in vec2 out_uv;

out vec4 out_colour;

uniform sampler2D base_texture;

void main()
{
	vec4 diffuse_colour = texture(base_texture, out_uv);

    out_colour = diffuse_colour;
}
