uniform vec3 ambient_colour;

out vec4 colour;

void main()
{
	colour = vec4(ambient_colour, 1.0);	
}
