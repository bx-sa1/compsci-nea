#include "common.csh"

in vec3 in_position;

out vec3 out_uv;

void main()
{
    gl_Position = (g_projectionMatrix * mat4(mat3(g_viewMatrix)) * vec4(in_position, 1.0));
	out_uv = in_position.xzy; //transform from z up to z forward
}