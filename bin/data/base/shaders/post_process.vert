in vec3 in_position;
out vec2 out_uv;

void main()
{
    gl_Position = vec4(in_position, 1.0);
    out_uv = (in_position*0.5+0.5).xy;
}