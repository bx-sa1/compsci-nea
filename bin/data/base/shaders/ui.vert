#include "common.csh"

in vec3 in_position;
in vec2 in_uv_0;
in vec4 in_colour_0;

out vec2 out_uv;
out vec4 out_colour;

void main()
{
	gl_Position = g_projectionMatrix * g_viewMatrix  * g_modelMatrix * vec4(in_position, 1.0);
	out_uv = in_uv_0;
	out_colour = in_colour_0;
}
