#include "common.csh"

in vec2 out_uv;
out vec4 out_colour;

uniform sampler2D colourbuffer;

void main()
{
    vec4 colourb = texture(colourbuffer, out_uv);
    //colourb.rgb = linearToSRGB(colourb.rgb);

    out_colour = colourb;
}
