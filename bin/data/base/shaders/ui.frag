in vec4 out_colour;
in vec2 out_uv;

uniform sampler2D ui_tex;

out vec4 final_colour;

void main()
{
	final_colour = 	out_colour * texture(ui_tex, out_uv);
}
